
import logging

from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

import alipaylib, alipaywap
from members.models import Credit
from orders.models import Payment
from orders.notify import sendPaymentFinishMail, sendOnPaymentFinishSMS


logger = logging.getLogger('payment')


def payment_notify(request):
    params = request.POST
    if params:
        logger.info('Verifying Payment for Request: %s' % unicode(params))
        if alipaylib.get_sign_verified(params):
            trade_status = params['trade_status']
            logger.info('Payment Verify Succeeded! The trade status is %s' % trade_status)

            if trade_status in {'TRADE_FINISHED', 'TRADE_SUCCESS'}:
                out_trade_no = params['out_trade_no']
                total_fee = params['total_fee']
                payment = Payment.objects.get(payment_no=out_trade_no)
                amount = float(total_fee)
                payment.confirm(amount, 1)
                obj = {
                    'payment_no': out_trade_no,
                    'payment_from': 'zhifubao',
                    'amount': str(amount)
                }
                if payment.member:
                    Credit.objects.create(member=payment.member,
                                          amount=int(amount+1), credit_type=8,
                                          reason=payment.comment)
                sendPaymentFinishMail(obj)
                sendOnPaymentFinishSMS(obj)
            return HttpResponse('success')
        else:
            logger.error('Payment Verify Failed!')
            return HttpResponse('Verify Result Failed', status=400)
    else:
        return HttpResponse('Missing Post Parameters', status=400)


@api_view(['GET'])
def payable(request, payment_no):
    payment = Payment.objects.get(payment_no=payment_no)
    if not payment.is_payable:
        return Response({'detail': _('Already Paid')}, status=status.HTTP_400_BAD_REQUEST)

    order = {'out_trade_no': payment.payment_no,
             'body': payment.comment,
             'subject': payment.comment,
             'total_fee': str(payment.amount),
             'notify_url': 'http://%s/alipay/payment_notify/' % request.META['HTTP_HOST'],
    }
    orderString = alipaylib.build_payable_order_string(order)
    logger.info('Built Payable Order String: %s' % orderString)

    return Response({'orderString': orderString})


def paymentwap_notify(request):
    params = { k: request.POST.get(k) for k in request.POST }
    if params:
        logger.info('Verifying WAP Payment Notify for Request: %s' % unicode(params))
        verified, notifydata = alipaywap.verify_payment_notify(params)
        if verified:
            trade_status = notifydata['trade_status']
            logger.info('WAP Payment Notify Verify Succeeded! The trade status is %s' % trade_status)
            if trade_status in {'TRADE_FINISHED', 'TRADE_SUCCESS'}:
                out_trade_no = notifydata['out_trade_no']
                total_fee = notifydata['total_fee']
                payment = Payment.objects.get(payment_no=out_trade_no)
                amount = float(total_fee)
                payment.confirm(amount, 4)
                obj = {
                    'payment_no': out_trade_no,
                    'payment_from': 'Alipay',
                    'amount': str(amount)
                }
                if payment.member:
                    Credit.objects.create(member=payment.member,
                                          amount=int(amount+1), credit_type=8,
                                          reason=payment.comment)
                sendPaymentFinishMail(obj)
                sendOnPaymentFinishSMS(obj)
            return HttpResponse('success')
        else:
            logger.error('WAP Payment Notify Verify Failed!')
            return HttpResponse('Verify Result Failed', status=400)
    else:
        return HttpResponse('Missing GET Parameters', status=400)


def paymentwap_callback(request):
    params = { k: request.GET.get(k) for k in request.GET }
    if params:
        logger.info('Verifying WAP Payment Callback for Request: %s' % unicode(params))
        if alipaywap.verify_payment_callback(params):
            out_trade_no = params['out_trade_no']
            logger.info('WAP Payment Callback Verify Succeeded! The payment number is %s' % out_trade_no)
            
#             payment = Payment.objects.get(payment_no=out_trade_no)
#             amount = payment.amount
#             payment.confirm(amount, 4)
#             obj = {
#                 'payment_no': out_trade_no,
#                 'payment_from': 'Alipay',
#                 'amount': str(amount)
#             }
#             if payment.member:
#                 Credit.objects.create(member=payment.member,
#                                       amount=int(amount+1), credit_type=8,
#                                       reason=payment.comment)
#             sendPaymentFinishMail(obj)
#             sendOnPaymentFinishSMS(obj)

            return redirect('http://mobile.clubmeiwei.com/')
        else:
            logger.error('WAP Payment Callback Verify Failed!')
            return HttpResponse('Verify Result Failed', status=400)
    else:
        return HttpResponse('Missing GET Parameters', status=400)


@api_view(['GET'])
def payablewap(request, payment_no):
    payment = Payment.objects.get(payment_no=payment_no)
    if not payment.is_payable:
        return Response({'detail': _('Already Paid')}, status=status.HTTP_400_BAD_REQUEST)
    
    callback_url = 'http://%s/alipay/paymentwap_callback/' % request.META['HTTP_HOST']
    notify_url = 'http://%s/alipay/paymentwap_notify/' % request.META['HTTP_HOST']
    amount = str(payment.amount)
    description = unicode(payment.comment) if payment.comment else payment.payment_no
    payable_url = alipaywap.build_payable_order_url(payment.payment_no, amount, description, callback_url, notify_url)
    logger.info('Built Payable Order String: %s' % payable_url)
    
    #return Response({'orderString': payable_url})
    return redirect(payable_url)
