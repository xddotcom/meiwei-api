# encoding=utf-8
import logging

from M2Crypto import BIO, RSA, EVP
import base64

logger = logging.getLogger('payment')


def sign(data, private_key):
    logger.info('RSA Signing Data: %s' % unicode(data))
    key = RSA.load_key_string(private_key)
    m = EVP.MessageDigest('sha1')
    m.update(data.encode('utf-8'))
    digest = m.final()
    signature = key.sign(digest, 'sha1')
    return base64.b64encode(signature)


def verify(data, ali_public_key, sign):
    logger.info('RSA Verifying Data: %s and Signature: %s' % (unicode(data), sign))
    bio = BIO.MemoryBuffer(ali_public_key)
    rsa = RSA.load_pub_key_bio(bio)
    pubkey = EVP.PKey()
    pubkey.assign_rsa(rsa)

    # If you need a different digest than the default 'sha1':
    # pubkey.reset_context(md='sha1')
    pubkey.verify_init()
    pubkey.verify_update(data.encode('utf-8'))
    verified = pubkey.verify_final(base64.b64decode(sign)) == 1
    if verified:
        logger.info('RSA Verify Succeeded')
        pass
    else:
        logger.info('RSA Verify Failed')
        pass
    return verified
