
from django.conf.urls import patterns, url

urlpatterns = patterns('alipay.views',
    url(r'^payable/(?P<payment_no>p?[0-9]+)', 'payable'),
    url(r'^payment_notify', 'payment_notify'),
    url(r'^payablewap/(?P<payment_no>p?[0-9]+)', 'payablewap'),
    url(r'^paymentwap_notify', 'paymentwap_notify'),
    url(r'^paymentwap_callback', 'paymentwap_callback')
)
