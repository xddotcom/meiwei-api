
from hashlib import md5
import logging
import urllib, urllib2, urlparse

import config as CONFIG
import xml.etree.ElementTree as ET


logger = logging.getLogger('payment')


def build_payable_order_url(order_no, amount, description, callback_url, notify_url):
    query_str = build_request('trade', order_no, amount, description, callback_url, notify_url)
    url = CONFIG.ALIPAY_GATEWAY + query_str
    
    logger.info('Get Alipay Token: %s' % url)
    response = urllib2.urlopen(url.encode('utf-8'))
    result = response.read()
    logger.info('Get Alipay Token Response: %s' % result)
    response.close()
    
    params = urlparse.parse_qs(result)
    res_data = parse_response(params['res_data'][0])
    token = res_data['request_token']
    
    query_str = build_request('execute', token)
    payable_url = CONFIG.ALIPAY_GATEWAY + query_str
    return payable_url


def build_request(service, *args, **kwargs):
    if service == 'trade':
        para = build_trade_para(*args, **kwargs)
    elif service == 'execute':
        para = build_execute_para(*args, **kwargs)
    
    para = filter_para(para)
    para_str = create_link_string(para, True, False)
    para['sign'] = md5_sign(para_str)
    para['sec_id'] = 'MD5'
    
    return create_link_string(para, False, True)


def build_trade_para(order_no, amount, description, callback_url, notify_url):
    req_data_str = '<direct_trade_create_req><notify_url>%s</notify_url><call_back_url>%s</call_back_url><seller_account_name>%s</seller_account_name><out_trade_no>%s</out_trade_no><subject>%s</subject><total_fee>%s</total_fee></direct_trade_create_req>'
    req_data = req_data_str % (notify_url,
                               callback_url,
                               CONFIG.EMAIL,
                               order_no,
                               description,
                               amount)
    
    para = {'service': 'alipay.wap.trade.create.direct',
            'partner': CONFIG.PARTNER,
            '_input_charset': 'utf-8',
            'sec_id': 'MD5',
            'format': 'xml',
            'v': '2.0',
            'req_id': order_no,
            'req_data': req_data, }
    return para


def build_execute_para(token):
    req_data_str = '<auth_and_execute_req><request_token>%s</request_token></auth_and_execute_req>'
    req_data = req_data_str % token
    para = {'req_data': req_data,
            'service': 'alipay.wap.auth.authAndExecute',
            'partner': CONFIG.PARTNER,
            'sec_id': 'MD5',
            '_input_charset': 'utf-8',
            'v': '2.0',
            'format': 'xml', }
    return para


def parse_response(response_str):
    root = ET.fromstring(response_str.encode('utf-8'))
    #return {child.tag:child.text for child in root.getchildren()}
    return {child.tag:child.text for child in list(root)}


def verify_payment_notify(params):
    result = md5_verify(params, params['sign'], wap_async=True)
    return result, parse_response(params['notify_data'])


def verify_payment_callback(params):
    return md5_verify(params, params['sign'], wap_async=False)


def md5_sign(para_str):
    return md5('%s%s' % (para_str.encode('utf-8'), CONFIG.KEY)).hexdigest()


def md5_verify(paras, sign, wap_async=False):
    paras = filter_para(paras)
    if wap_async:
        para_str = create_wap_async_notice_str(paras)
    else:
        para_str = create_link_string(paras, True, False)
    return md5_sign(para_str) == sign


def filter_para(paras):
    for k, v in paras.items():
        if not v or k in ['sign', 'sign_type']:
            paras.pop(k)
    return paras


def create_link_string(paras, sort, encode):
    if sort:
        paras = sorted(paras.items(), key=lambda d:d[0])
    else:
        paras = paras.items()
    ps = '&'.join(['%s=%s' % (k, urllib.quote(v.encode('utf-8')) if encode else v) for k, v in paras])
    return ps


def create_wap_async_notice_str(paras):
    service = paras.get('service')
    v = paras.get('v')
    sec_id = paras.get('sec_id')
    notify_data = paras.get('notify_data')
    return 'service=%s&v=%s&sec_id=%s&notify_data=%s' % (service, v, sec_id, notify_data)
