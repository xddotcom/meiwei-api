# encoding=utf-8

import logging
import re
import urllib
import urllib2

import config as CONFIG
import rsa


logger = logging.getLogger('payment')


config = {
    'partner': CONFIG.PARTNER,
    'seller_id': CONFIG.EMAIL,
    'private_key': CONFIG.PRIVATE_KEY,
    'ali_public_key': CONFIG.ALIPAY_PUBLIC_KEY,
    'input_charset': 'utf-8',
    'sign_type': 'RSA',
    'transport': 'https',
    'https_verify_url': 'https://mapi.alipay.com/gateway.do?service=notify_verify&',
    'http_verify_url': 'http://notify.alipay.com/trade/notify_query.do?'
}


def filter_params(params):
    return {key: params[key] 
            for key in params
            if key.lower() not in {'sign', 'sign_type'} and params[key] }


def join_params(_params):
    params = filter_params(_params)
    sorted_keys = params.keys()
    sorted_keys.sort()
    return '&'.join(['%s=%s' % (key, params[key]) for key in sorted_keys])


def build_payable_order_string(_params):
    _params.update({'partner': config['partner'],
                    'seller_id': config['seller_id'],
                    'service': 'mobile.securitypay.pay',
                    'payment_type': '1',
                    '_input_charset': 'UTF-8',
                    'it_b_pay': '1m',
                    'show_url': ''})
    params = filter_params(_params)
    
    if 'notify_url' in params:
        params['notify_url'] = urllib.quote(params['notify_url'])
    
    joined_string = '&'.join(['%s="%s"' % (key, unicode(value))
                              for key, value in params.items()])
    params['sign'] = urllib.quote(rsa.sign(joined_string, config['private_key']))
    params['sign_type'] = 'RSA'
    
    return '&'.join(['%s="%s"' % (key, value) for key, value in params.items()])


def get_sign_verified(params):
    sign = params['sign']
    prestr = join_params(params)
    verified = rsa.verify(prestr, config['ali_public_key'], sign)
    request_valid = 'notify_id' in params and check_request(params['notify_id'])
    return request_valid and verified


def check_request(notify_id):
    transport = config['transport']
    partner = config['partner']
    veryfy_url = config['https_verify_url'
                        if transport == 'https'
                        else 'http_verify_url']
    veryfy_url += 'partner=%s&notify_id=%s' % (partner, notify_id)
    urlopen = urllib2.urlopen(veryfy_url, timeout=12000)
    result = urlopen.read()
    logger.info('Checking request using notify_id: %s' % unicode(result))
    if re.match(result, 'true'):
        return True
    else:
        logger.error('Request with notify_id %s is not valid' % notify_id)
        return False
