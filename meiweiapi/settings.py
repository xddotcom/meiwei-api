
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'meiweiapi.wsgi.application'
ROOT_URLCONF = 'meiweiapi.urls'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'p++1t%$*s_+i(rm_@@q1&amp;s$3g^!=7g!o$9b7vk_p1^r!aw%skp'

import os
SETTING_DIR = os.path.realpath(os.path.dirname(__file__))
SITE_ROOT = os.path.abspath(os.path.join(SETTING_DIR, '..'))
SITE_URL = "http://api.clubmeiwei.com"

SITE_ID = 1

DEBUG = False
TEMPLATE_DEBUG = DEBUG

EMAIL_DEBUG = DEBUG
SMS_DEBUG = DEBUG
ANSHIFU_DEBUG = DEBUG
PAYMENT_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

ADMINS = (
    ('Xindong Ding', 'dxd.spirits@gmail.com'),
)

MANAGERS = ADMINS

AUTHENTICATION_BACKENDS = (
    'members.auth.backends.MeiweiBackend',
    'members.auth.backends.OAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'meiwei',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

TIME_ZONE = 'Asia/Shanghai'
LANGUAGE_CODE = 'zh-CN'
DATE_FORMAT = 'Y-m-d'
TIME_FORMAT = 'H:i:s'
DATETIME_FORMAT = 'Y-m-d H:i:s'
USE_I18N = True   # Internationalization machinery
USE_L10N = False  # Format dates, numbers and calendars according to the current locale
USE_TZ = False    # Timezone-aware datetimes
LOCALE_PATHS = (
    os.path.join(SITE_ROOT, 'locale'),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')
# URL that handles the media served from MEDIA_ROOT.
#MEDIA_URL = SITE_URL + '/media/'
MEDIA_URL = "http://meiwei.u.qiniudn.com/media/"

# Don't put anything in this directory yourself
STATIC_ROOT = os.path.join(SITE_ROOT, 'static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    # Additional locations of static files
    # os.path.join(SITE_ROOT, 'staticfiles'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_DIRS = (
    # Always use forward slashes and absolute paths.
    # os.path.join(SITE_ROOT, 'templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django.middleware.gzip.GZipMiddleware',

    'django.middleware.transaction.TransactionMiddleware',

    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # 'django.middleware.csrf.CsrfViewMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Allow CORS
    'corsheaders.middleware.CorsMiddleware'
)

INSTALLED_APPS = (
    'suit',
    
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin',
    'django.contrib.admindocs',

    'rest_framework',
    'corsheaders',

    'members',
    'restaurants',
    'staffs',
    'orders',
    'address',
    'payments',

    'clients',
)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] "%(levelname)s %(module)s" %(message)s',
            'datefmt': '%d/%b/%Y %I:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'logfile': {
            'level':'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.abspath(os.path.join(SETTING_DIR, '../logs/meiweiapi.log')),
            'formatter': 'verbose'
        },
        'searchlogfile': {
            'level':'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.abspath(os.path.join(SETTING_DIR, '../logs/keywordsearch.log')),
            'formatter': 'verbose'
        },
        'paymentlogfile': {
            'level':'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.abspath(os.path.join(SETTING_DIR, '../logs/payment.log')),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'meiweiapi': {
            'handlers': ['mail_admins', 'logfile'],
            'level': 'INFO',
            'propagate': True,
        },
        'payment': {
            'handlers': ['mail_admins', 'paymentlogfile'],
            'level': 'INFO',
            'propagate': True,
        },
        'searchlog': {
            'handlers': ['searchlogfile'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}

SERVER_EMAIL = 'systemadmin@clubmeiwei.com'
EMAIL_HOST = 'smtp.ym.163.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'systemadmin@clubmeiwei.com'
EMAIL_HOST_PASSWORD = 'pull1009'
ADMIN_MOBILE = '13501976895'

# NIGHT_CLUB_MOBILE = '15026920079'
NIGHT_CLUB_MOBILE = '13524265466'
MEIWEI_SERVICE_MOBILE = '13501976895'

REST_FRAMEWORK = {
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',
    'UNAUTHENTICATED_USER':
        'members.models.AnonymousMember',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'members.auth.authentication.TokenAuthentication',
        'members.auth.authentication.URLTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'members.auth.permissions.ReadOnly',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'helper.parsers.PlainTextParser',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.UnicodeJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'PAGINATE_BY': 10
}

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'cache-control'
)

RENREN_AUTH = {
    'api_key': '3cc7591abdf2424aa984f2d6dedfd4e0',
    'secret_key': '5c941d09b8984e5996ef39d7be57d3b1',
    'redirect_uri': 'http://api.clubmeiwei.com/members/renren/',
    'scope': ['read_user_blog', 'read_user_status', 'read_user_message'],
}

WEIBO_AUTH = {
    'api_key': '1465606341',
    'secret_key': '35651137c2e86ac0e5bf12a36c49e917',
    'redirect_uri': 'http://api.clubmeiwei.com/members/weibo/',
    'scope': ['friendships_groups_read', 'friendships_groups_write', 'statuses_to_me_read'],
}

try:
    LOCAL_SETTINGS #@UndefinedVariable 
except NameError:
    try:
        from settings_local import * #@UnusedWildImport
    except ImportError:
        pass
