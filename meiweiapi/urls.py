from django.conf.urls import patterns, include, url
# from django.views.generic.base import TemplateView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

handler500 = 'meiweiapi.views.server_error'

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    
    # url(r'^$', TemplateView.as_view(template_name="desktop/index.html")),
    
    url(r'^restaurants/', include('restaurants.urls')),
    url(r'^members/', include('members.urls')),
    url(r'^staffs/', include('staffs.urls')),
    url(r'^orders/', include('orders.urls')),
    url(r'^address/', include('address.urls')),
    url(r'^clients/', include('clients.urls')),
    url(r'^payments/', include('payments.urls')),
    url(r'^alipay/', include('alipay.urls')),
    # url(r'^danbao/', include('danbao.urls')),
    url(r'^wxpay/', include('wxpay.urls')),
    url(r'^staffservice/', include('staffservice.urls')),

#     url(r'^api/meiwei/restaurants/', include('restaurants.urls')),
#     url(r'^api/meiwei/members/', include('members.urls')),
#     url(r'^api/meiwei/staffs/', include('staffs.urls')),
#     url(r'^api/meiwei/orders/', include('orders.urls')),
#     url(r'^api/meiwei/clients/', include('clients.urls')),
#     url(r'^api/meiwei/payments/', include('payments.urls')),
)

from django.conf import settings
try:
    if settings.LOCAL_SETTINGS and settings.DEBUG:
        from django.conf.urls.static import static
        urlpatterns += static('/media/', document_root = settings.MEDIA_ROOT)
except:
    pass
