from django import http
from django.views.decorators.csrf import requires_csrf_token
import json

@requires_csrf_token
def server_error(request):
    return http.HttpResponseServerError(json.dumps({'error': 'Server Error (500)'}), 
                                        content_type="application/json")
