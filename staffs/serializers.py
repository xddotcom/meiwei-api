
from rest_framework import serializers
from restaurants.models import Restaurant, TextInfor
from restaurants.serializers import SimpleRestaurantSerializer
from orders.serializers import OrderSerializer

class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextInfor
        fields = ('textid', 'chinese', 'english')
        read_only_fields = ('textid',)
        depth = 0


class StaffOrderSerializer(OrderSerializer):
    restaurantinfor = SimpleRestaurantSerializer(many=False, source='restaurant', read_only = True)


class StaffRestaurantSerializer(serializers.HyperlinkedModelSerializer):
    fullname = TextSerializer(required = False)
    shortname = TextSerializer(required = False)
    description = TextSerializer(required = False)
    address = TextSerializer(required = False)
    discount = TextSerializer(required = False)
    parking = TextSerializer(required = False)
    workinghour = TextSerializer(required = False)
    
    frontpic = serializers.Field(source='frontpic_fullpath')
    
    class Meta:
        model = Restaurant
        fields = ('id', 'fullname', 'shortname', 'description', 'address', 'parking', 'workinghour', 'discount', 'frontpic')
        depth = 0
