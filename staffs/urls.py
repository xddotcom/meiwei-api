
from django.conf.urls import patterns, include, url
from rest_framework import routers
from staffs.apiviews import RestaurantViewSet, OrderViewSet, OrderAirportViewSet, StaffOrderViewSet

router = routers.DefaultRouter()

router.register(r'restaurant', RestaurantViewSet, base_name="staff_restaurant")
router.register(r'order', OrderViewSet, base_name="staff_order")
router.register(r'staffadminorder', StaffOrderViewSet, base_name="staff_admin_order")
router.register(r'orderairport', OrderAirportViewSet, base_name="staff_orderairport")

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
)
