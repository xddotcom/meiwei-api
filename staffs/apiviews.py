
from django.db import transaction
from members.auth import permissions
from orders.models import Order, OrderAirport
from orders.serializers import GenericOrderSerializer
from orders.notify import sendOrderFulfilledSMS, sendOrderCancelSMS, sendOrderConfirmSMS, sendNightClubOrderConfirmSMS, sendOrderCancelSMS2Meiwei
from staffs.serializers import StaffOrderSerializer
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.permissions import BasePermission
from restaurants.models import Restaurant, TextInfor
from staffs.serializers import StaffRestaurantSerializer
from staffs.exceptions import StaffException


class RestaurantViewSet(viewsets.mixins.RetrieveModelMixin,
                        viewsets.mixins.ListModelMixin,
                        viewsets.mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):
    queryset = Restaurant.objects.all().order_by('-score')
    serializer_class = StaffRestaurantSerializer
    permission_classes = (permissions.IsStaff,)
    paginate_by = 10
    
    def get_queryset(self):
        queryset = self.queryset.filter(staff=self.request.user)
        return queryset
    
    def update(self, request, *args, **kwargs):
        updatable_fields = ['fullname', 'shortname', 'description', 'description', 'address', 'discount', 'parking', 'workinghour']
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.DATA)
        if serializer.is_valid():
            update_fields = []
            try:
                with transaction.commit_on_success():
                    for field in updatable_fields:
                        value = request.DATA.get(field)
                        #TODO: Inconsistent with serializer validation. BUT serializer.data is cleaned by to_native.
                        if value:
                            update_fields.append(field)
                            text = TextInfor.objects.create(**value)
                            setattr(self.object, field, text)
                    self.object.save(update_fields=update_fields)
                    return Response({}, status=status.HTTP_200_OK)
            except StaffException as e:
                return Response(e.detail, status=e.status_code)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OrderViewSet(viewsets.mixins.RetrieveModelMixin,
                   viewsets.mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Order.objects.all().order_by('orderdate', 'ordertime', 'id')
    serializer_class = StaffOrderSerializer
    permission_classes = (permissions.IsAdmin,)
    paginate_by = 100
    
    def get_queryset(self):
        queryset = self.queryset
        # if self.request.user.is_staff:
        #     queryset = queryset.filter(restaurant__staff=self.request.user)
        params = self.request.QUERY_PARAMS
        if 'status' in params:
            status = params['status']
            if status == 'new':
                queryset = queryset.filter(status = 0)
            elif status == 'pending':
                queryset = queryset.filter(status__gt = 0, status__lt = 30)
            elif status == 'fulfilled':
                queryset = queryset.filter(status__gte = 30, status__lte = 50).order_by('-orderdate', '-ordertime', '-id')
        return queryset
    
    @action(methods=['put'])
    def confirm(self, request, pk=None):
        order = self.get_object()
        order.confirm()
        if order.restaurant.restaurant_type == 20:
            sendNightClubOrderConfirmSMS(order)
        else:
            sendOrderConfirmSMS(order)
        return Response({}, status=status.HTTP_204_NO_CONTENT)
    
    @action(methods=['put'])
    def save_amount(self, request, pk=None):
        amount = float(request.DATA['amount'])
        order = self.get_object()
        order.save_amount(amount)
        sendOrderFulfilledSMS(order)
        return Response({}, status=status.HTTP_204_NO_CONTENT)
    
    @action(methods=['put'])
    def cancel(self, request, pk=None):
        order = self.get_object()
        order.cancel(staff = True)
        sendOrderCancelSMS(order)
        return Response({}, status=HTTP_204_NO_CONTENT)


class IsRestaurantStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_staff
    def has_object_permission(self, request, view, obj):
        try:
            return obj.staff.id == request.user.id
        except:
            try:
                return obj.restaurant.staff.id == request.user.id
            except:
                return False


class StaffOrderViewSet(viewsets.mixins.RetrieveModelMixin,
                   viewsets.mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Order.objects.all().order_by('orderdate', 'ordertime', 'id')
    serializer_class = StaffOrderSerializer
    permission_classes = (IsRestaurantStaff,)
    paginate_by = 100

    def get_queryset(self):
        queryset = self.queryset
        if self.request.user.is_staff:
            queryset = queryset.filter(restaurant__staff=self.request.user)
        params = self.request.QUERY_PARAMS
        if 'status' in params:
            status = params['status']
            if status == 'new':
                queryset = queryset.filter(status = 0)
            elif status == 'pending':
                queryset = queryset.filter(status__gt = 0, status__lt = 30)
            elif status == 'fulfilled':
                queryset = queryset.filter(status__gte = 30, status__lte = 50).order_by('-orderdate', '-ordertime', '-id')
            for order in queryset:
                l = len(order.contactphone)
                order.contactphone = order.contactphone[l-4:]
                order.consumeamount = None
        return queryset

    @action(methods=['put'])
    def confirm(self, request, pk=None):
        order = self.get_object()
        order.confirm()
        if order.restaurant.restaurant_type == 20:
            sendNightClubOrderConfirmSMS(order)
        else:
            sendOrderConfirmSMS(order)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    @action(methods=['put'])
    def save_amount(self, request, pk=None):
        amount = float(request.DATA['amount'])
        order = self.get_object()
        order.save_amount(amount)
        sendOrderFulfilledSMS(order)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    @action(methods=['put'])
    def cancel(self, request, pk=None):
        reason = request.DATA['reason']
        order = self.get_object()
        order.cancelreason = reason
        order.cancel(staff=True)
        sendOrderCancelSMS(order)
        sendOrderCancelSMS2Meiwei(order)
        return Response({}, status=HTTP_204_NO_CONTENT)

class IsVVIPStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_vvip_staff
    def has_object_permission(self, request, view, obj):
        return True

class OrderAirportViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsVVIPStaff,)
    queryset = OrderAirport.objects.all()
    serializer_class = GenericOrderSerializer
    paginate_by = 10

    def get_queryset(self):
        queryset = self.queryset
        params = self.request.QUERY_PARAMS
        status = params.get('status')
        if status: queryset = queryset.filter(status = status)

        return queryset
    
    @action(methods=['put'])
    def confirm(self, request, pk=None):
        order = self.get_object()
        order.confirm()
        return Response({}, status=status.HTTP_204_NO_CONTENT)
    
    @action(methods=['put'])
    def complete(self, request, pk=None):
        order = self.get_object()
        order.complete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)
