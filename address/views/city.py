from address.models import Province, City, Area
from address.serializers import ProvinceSerializer, CitySerializer, AreaSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet


class ProvinceViewSet(ReadOnlyModelViewSet):
    permission_classes = []
    serializer_class = ProvinceSerializer
    queryset = Province.objects.all()
    paginate_by = 100

    def get_queryset(self):
        return self.queryset


class CityViewSet(ReadOnlyModelViewSet):
    permission_classes = []
    serializer_class = CitySerializer
    queryset = City.objects.all()
    paginate_by = 100

    def get_queryset(self):
        params = self.request.QUERY_PARAMS
        if 'father' in params:
            father = params['father']
            self.queryset = self.queryset.filter(father=father)
        return self.queryset


class AreaViewSet(ReadOnlyModelViewSet):
    permission_classes = []
    serializer_class = AreaSerializer
    queryset = Area.objects.all()
    paginate_by = 100

    def get_queryset(self):
        params = self.request.QUERY_PARAMS
        if 'father' in params:
            father = params['father']
            self.queryset = self.queryset.filter(father=father)
        return self.queryset