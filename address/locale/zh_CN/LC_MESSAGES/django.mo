��    A      $  Y   ,      �     �     �     �  
   �     �  
   �     �     �     �          %     4     E     R     a     j     v     �     �     �  
   �     �     �     �     �     �       "        7     >     L     [     k     s     z     �     �     �     �     �     �     �     �     �     �            !   0  #   R  "   v     �     �     �  
   �     �     �     �      	  !   $	  !   F	  "   h	     �	  
   �	     �	  d  �	  	        "     +     8     E     Y     j     {     �     �     �     �     �  
   �     �               .     ?     P  
   a     l  
   }     �  	   �     �     �     �  	   �     �     �       	        "     )     6     C     Y     `     m     z     �     �     �     �     �     �     �       ;        X     e     x     �     �     �     �     �     �     �  '   �     &     <     O        	      &           >   6   3                   5              7                    1   #   ?          (              *   
   A   -      <          :              4   %              '       @   =         )   ;       +       2       8   !      0      ,   /      .   9                             "                   $       0. Available 0. Naive 0. New 0. Payable 10. Confirmed 10. PingAn 10. Resto Confirmed 20. Member Present 20. Restaurant 30. Fulfilled, Resto Payable 40. Resto Paid 41. Resto Unpaid 50. Complete 50. Successful 50. Used 90. Expired 96. Member Absent 97. Admin Cancelled 98. Resto Cancelled 99. Cancelled 99. Failed 99. Member Cancelled 99. Misc Airport Anshifu Anshifu Order Anshifu Orders Contact's information is required. Coupon Coupon Detail Coupon Details Coupon Invalid. Coupons Female Generic Order Generic Orders Invalid operation. Male Order Attribute Order Attributes Payable Link Payment Payments Ping'An Order Ping'An Orders PingAn Please add at least one guest Please complete your information. Please input a valid mobile number. Please let us know 24 hours before Product Product Order Product Orders Restaurant Restaurant Order Restaurant Orders Restaurant Reservation The transaction amount is wrong. This order couldn't be cancelled. This order couldn't be confirmed. Time invalid, please choose again. Too many people. VVIP Order VVIP Orders Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-05-07 11:12+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 0. 有效 0. Naive 0. 新订单 0. 待支付 10. 订单已确认 10. 平安银行 10. 餐厅确认 20. 客人出席 20. 餐厅优惠券 30. 消费完成，佣金待付 40. 佣金已付 41. 佣金未付 50. 订单完成 50. 成功 50. 已使用 90. 已过期 96. 客人未出席 97. 美位取消 98. 餐厅取消 99. 订单取消 99. 失败 99. 用户取消 99. 其他 机场服务 安师傅 安师傅订单 安师傅订单 请填写联系人信息。 优惠券 优惠券详情 优惠券详情 优惠券无效。 优惠券 小姐 通用订单 通用订单 订单操作无效。 先生 订单属性 订单属性 支付链接 支付 支付 平安银行订单 平安银行订单 平安银行 请至少添加一名贵宾 请完善您的预订信息。 请输入手机号码。 请至少在飞机起飞前24小时提醒我们陪同人数 增值服务 增值服务订单 增值服务订单 餐厅 餐厅预订 餐厅预订 餐厅预订 交易金额错误 无法取消该订单。 无法确认该订单。 订单时间无效，请重新选择。 订单人数过多。 机场服务订单 机场服务订单 