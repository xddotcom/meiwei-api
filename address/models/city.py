from django.db import models


class Province(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    province_id = models.CharField(db_column='provinceid', max_length=10)
    province = models.CharField(db_column='province', max_length=20)

    class Meta:
        app_label = 'orders'
        db_table = u'hat_province'

    def __unicode__(self):
        return unicode(self.province)


class City(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    city_id = models.CharField(db_column='cityid', max_length=10)
    city = models.CharField(db_column='city', max_length=20)
    father = models.CharField(db_column='father', max_length=10)

    class Meta:
        app_label = 'orders'
        db_table = u'hat_city'

    def __unicode__(self):
        return unicode(self.city)


class Area(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    area_id = models.CharField(db_column='areaid', max_length=10)
    area = models.CharField(db_column='area', max_length=20)
    father = models.CharField(db_column='father', max_length=10)

    class Meta:
        app_label = 'orders'
        db_table = u'hat_area'

    def __unicode__(self):
        return unicode(self.area)