from django.conf.urls import patterns, url, include
from address.views import ProvinceViewSet, CityViewSet, AreaViewSet
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'provinces', ProvinceViewSet, base_name="province")
router.register(r'cities', CityViewSet, base_name="city")
router.register(r'areas', AreaViewSet, base_name="area")

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
)
