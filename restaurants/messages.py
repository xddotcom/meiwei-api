
# encoding=utf-8

messages = {

'Admin Product Redeem Subject':
u'【积分兑换】 {member} {reason}',

'Admin Product Redeem Content':
u"""
用户ID {id}
用户名 {username}
邮箱 {email}
手机 {mobile}
http://api.clubmeiwei.com/admin/members/credit/?credit_type__exact=6
""",

} # END messages dict
