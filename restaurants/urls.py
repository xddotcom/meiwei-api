
from django.conf.urls import patterns, include, url
from rest_framework import routers
from restaurants.apiviews import RestaurantViewSet, DistrictViewSet, \
    CircleViewSet, CuisineViewSet, RecommendViewSet, SimpleRecommendViewSet, \
    ProductItemViewSet, ProductViewSet, FiltersViewSet

router = routers.DefaultRouter()

router.register(r'restaurant', RestaurantViewSet)
router.register(r'district', DistrictViewSet)
router.register(r'circle', CircleViewSet)
router.register(r'cuisine', CuisineViewSet)
router.register(r'recommend', RecommendViewSet, base_name="recommend")
router.register(r'recommendname', SimpleRecommendViewSet, base_name="recommend_name_list")
router.register(r'productitem', ProductItemViewSet)
router.register(r'product', ProductViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^filters/$', FiltersViewSet.as_view()),
)
