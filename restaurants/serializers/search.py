
from rest_framework import serializers
from restaurants.models import RestaurantSearch


class RestaurantSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = RestaurantSearch
        fields = ('id', 'latitude', 'longitude')
        depth = 0
