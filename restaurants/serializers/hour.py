
from rest_framework import serializers
from restaurants.models import Hour
from textinfor import TextSerializer


class HourSerializer(serializers.ModelSerializer):
    timename = TextSerializer()
    discount = TextSerializer()
    class Meta:
        model = Hour
        fields = ('starttime', 'endtime', 'days', 'timename', 'discount')
        depth = 0
