from rest_framework import serializers
from restaurants.models import ProductItem, Product
from restaurants.serializers.picture import ProductPictureSerializer
from textinfor import TextSerializer


class ProductItemSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    description = TextSerializer()
    picture = serializers.Field(source='picture_fullpath')

    productpicture_set = ProductPictureSerializer(many=True, read_only=True)

    class Meta:
        model = ProductItem
        fields = ('id', 'name', 'picture', 'productpicture_set', 'count', 'credit', 'price', 'description')
        depth = 0


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    description = TextSerializer()
    productitem_set = ProductItemSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'order', 'productitem_set', 'description')
        depth = 0
