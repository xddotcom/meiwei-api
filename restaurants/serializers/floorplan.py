
from rest_framework import serializers
from restaurants.models import Floorplan
from textinfor import TextSerializer


class FloorplanSerializer(serializers.ModelSerializer):
    caption = TextSerializer()
    path = serializers.Field(source='fullpath')
    class Meta:
        model = Floorplan
        fields = ('id', 'path', 'caption')
        depth = 0
