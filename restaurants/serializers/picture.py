from rest_framework import serializers
from restaurants.models import Picture, ProductPicture


class PictureSerializer(serializers.ModelSerializer):
    # caption = TextSerializer() //Picture caption is not TextInfor !
    path = serializers.Field(source='fullpath')

    class Meta:
        model = Picture
        fields = ('path', 'caption')
        depth = 0


class ProductPictureSerializer(serializers.ModelSerializer):
    # caption = TextSerializer() //Picture caption is not TextInfor !
    path = serializers.Field(source='fullpath')

    class Meta:
        model = ProductPicture
        fields = ('path', 'caption')
        depth = 0
