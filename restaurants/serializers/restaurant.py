
from floorplan import FloorplanSerializer
from picture import PictureSerializer
from rest_framework import serializers
from restaurants.models import Restaurant
from search import RestaurantSearchSerializer
from textinfor import TextSerializer


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
    # pictures = serializers.HyperlinkedIdentityField(view_name='restaurant-pictures')
    # floorplans = serializers.HyperlinkedIdentityField(view_name='restaurant-floorplans')
    menus = serializers.HyperlinkedIdentityField(view_name='restaurant-menus')
    reviews = serializers.HyperlinkedIdentityField(view_name='restaurant-reviews')
    hours = serializers.HyperlinkedIdentityField(view_name='restaurant-hours')
    # hours_per_day = serializers.Field(source='simple_hours')
    
    fullname = TextSerializer(required=False)
    shortname = TextSerializer(required=False)
    description = TextSerializer(required=False)
    address = TextSerializer(required=False)
    discount = TextSerializer(required=False)
    parking = TextSerializer(required=False)
    workinghour = TextSerializer(required=False)
    
    frontpic = serializers.Field(source='frontpic_fullpath')
    # price = serializers.Field(source='price_range')
    pictures = PictureSerializer(many=True, source='picture_set', read_only=True)
    floorplans = FloorplanSerializer(many=True, source='floorplan_set', read_only=True)
    coordinate = RestaurantSearchSerializer(source="restaurantsearch", read_only=True)
    
    reviews_control = serializers.Field(source='reviews_control')

    class Meta:
        model = Restaurant
        fields = ('id', 'fullname', 'shortname','restaurant_type', 'premium', 'description', 'address', 'parking', 'workinghour', 'discount', 'frontpic',
                  'score', 'price', 'pictures', 'floorplans', 'hours', 'menus', 'coordinate', 'reviews', 'reviews_control')
                    # , 'hours_per_day')
        depth = 0


class SimpleRestaurantSerializer(serializers.HyperlinkedModelSerializer):
    fullname = TextSerializer()
    address = TextSerializer()
    discount = TextSerializer()
    frontpic = serializers.Field(source='frontpic_fullpath')
    class Meta:
        model = Restaurant
        fields = ('id', 'fullname', 'address', 'discount', 'frontpic', 'premium')
        depth = 0
