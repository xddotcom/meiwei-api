
from rest_framework import serializers
from restaurant import RestaurantSerializer
from restaurants.models import Recommend, RecommendItem, MemberRecommend
from textinfor import TextSerializer


class MemberRecommendItemSerializer(serializers.ModelSerializer):
    restaurant = RestaurantSerializer(many=False)
    class Meta:
        model = MemberRecommend
        fields = ('id', 'restaurant')
        depth = 0


class RecommendItemSerializer(serializers.ModelSerializer):
    restaurant = RestaurantSerializer(many=False)
    class Meta:
        model = RecommendItem
        fields = ('id', 'order', 'restaurant')
        depth = 0


class RecommendSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    #recommenditem_set = RecommendItemSerializer(source='top_recommenditems', many=True)
    recommenditem_set = serializers.SerializerMethodField('get_recommenditems')
    
    class Meta:
        model = Recommend
        fields = ('id', 'isrecommended', 'order', 'name', 'recommenditem_set')
        depth = 0
    
    def get_recommenditems(self, obj):
        if obj.id == 5:
            request = self.context['request']
            member = request.user if request.user.is_authenticated() else None
            if request.user.is_authenticated():
                member = request.user
                recommends = MemberRecommend.objects.filter(member = member)
                items = [MemberRecommendItemSerializer(item, context=self.context).data for item in recommends]
                if len(items) > 5:
                    return items
        return RecommendItemSerializer(obj.recommenditem_set.filter(order__lt=100),
                                       many=True,
                                       context=self.context).data


class SimpleRecommendSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    class Meta:
        model = Recommend
        fields = ('id', 'isrecommended', 'order', 'name')
        depth = 0
