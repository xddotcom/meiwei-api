
from rest_framework import serializers
from restaurants.models import TextInfor


class TextSerializer(serializers.ModelSerializer):
    def to_native(self, value):
        request = self.context.get('request', None)
        code = request.LANGUAGE_CODE.upper()
        if value:
            if 'CN' in code or 'ZH' in code:
                return value.chinese
            else:
                return value.english
        else:
            return value
    class Meta:
        model = TextInfor
        fields = ('textid', 'chinese', 'english')
        read_only_fields = ('textid',)
        depth = 0
