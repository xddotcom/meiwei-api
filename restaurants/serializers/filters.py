
from recommend import SimpleRecommendSerializer
from rest_framework import serializers
from restaurants.models import District, Circle, Cuisine
from textinfor import TextSerializer


class DistrictSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    city = serializers.RelatedField()
    class Meta:
        model = District
        fields = ('id', 'city', 'name', 'order')
        depth = 1


class CircleSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    district = DistrictSerializer()
    class Meta:
        model = Circle
        fields = ('id', 'isrecommended', 'order', 'name', 'district')
        depth = 1


class CuisineSerializer(serializers.HyperlinkedModelSerializer):
    name = TextSerializer()
    class Meta:
        model = Cuisine
        fields = ('id', 'name', 'order', 'type')
        depth = 1


class FiltersSerializer(serializers.Serializer):
    recommendnames = SimpleRecommendSerializer(many=True)
    circles = CircleSerializer(many=True)
    cuisines = CuisineSerializer(many=True)
