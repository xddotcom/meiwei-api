
from rest_framework import serializers
from restaurants.models import Menu
from textinfor import TextSerializer


class MenuSerializer(serializers.ModelSerializer):
    name = TextSerializer()
    description = TextSerializer()
    picture = serializers.Field(source='picture_fullpath')
    class Meta:
        model = Menu
        fields = ('id', 'name', 'picture', 'category', 'description', 'price')
        depth = 0
