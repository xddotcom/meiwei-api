
from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models
from django.forms import Textarea, Select
from django.utils.translation import ugettext_lazy as _
from restaurants.models import *  # @UnusedWildImport
import restaurants


class ImageFormMixin(object):
    def _max_size_kb_(self): return 100
    def _load_image_(self, fieldname):
        picture = self.cleaned_data.get(fieldname, False)
        if picture:
            return picture
        else:
            raise ValidationError(_("Couldn't read uploaded image"))
    def _valid_error_too_large_(self):
        return _("Image file too large ( > {max_size} kb )").format(max_size=self._max_size_kb_())
    def _valid_size_(self, fieldname):
        picture = self._load_image_(fieldname)
        if hasattr(picture, '_size') and picture._size > self._max_size_kb_() * 1024:
            raise ValidationError(self._valid_error_too_large_())
        return picture
    def clean_picture(self):
        return self._valid_size_('picture')
    def clean_frontpic(self):
        return self._valid_size_('frontpic')
    def clean_path(self):
        return self._valid_size_('path')


class BaseInline(admin.TabularInline):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs={'style': 'max-width: 150px'})},
    }


class HourInline(BaseInline):
    model = Hour
    extra = 1

class FloorplanInline(BaseInline):
    model = Floorplan
    extra = 1

class PictureInline(BaseInline):
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta: model = Picture
    form = CustomForm
    model = Picture
    extra = 1


class ProductPictureInline(BaseInline):
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta: model = ProductPicture
    form = CustomForm
    model = ProductPicture
    extra = 1

class MenuInline(BaseInline):
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta: model = Menu
    form = CustomForm
    model = Menu
    extra = 1

class RecommendItemInline(BaseInline):
    model = RecommendItem
    count = 3


class BaseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs={'style': 'max-width: 300px'})},
    }


class CityAdmin(BaseAdmin):
    list_display = ['id', 'cityname', 'country']

class DistrictAdmin(BaseAdmin):
    list_display = ['id', 'name', 'city', 'order']
    list_editable = ['order']

class CircleAdmin(BaseAdmin):
    def Restaurants(self, instance):
        items = instance.restaurant_set.all()
        return len(items) if items else ""
    list_display = ['name', 'order', 'Restaurants', 'district']
    list_editable = ['order', 'district']

class CuisineAdmin(BaseAdmin):
    def Restaurants(self, instance):
        items = instance.restaurant_set.all()
        return len(items) if items else ""
    list_display = ['id', 'name', 'order', 'type', 'Restaurants']
    list_editable = ['order']


class MenuAdmin(BaseAdmin):
    def picture_thumbnail(self, instance):
        return u'<img src="%s" height="50px" alt="" />' % instance.picture.url
    picture_thumbnail.allow_tags = True
    picture_thumbnail.short_description = "Thumbnail"
    list_display = ['picture_thumbnail', 'restaurant', 'price', 'name', 'category', 'isrecommended']
    list_display_links = ['picture_thumbnail']
    list_editable = ['restaurant', 'price']
    ordering = ['restaurant']


class FloorplanAdmin(BaseAdmin):
    list_display = ['caption', 'restaurant', 'path']
    list_display_links = ['caption', 'path']
    ordering = ['restaurant', 'caption']


class PictureAdmin(BaseAdmin):
    list_filter = ('restaurant',)


class HourAdmin(BaseAdmin):
    list_per_page = 200
    list_display = ['id', 'restaurant', 'days', 'starttime', 'endtime', 'discount']
    list_editable = ['days', 'starttime', 'endtime']


class RestaurantAdmin(BaseAdmin):
    list_per_page = 200
    list_display = ['id', 'fullname', 'isdeleted', 'premium', 'score', 'restaurant_type']
    list_editable = ['isdeleted', 'score', 'restaurant_type']
    list_display_links = ['fullname']
    list_filter = ('isdeleted', 'restaurant_type')

    inlines = [HourInline, FloorplanInline, PictureInline, MenuInline]
    
    def ViewPics(self, instance):
        items = instance.picture_set.all()
        return len(items) if items else ""
    def Floorplan(self, instance):
        items = instance.floorplan_set.all()
        return len(items) if items else ""
    def Menus(self, instance):
        items = instance.menu_set.all()
        return len(items) if items else ""
    def frontpic_thumbnail(self, instance):
        # return u'<img src="%s" height="20px" alt="" />' % instance.frontpic.url
        return "OK" if instance.frontpic else ""
    frontpic_thumbnail.allow_tags = True
    frontpic_thumbnail.short_description = "Thumbnail"
    
    fieldsets = (
        (None, {
            'fields': (('staff', 'isdeleted', 'premium'),
                       ('circle', 'cuisines')),
        }),
        (None, {
            'fields': (('fullname', 'shortname', 'description'),
                       ('address', 'parking', 'transport'),
                       ('workinghour', 'discount')),
        }),
        (None, {
            'fields': (('score', 'telephone', 'price', 'commissionrate'),),
        }),
        ('Profile Pictures', {
            # 'fields': ('frontpic', 'mappic', 'discountpic'),
            'fields': ('frontpic',),
        }),
    )
    
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta: model = Restaurant
    form = CustomForm


class RecommendItemAdmin(BaseAdmin):
    list_display = ['recommend', 'order', 'restaurant']
    list_display_links = ['recommend']
    list_editable = ['order', 'restaurant']
    ordering = ('recommend', 'order')


class RecommendAdmin(BaseAdmin):
    list_display = ['name', 'order', 'isrecommended']
    list_display_links = ['name']
    list_editable = ['order']
    ordering = ('order',)
    inlines = [RecommendItemInline]


class TextInforAdmin(BaseAdmin):
    list_per_page = 100
    fields = ['chinese', 'english']
    list_display = ['textid', 'available', 'chinese', 'english']
    list_display_links = ['textid']
    list_editable = ['chinese', 'english']
    list_filter = ('available',)
    ordering = ('available',)


class RestaurantSearchAdmin(BaseAdmin):
    list_per_page = 100
    list_display = ['id', 'restaurant', 'address', 'latitude', 'longitude', 'fulladdress', 'tile', 'keywords']
    list_editable = ['address', 'keywords']
    actions = ['reset', 'init', 'init_keywords', 'reset_tiles']
    def reset(self, request, queryset):
        for search in queryset:
            restaurants.reset_restaurant_search(search)
    reset.short_description = "Clear Coordinates and Keywords"
    def init(self, request, queryset):
        for search in queryset:
            restaurants.init_keywords(search)
            restaurants.init_coordinates(search)
            restaurants.init_tiles(search)
    init.short_description = "Generate Coordinates and Keywords"
    def init_keywords(self, request, queryset):
        for search in queryset:
            restaurants.init_keywords(search)
    init_keywords.short_description = "Re-Generate Keywords"
    def reset_tiles(self, request, queryset):
        for search in queryset:
            restaurants.init_tiles(search)
    reset_tiles.short_description = "Re-Calculate Tiles"
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs={'style': 'max-width: 300px'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 50})},
        models.CharField: {'widget': Textarea(attrs={'rows': 2, 'cols': 30})},
    }


class ProductItemAdmin(BaseAdmin):
    def product_thumbnail(self, instance):
        return u'<img src="%s" height="64px" alt="" />' % instance.picture.url
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta:
            model = ProductItem
    form = CustomForm
    product_thumbnail.allow_tags = True
    product_thumbnail.short_description = "Thumbnail"
    list_display = ['id', 'name', 'product', 'product_thumbnail', 'count', 'credit', 'price', 'order', 'description']
    list_display_links = ['id', 'name']
    list_editable = ['product', 'count', 'credit', 'price', 'order']
    list_filter = ('product', 'product__category',)
    inlines = [ProductPictureInline]


class ProductItemInline(BaseInline):
    class CustomForm(ImageFormMixin, forms.ModelForm):
        def _max_size_kb_(self): return 100
        class Meta: model = Picture
    form = CustomForm
    model = ProductItem
    extra = 1


class ProductAdmin(BaseAdmin):
    list_display = ['id', 'name', 'category', 'order', 'description']
    list_display_links = ['id', 'name']
    list_editable = ['category', 'order']
    #inlines = [ProductItemInline]


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(RestaurantSearch, RestaurantSearchAdmin)
admin.site.register(Recommend, RecommendAdmin)
admin.site.register(RecommendItem, RecommendItemAdmin)

admin.site.register(Circle, CircleAdmin)
admin.site.register(Cuisine, CuisineAdmin)

admin.site.register(TextInfor, TextInforAdmin)

admin.site.register(Menu, MenuAdmin)
admin.site.register(Picture, PictureAdmin)
admin.site.register(Floorplan, FloorplanAdmin)
admin.site.register(Hour, HourAdmin)

admin.site.register(City, CityAdmin)
admin.site.register(District, DistrictAdmin)

admin.site.register(ProductItem, ProductItemAdmin)
admin.site.register(Product, ProductAdmin)
