from django.db import transaction
from django.db.models import Sum
from django.http import Http404
from helper.functions import cut_words
from helper.globalmap import find_neighbours
from helper.mixins import CacheControlMixin
from members.auth import permissions
from members.exceptions import MemberPurchaseFail
from members.models import Credit
from rest_framework.decorators import link #, action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from restaurants.models import * #@UnusedWildImport
from restaurants.serializers import * #@UnusedWildImport
from restaurants.notify import sendProductRedeemMail
import logging
import warnings


searchlog = logging.getLogger('searchlog')


class RestaurantViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Restaurant.objects.all().order_by('-score')
    serializer_class = RestaurantSerializer
    paginate_by = 10

    def get_queryset(self):
        queryset = self.queryset
        params = self.request.QUERY_PARAMS

        recommend = params.get('recommend')
        if recommend: queryset = Recommend.objects.get(id=int(recommend)).restaurants.all().order_by('-score')

        district = params.get('district')
        if district: queryset = queryset.filter(circle__district__id=int(district)).order_by('-score')

        circle = params.get('circle')
        if circle: queryset = queryset.filter(circle__id=int(circle)).order_by('-score')

        cuisine = params.get('cuisine')
        if cuisine: queryset = queryset.filter(cuisines=int(cuisine)).order_by('-score')

        restaurant_type = params.get('restaurant_type')
        if restaurant_type: queryset = queryset.filter(restaurant_type=int(restaurant_type)).order_by('-score')

        keywords = params.get('keywords')
        if keywords:
            keywords = cut_words(keywords).replace("'", ' ').replace('"', ' ')
            queryset = queryset.select_related('restaurantsearch') \
                .extra(select={'relevance': "MATCH(restaurant_search.keywords) AGAINST ('%s')" % keywords},
                       where=['MATCH(restaurant_search.keywords) AGAINST (%s)'],
                       params=[keywords], order_by=['-relevance'])
            unicode(queryset.query)
            if not queryset:
                searchlog.error('no result for %s' % params['keywords'])
            else:
                searchlog.info('%d results for %s' % (len(queryset), params['keywords']))

        lat = params.get('lat')
        lng = params.get('lng')
        if lat and lng:
            lat = int(float(lat) * 100000)
            lng = int(float(lng) * 100000)
            tile_list = find_neighbours(lat, lng, 2000)
            if len(tile_list) > 0:
                like_list = " or ".join([("restaurant_search.tile like '%s'" % tile) for tile in tile_list])
                queryset = queryset.select_related('restaurantsearch').extra(where=[like_list])
                unicode(queryset.query)
        return queryset.filter(isdeleted=0)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(id__gt=1)
        self.object_list = self.filter_queryset(queryset)

        if not self.allow_empty and not self.object_list:
            warnings.warn(PendingDeprecationWarning)
            class_name = self.__class__.__name__
            error_msg = self.empty_error % {'class_name': class_name}
            raise Http404(error_msg)

        page = self.paginate_queryset(self.object_list)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(self.object_list, many=True)

        return Response(serializer.data)

    @link()
    def hours(self, request, pk=None):
        resto = Restaurant.objects.get(id=pk)
        return Response(resto.simple_hours())

    @link()
    def menus(self, request, pk=None):
        queryset = Restaurant.objects.get(id=pk).menu_set.all()
        serializer = MenuSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @link()
    def reviews(self, request, pk=None):
        from members.serializers import ReviewSerializer

        queryset = Restaurant.objects.get(id=pk).review_set.filter(approved=1)
        serializer = ReviewSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @link()
    def pictures(self, request, pk=None):
        queryset = Restaurant.objects.get(id=pk).picture_set.all()
        serializer = PictureSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @link()
    def floorplans(self, request, pk=None):
        queryset = Restaurant.objects.get(id=pk).floorplan_set.all()
        serializer = FloorplanSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class DistrictViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    paginate_by = 100


class CircleViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Circle.objects.all().order_by('district__order', 'district', 'order')
    serializer_class = CircleSerializer
    paginate_by = 100


class CuisineViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Cuisine.objects.filter(type=1).order_by('order')
    serializer_class = CuisineSerializer
    paginate_by = 100


class RecommendViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Recommend.objects.all().order_by('order')
    serializer_class = RecommendSerializer
    paginate_by = 10


class SimpleRecommendViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Recommend.objects.all().order_by('order')
    serializer_class = SimpleRecommendSerializer
    paginate_by = 100


class ProductItemViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = ProductItem.objects.all()
    serializer_class = ProductItemSerializer
    paginate_by = 100

    def get_queryset(self):
        queryset = self.queryset
        params = self.request.QUERY_PARAMS

        recommend = params.get('recommend')
        if recommend: queryset = queryset.filter(id__in=[27, 40, 43, 45, 46, 47])

        product_type = params.get('product_type')
        if product_type: queryset = queryset.filter(product=int(product_type))

        return queryset

    @link(permission_classes=[permissions.IsAuthenticated])
    def purchase(self, request, pk=None):
        member = request.user
        with transaction.commit_on_success():
            item = ProductItem.objects.get(id=pk)
            balance = Credit.objects.filter(member=member).aggregate(Sum('amount'))['amount__sum']
            if balance < item.credit:
                raise MemberPurchaseFail
            credit = Credit.objects.create(member=member,
                                           amount=-item.credit,
                                           credit_type=6,
                                           reason=str(item))
            sendProductRedeemMail(credit)
        return Response({'cost': item.credit, 'balance': balance - item.credit}, status=HTTP_200_OK)


class ProductViewSet(CacheControlMixin, ReadOnlyModelViewSet):
    queryset = Product.objects.all().order_by('order')
    serializer_class = ProductSerializer
    paginate_by = 100

    def get_queryset(self):
        queryset = self.queryset
        params = self.request.QUERY_PARAMS

        category = params.get('category')
        if category: queryset = queryset.filter(category=int(category))
        return queryset


class FiltersViewSet(CacheControlMixin, APIView):
    def get(self, request):
        class Filter(object):
            def __init__(self):
                self.recommendnames = Recommend.objects.all().order_by('order')
                self.circles = Circle.objects.all().order_by('district__order', 'district', 'order')
                self.cuisines = Cuisine.objects.all().order_by('order')

        instance = Filter()
        serializer = FiltersSerializer(instance, context={'request': request})
        return Response(serializer.data)
