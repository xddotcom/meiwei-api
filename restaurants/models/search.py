
# from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils.translation import ugettext_lazy as _
from restaurant import Restaurant


class RestaurantSearch(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    restaurant = models.OneToOneField(Restaurant, db_column='restaurant', null=True, blank=True)
    address = models.CharField(db_column='address', max_length=100, default='', null=True, blank=True)
    fulladdress = models.CharField(db_column='fulladdress', max_length=100, default='', null=True, blank=True)
    latitude = models.IntegerField(db_column='latitude', default=0, null=True, blank=True)
    longitude = models.IntegerField(db_column='longitude', default=0, null=True, blank=True)
    tile = models.CharField(db_column='tile', max_length=32, default='', null=True, blank=True, db_index=True)
    keywords = models.TextField(db_column='keywords', default='', null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = u'restaurant_search'
        verbose_name = _('RestaurantSearch')
        verbose_name_plural = _('RestaurantSearches')
    def __unicode__(self):
        return unicode(self.restaurant)


def create_restaurant_search(sender, instance, created, **kwargs):
    if created:
        search = RestaurantSearch(restaurant=instance, latitude=0, longitude=0, fulladdress='')
        if instance.address is not None: search.address = instance.address.chinese
        search.save()
