
# from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TextInfor(models.Model):
    textid = models.AutoField(primary_key=True, db_column='textId')
    available = models.CharField(db_column='available', max_length=20, default='', null=True, blank=True)
    chinese = models.TextField(db_column='chinese', default='', null=True, blank=True)
    english = models.TextField(db_column='english', default='', null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = u'text_infor'
        verbose_name = _('TextInfor')
        verbose_name_plural = _('TextInfors')
    def __unicode__(self):
        # return unicode(self.english) + "   " + unicode(self.chinese)
        return unicode(self.chinese)
