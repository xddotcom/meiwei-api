
from django.db import models
from django.utils.translation import ugettext_lazy as _

from textinfor import TextInfor


class City(models.Model):
    id = models.AutoField(primary_key=True, db_column='cityId')
    cityname = models.ForeignKey(TextInfor, db_column='cityName', related_name='cityname_set', null=True, blank=True, verbose_name='City Name')
    country = models.ForeignKey(TextInfor, db_column='country', related_name='countryname_set', null=True, blank=True, verbose_name='Country')
    class Meta:
        app_label = 'restaurants'
        db_table = 'base_city_infor'
        verbose_name = _('City')
        verbose_name_plural = _('Cities')
    def __unicode__(self):
        return unicode(self.cityname)


class District(models.Model):
    id = models.AutoField(primary_key=True, db_column='districtId')
    city = models.ForeignKey(City, db_column='city', null=True, blank=True)
    name = models.ForeignKey(TextInfor, db_column='districtName', null=True, blank=True)
    order = models.IntegerField(db_column='rank', default=0, null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = 'base_district_infor'
        verbose_name = _('District')
        verbose_name_plural = _('Districts')
    def __unicode__(self):
        return unicode(self.name)


class Circle(models.Model):
    id = models.AutoField(primary_key=True, db_column='circleId')
    name = models.ForeignKey(TextInfor, db_column='circleName', null=True, blank=True)
    district = models.ForeignKey(District, db_column='district', null=True, blank=True)
    isrecommended = models.IntegerField(db_column='isRecommended', null=True, blank=True)
    order = models.IntegerField(db_column='rank', null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = 'base_circle_infor'
        verbose_name = _('Circle')
        verbose_name_plural = _('Circles')
    def __unicode__(self):
        return unicode(self.name)


class Cuisine(models.Model):
    CUISINE_TYPE = ((1, _('Cuisine')), (2, _('Misc')))
    id = models.AutoField(primary_key=True, db_column='cuisineId')
    name = models.ForeignKey(TextInfor, db_column='cuisineName', null=True, blank=True)
    type = models.IntegerField(db_column='cuisineType', default=1, null=True, blank=True, choices=CUISINE_TYPE)
    isrecommended = models.IntegerField(db_column='isRecommended', default=1, null=True, blank=True)
    order = models.IntegerField(db_column='rank', default=0, null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = 'base_cuisine_infor'
        verbose_name = _('Cuisine')
        verbose_name_plural = _('Cuisines')
    def __unicode__(self):
        return unicode(self.name)
