
from django.db import models
from django.utils.translation import ugettext_lazy as _

from fields import rename, fullpath
from restaurant import Restaurant
from textinfor import TextInfor


class Menu(models.Model):
    def image_name(self, filename):
        return 'restaurant/' + rename(filename)
    
    id = models.AutoField(primary_key=True, db_column='menuId')
    isrecommended = models.IntegerField(db_column='isRecommended', default=1, null=True, blank=True, verbose_name='Is Recommended')
    category = models.IntegerField(db_column='menuCategory', default=0, null=True, blank=True)
    picture = models.ImageField(upload_to=image_name, db_column='menuPic', max_length=300, null=True, blank=True)
    price = models.FloatField(db_column='price', default=0, null=True, blank=True)
    description = models.ForeignKey(TextInfor, db_column='description', related_name='menu_description_set', null=True, blank=True)
    name = models.ForeignKey(TextInfor, db_column='menuName', related_name='menu_name_set', null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)
    
    def picture_fullpath(self):
        return fullpath(self.picture)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'restaurant_menu_infor'
        verbose_name = _('Menu')
        verbose_name_plural = _('Menus')
    
    def __unicode__(self):
        return unicode(self.picture)
