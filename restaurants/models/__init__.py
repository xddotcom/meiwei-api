from django.db.models.signals import post_save
import filters, floorplan, hour, menu, picture, product, recommend, restaurant, search, textinfor

post_save.connect(search.create_restaurant_search, sender=restaurant.Restaurant)

from filters import City, District, Circle, Cuisine
from floorplan import Floorplan
from hour import Hour
from menu import Menu
from picture import Picture, ProductPicture
from product import Product, ProductItem
from recommend import Recommend, RecommendItem, MemberRecommend
from restaurant import Restaurant
from search import RestaurantSearch
from textinfor import TextInfor
