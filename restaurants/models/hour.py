
from django.db import models
from django.utils.translation import ugettext_lazy as _

from restaurant import Restaurant
from textinfor import TextInfor


class Hour(models.Model):
    id = models.AutoField(primary_key=True, db_column='timeId')
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)
    starttime = models.TimeField(db_column='startTime', null=True, blank=True, verbose_name='Start Time')
    endtime = models.TimeField(db_column='endTime', null=True, blank=True, verbose_name='End Time')
    days = models.IntegerField(db_column='days', default=127, null=True, blank=True)
    timename = models.ForeignKey(TextInfor, db_column='timeName', related_name='hours_timename_set', null=True, blank=True)
    discount = models.ForeignKey(TextInfor, db_column='discount', related_name='hours_discount_set', null=True, blank=True)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'restaurant_time_infor'
        verbose_name = _('Hour')
        verbose_name_plural = _('Hours')
    
    def __unicode__(self):
        return unicode(self.timename)
