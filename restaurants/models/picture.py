
from django.db import models
from fields import rename, fullpath
from django.utils.translation import ugettext_lazy as _
from restaurant import Restaurant
from product import ProductItem


class Picture(models.Model):
    def image_name(self, filename):
        return 'restaurant/' + rename(filename)

    id = models.AutoField(primary_key=True, db_column='picId')
    path = models.ImageField(upload_to=image_name, db_column='bigPath', max_length=300, null=True, blank=True)
    displayorder = models.IntegerField(db_column='displayOrder', default=0, null=True, blank=True)
    caption = models.CharField(max_length=100, db_column='picTitle', null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)

    def fullpath(self):
        return fullpath(self.path)

    class Meta:
        app_label = 'restaurants'
        db_table = 'restaurant_pic_infor'
        verbose_name = _('Picture')
        verbose_name_plural = _('Pictures')

    def __unicode__(self):
        return unicode(self.path)


class ProductPicture(models.Model):
    def image_name(self, filename):
        return 'product/' + rename(filename)
    
    id = models.AutoField(primary_key=True, db_column='picId')
    path = models.ImageField(upload_to=image_name, db_column='bigPath', max_length=300, null=True, blank=True)
    displayorder = models.IntegerField(db_column='displayOrder', default=0, null=True, blank=True)
    caption = models.CharField(max_length=100, db_column='picTitle', null=True, blank=True)
    product = models.ForeignKey(ProductItem, db_column='product', null=True, blank=True)
    
    def fullpath(self):
        return fullpath(self.path)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'product_pic_infor'
        verbose_name = _('Product Picture')
        verbose_name_plural = _('Product Pictures')
    
    def __unicode__(self):
        return unicode(self.path)
