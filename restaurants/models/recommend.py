
# from django.utils.translation import ugettext_lazy as _
from django.db import models
from restaurant import Restaurant
from django.utils.translation import ugettext_lazy as _
from textinfor import TextInfor


class Recommend(models.Model):
    id = models.AutoField(primary_key=True, db_column='ruleId')
    isrecommended = models.IntegerField(db_column='isRecommended', default=0, null=True, blank=True, verbose_name='Display on Homepage')
    order = models.IntegerField(db_column='ruleOrder', default=999, null=True, blank=True, verbose_name='Display Order')
    name = models.ForeignKey(TextInfor, db_column='ruleName', null=True, blank=True)
    restaurants = models.ManyToManyField(Restaurant, through='RecommendItem')
    def top_recommenditems(self):
        return self.recommenditem_set.filter(order__lt=100)
    class Meta:
        app_label = 'restaurants'
        db_table = u'recommend_rule'
        verbose_name = _('Recommend')
        verbose_name_plural = _('Recommends')
        ordering = ['order']
    def __unicode__(self):
        return unicode(self.name)


class RecommendItem(models.Model):
    id = models.AutoField(primary_key=True, db_column='recommendId')
    order = models.IntegerField(db_column='rank', default=9999, null=True, blank=True)
    recommend = models.ForeignKey(Recommend, db_column='rankRule', null=True, blank=True, verbose_name='Recommend')
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)
    class Meta:
        app_label = 'restaurants'
        db_table = u'recommend_infor'
        verbose_name = _('RecommendItem')
        verbose_name_plural = _('RecommendItems')
        ordering = ['order']
    def __unicode__(self):
        return unicode(self.recommend)


class MemberRecommend(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    member = models.ForeignKey('members.Member', db_column='member', null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)
    score = models.IntegerField(db_column='score', default=0, null=True, blank=True)
    
    class Meta:
        app_label = 'restaurants'
        db_table = u'recommend_member'
        verbose_name = _('MemberRecommend')
        verbose_name_plural = _('MemberRecommends')
        index_together = [['member', 'score'],]
        ordering = ['member', '-score']
