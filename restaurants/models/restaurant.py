
from datetime import time
from django.db import models
from django.utils.translation import ugettext_lazy as _
from fields import rename, fullpath
from filters import Circle, Cuisine
from textinfor import TextInfor


class Restaurant(models.Model):
    VISIBILITY = ((0, _('Visible')), (1, _('Hidden')))
    
    RESTAURANT_TYPE = ((10, _('RESTAURANT')), (20, _('NIGHT CLUB')), (30, _('CLUB')))
    
    def image_name(self, filename):
        return 'restaurant/' + rename(filename)
    
    id = models.AutoField(primary_key=True, db_column='restaurantId')
    
    staff = models.ForeignKey('members.Member', db_column='memberadmin', null=True, blank=True, verbose_name=_('Restaurant Admin'))
    
    restaurant_type = models.IntegerField(db_column='restaurant_type', default=10, null=True, blank=True, choices=RESTAURANT_TYPE, verbose_name=_('Restaurant Type'))
    
    isdeleted = models.IntegerField(db_column='isDeleted', default=0, null=True, blank=True, choices=VISIBILITY, verbose_name=_('Visibility'))
    
    commissionrate = models.FloatField(db_column='commissionRate', default=0, null=True, blank=True, verbose_name=_('Commission Rate'))
    
    price = models.FloatField(db_column='perBegin', default=0, null=True, blank=True, verbose_name=_('Price'))
    
    score = models.FloatField(db_column='score', default=3, null=True, blank=True, verbose_name=_('Rating'))
    telephone = models.CharField(db_column='telephone', max_length=20, null=True, blank=True, verbose_name=_('Telephone'))
    
    frontpic = models.ImageField(upload_to=image_name, db_column='frontPic', max_length=300, null=True, blank=True, verbose_name=_('Picture'))
    
    circle = models.ForeignKey(Circle, db_column='circle', null=True, blank=True, verbose_name=_('Circle'))
    
    cuisines = models.ManyToManyField(Cuisine, db_column='cuisines', db_table='restaurant_cuisine', null=True, blank=True, verbose_name=_('Cuisines'))
    
    fullname = models.ForeignKey(TextInfor, db_column='fullName', related_name='restaurant_fullname_set', null=True, blank=True, verbose_name=_('Fullname'))
    shortname = models.ForeignKey(TextInfor, db_column='shortName', related_name='restaurant_shortname_set', null=True, blank=True, verbose_name=_('Shortname'))
    address = models.ForeignKey(TextInfor, db_column='address', related_name='restaurant_address_set', null=True, blank=True, verbose_name=_('Address'))
    description = models.ForeignKey(TextInfor, db_column='description', related_name='restaurant_description_set', null=True, blank=True, verbose_name=_('Description'))
    discount = models.ForeignKey(TextInfor, db_column='discount', related_name='restaurant_discount_set', null=True, blank=True, verbose_name=_('Discount'))
    parking = models.ForeignKey(TextInfor, db_column='parking', related_name='restaurant_parking_set', null=True, blank=True, verbose_name=_('Parking'))
    transport = models.ForeignKey(TextInfor, db_column='transport', related_name='restaurant_transport_set', null=True, blank=True, verbose_name=_('Transport'))
    workinghour = models.ForeignKey(TextInfor, db_column='workinghour', related_name='restaurant_workinghour_set', null=True, blank=True, verbose_name=_('Working Hour'))
    
    premium = models.BooleanField(db_column='premium', default=False, db_index=True, verbose_name=_('Premium'))
    
    def simple_hours(self):
        wholeday = sorted([time(h) for h in range(24)] + [time(h, 30) for h in range(24)])
        hours = self.hour_set.all()
        days = {i: [] for i in range(7)}
        if hours:
            for hour in hours:
                if hour.endtime < hour.starttime:
                    hour.endtime = time(23, 59, 59)
                mask = hour.days
                if hour.discount is None:
                    discount = ''
                else:
                    discount = unicode(hour.discount)
                day = 6
                while mask > 0:
                    if mask % 2 == 1:
                        for t in wholeday:
                            if t >= hour.starttime and t <= hour.endtime:
                                days[day].append((str(t)[:5], discount))
                    day -= 1
                    mask /= 2
        return days
    
    def frontpic_fullpath(self):
        return fullpath(self.frontpic)
    
    @property
    def reviews_control(self):
        return False
    
    def price_range(self):
        if self.price >= 5000:
            return 5
        elif self.price >= 400:
            return 4
        elif self.price >= 300:
            return 3
        elif self.price >= 200:
            return 2
        else:
            return 1
        
    class Meta:
        app_label = 'restaurants'
        db_table = 'restaurant_infor'
        verbose_name = _('Restaurant')
        verbose_name_plural = _('Restaurants')
    
    def __unicode__(self):
        return unicode(self.fullname)
