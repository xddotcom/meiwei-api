
from django.db import models
from django.utils.translation import ugettext_lazy as _
from fields import rename, fullpath
from textinfor import TextInfor


class Product(models.Model):
    CATEGORY = ((1, _('Value-added Service')),
                (2, _('Reward')),
                (3, _('VIP Card')),)
    
    id = models.AutoField(primary_key=True, db_column='productTypeId')
    category = models.IntegerField(db_column='category', default=0, null=True, blank=True, choices=CATEGORY)
    order = models.IntegerField(db_column='typeOrder', default=0, null=True, blank=True)
    name = models.ForeignKey(TextInfor, related_name='product_name_set', db_column='typeName', null=True, blank=True)
    description = models.ForeignKey(TextInfor, related_name='product_description_set', db_column='typeRemark', null=True, blank=True)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'producttype_infor'
        verbose_name = _('Product')
        verbose_name_plural = _('Product')
        ordering = ['order']
    
    def __unicode__(self):
        return unicode(self.name)


class ProductItem(models.Model):
    def image_name(self, filename):
        return 'product/' + rename(filename)
    
    id = models.AutoField(primary_key=True, db_column='productId')
    product = models.ForeignKey(Product, db_column='productType', null=True, blank=True)
    
    count = models.IntegerField(default=0, db_column='count', null=True, blank=True)
    credit = models.IntegerField(default=0, db_column='credit', null=True, blank=True)
    
    picture = models.ImageField(upload_to=image_name, db_column='picturePath', max_length=100, default='', null=True, blank=True)
    
    name = models.ForeignKey(TextInfor, db_column='productName', related_name='productitem_name_set', null=True, blank=True)
    price = models.IntegerField(db_column='price', default=0, null=True, blank=True)
    description = models.ForeignKey(TextInfor, db_column='productRemark', related_name='productitem_description_set', null=True, blank=True)
    
    order = models.IntegerField(db_column='order', default=0, null=True, blank=True)
    
    def picture_fullpath(self):
        return fullpath(self.picture)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'product_infor'
        verbose_name = _('Product Item')
        verbose_name_plural = _('Product Items')
        ordering = ['order']
    
    def __unicode__(self):
        return unicode(self.name)
