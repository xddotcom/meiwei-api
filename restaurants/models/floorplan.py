
from django.db import models
from django.utils.translation import ugettext_lazy as _

from fields import rename, fullpath
from restaurant import Restaurant
from textinfor import TextInfor


class Floorplan(models.Model):
    def svg_name(self, filename):
        return 'restaurant/table/' + rename(filename)
    
    id = models.AutoField(primary_key=True, db_column='tablePicId')
    picheight = models.IntegerField(db_column='picHeight', default=300, null=True, blank=True)
    picwidth = models.IntegerField(db_column='picWidth', default=300, null=True, blank=True)
    path = models.FileField(upload_to=svg_name, db_column='tablePicPath', max_length=300, null=True, blank=True, verbose_name='Floorplan Path')
    caption = models.ForeignKey(TextInfor, db_column='picName', null=True, blank=True, verbose_name='Floorplan Name')
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True)
    
    def fullpath(self):
        return fullpath(self.path)
    
    class Meta:
        app_label = 'restaurants'
        db_table = 'restaurant_tablepic_infor'
        verbose_name = _('Floorplan')
        verbose_name_plural = _('Floorplans')
    
    def __unicode__(self):
        return unicode(self.path)
