
from django.conf import settings
from helper.functions import send_mail
from restaurants.messages import messages
import logging

logger = logging.getLogger('meiweiapi')

if settings.DEBUG:
    ORDER_MANAGER = 'xindong.ding@clubmeiwei.com'
else:
    ORDER_MANAGER = 'neworder@clubmeiwei.com'


def sendProductRedeemMail(credit):
    try:
        subjectTPL = messages['Admin Product Redeem Subject']
        subject = subjectTPL.format(member=credit.member.username,
                                    reason=credit.reason.decode('utf-8'))
        contentTPL = messages['Admin Product Redeem Content']
        content = contentTPL.format(id=credit.member.id,
                                    username=credit.member.username,
                                    email=credit.member.profile.email,
                                    mobile=credit.member.profile.mobile)
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))
