from django.utils.encoding import smart_text, smart_str
from helper import jieba, pinyin
from helper.globalmap import calculate_tile_id
from restaurants.models import Restaurant, District, Circle, RestaurantSearch, \
    TextInfor, ProductItem
import json
import urllib
import urllib2

def reset_restaurant_search(search):
    resto = search.restaurant
    search.latitude = 0
    search.longitude = 0
    search.fulladdress = ''
    search.tile = ''
    search.keywords = ''
    if resto.address: search.address = resto.address.chinese
    search.save()

def init_keywords(search):
    restaurant = search.restaurant
    keywords = set()
    if restaurant.fullname is not None:
        keywords |= set(restaurant.fullname.english.split())
        keywords |= set(jieba.cut_for_search(restaurant.fullname.chinese))
        keywords |= {restaurant.fullname.chinese}
    if restaurant.circle is not None:
        keywords |= {restaurant.circle.name.chinese}
        keywords |= set(jieba.cut_for_search(restaurant.circle.district.name.chinese))
    for cuisine in restaurant.cuisines.all():
        keywords |= set(jieba.cut_for_search(cuisine.name.chinese))
    keywords |= {pinyin.get(keyword) for keyword in keywords}
    search.keywords = ' '.join(keywords)
    search.save()

def init_coordinates(search):
    url = 'http://restapi.amap.com/v3/geocode/geo'
    params = urllib.urlencode({'city': '021',
                               'address': smart_str(search.address),
                               'key': '88079b446671c954e1de335141228c28'})
    response = urllib2.urlopen(url, params)
    result = json.loads(response.read())
    lonlat = result['geocodes'][0]['location']
    search.longitude, search.latitude = [100000 * float(i) for i in lonlat.split(',')]
    url = 'http://restapi.amap.com/v3/geocode/regeo'
    params = urllib.urlencode({'city': '021',
                               'location': lonlat,
                               'key': '88079b446671c954e1de335141228c28'})
    response = urllib2.urlopen(url, params)
    result = json.loads(response.read())
    search.fulladdress = result['regeocode']['formatted_address']
    search.save()
    print search.id, lonlat

def init_tiles(search):
    search.tile = calculate_tile_id(search.latitude, search.longitude)
    search.save()

def export_frontpics():
    import shutil
    import os
    try:
        os.makedirs('D:/export/restaurant/')
    except:
        pass
    for resto in Restaurant.objects.all():
        pic = resto.frontpic
        try:
            src = pic.file.name
            dst = 'D:\\export\\restaurant\\%d.jpg' % resto.id
            print src, dst
            shutil.copy(src, dst)
        except:
            print 'fail -- ', resto.id

def export_prodpics():
    import shutil
    import os
    try:
        os.makedirs('D:/export/product/')
    except:
        pass
    for item in ProductItem.objects.all():
        pic = item.picture
        try:
            src = pic.file.name
            dst = 'D:\\export\\product\\%d.jpg' % item.id
            print src, dst
            shutil.copy(src, dst)
        except:
            print 'fail -- ', item.id
