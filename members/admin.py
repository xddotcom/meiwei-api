
from django.contrib import admin
from django.core import urlresolvers
from django.db import models
from django.forms import Select
from members.models import * # @UnusedWildImport

def change_link(instance, attr):
    if instance.__getattribute__(attr) is not None:
        return u'<a target="_blank" href="%s">Modify Text</a>' % (urlresolvers.reverse('admin:restaurants_textinfor_change',
                                                                                       args = (instance.__getattribute__(attr).textid,)))

class BaseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs = {'style': 'max-width: 150px'})},
    }


class BaseInline(admin.TabularInline):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs = {'style': 'max-width: 150px'})},
    }


class ReviewAdmin(BaseAdmin):
    list_display = ['id', 'member', 'restaurant', 'comments', 'approved', 'time_created']
    list_editable = ['approved']
    list_filter = ('restaurant', 'approved')


class FavoriteAdmin(BaseAdmin):
    list_display = ['id', 'member', 'restaurant']


class ContactAdmin(BaseAdmin):
    list_display = ['id', 'member', 'name', 'email', 'mobile', 'sexe']

class ContactInline(BaseInline):
    model = Contact
    extra = 0


class AnniversaryAdmin(BaseAdmin):
    list_display = ['id', 'member', 'date', 'description']

class AnniversaryInline(BaseInline):
    model = Anniversary
    extra = 0


class CreditAdmin(BaseAdmin):
    list_display = ['id', 'member', 'time_created', 'credit_type', 'amount', 'reason']
    list_editable = ['credit_type', 'amount', 'reason']
    list_filter = ('credit_type', )


class CreditInline(BaseInline):
    model = Credit
    extra = 0


class ProfileAdmin(BaseAdmin):
    list_display = ['id', 'member', 'avatar', 'nickname', 'email', 'mobile', 'sexe', 'anniversary', 'birthday', 'credit']

class ProfileInline(admin.StackedInline):
    model = Profile
    extra = 0

class TokenInline(admin.StackedInline):
    model = AuthToken
    extra = 0

class MemberAdmin(BaseAdmin):
    list_display = ['id', 'username', 'member_type', 'last_login', 'date_joined']
    list_filter = ('member_type',)
    readonly_fields = ('password',)
    inlines = [ProfileInline, ContactInline, TokenInline]
    def get_readonly_fields(self, request, obj=None):
        return [] if obj is None else super(MemberAdmin, self).get_readonly_fields(request, obj)
    def get_inline_instances(self, request, obj=None):
        return [] if obj is None else super(MemberAdmin, self).get_inline_instances(request, obj)
    def get_fieldsets(self, request, obj=None):
        fieldsets = [(None, {'fields': ('username', 'password')})]
        return fieldsets if obj is None else super(MemberAdmin, self).get_fieldsets(request, obj)
    def save_model(self, request, obj, form, change):
        if not change: obj.set_password(obj.password)
        obj.save()


admin.site.register(Member, MemberAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Favorite, FavoriteAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Credit, CreditAdmin)
admin.site.register(Anniversary, AnniversaryAdmin)
