
from members.models import Credit
from rest_framework import serializers


class CreditSerializer(serializers.ModelSerializer):
    member = serializers.RelatedField()
    class Meta:
        model = Credit
        fields = ('id', 'member', 'amount', 'credit_type', 'time_created', 'reason')
        depth = 0
