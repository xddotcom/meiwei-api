
from members.models import Profile
from rest_framework import serializers


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'member', 'email', 'mobile', 'memberno', 'nickname', 'avatar',
                  'sexe', 'anniversary', 'birthday', 'credit')
        read_only_fields = ('id', 'credit')
        depth = 0
