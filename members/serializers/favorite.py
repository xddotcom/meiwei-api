
from members.models import Favorite
from rest_framework import serializers
from restaurants.serializers import RestaurantSerializer


class FavoriteSerializer(serializers.ModelSerializer):
    member = serializers.RelatedField()
    restaurantinfor = RestaurantSerializer(many=False, source='restaurant', read_only=True)
    
    def save_object(self, obj, **kwargs):
        favorite, created = Favorite.objects.get_or_create(member=obj.member, restaurant=obj.restaurant)
        if not created:
            favorite.save()
    
    class Meta:
        model = Favorite
        fields = ('id', 'member', 'restaurant', 'restaurantinfor')
        depth = 0
