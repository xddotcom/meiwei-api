from address.models import Province
from address.serializers import ProvinceSerializer, AreaSerializer, CitySerializer
from members.models import Contact
from rest_framework import serializers


class ContactSerializer(serializers.ModelSerializer):
    member = serializers.RelatedField()
    province_info = serializers.SerializerMethodField('get_province_info')
    city_info = serializers.SerializerMethodField('get_city_info')
    area_info = serializers.SerializerMethodField('get_area_info')
    email = serializers.EmailField(required=False)
    mobile = serializers.CharField(required=True)
    # province = serializers.IntegerField(required=True)
    # city = serializers.IntegerField(required=True)
    # area = serializers.IntegerField(required=True)
    name = serializers.CharField(required=True)
    address = serializers.CharField(required=True)
    zipcode = serializers.CharField(required=False)

    class Meta:
        model = Contact
        fields = (
            'id', 'member', 'email', 'name', 'sexe', 'mobile', 'address', 'province', 'city', 'area', 'province_info',
            'city_info', 'area_info', 'blood_type', 'relationship', 'career', 'habit', 'revenue', 'character',
            'receive_time', 'color')
        depth = 0

    def get_province_info(self, obj):
        if obj.province:
            return ProvinceSerializer(obj.province, context=self.context).data

    def get_city_info(self, obj):
        if obj.city:
            return CitySerializer(obj.city, context=self.context).data

    def get_area_info(self, obj):
        if obj.area:
            return AreaSerializer(obj.area, context=self.context).data

    def save_object(self, obj, **kwargs):
        request = self.context['request']
        print(request.POST)
        return
        if obj.province:
            obj.type = 1
        else:
            obj.type = 0
        super(ContactSerializer, self).save_object(obj, **kwargs)
