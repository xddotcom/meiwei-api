
from members.models import Anniversary
from rest_framework import serializers


class AnniversarySerializer(serializers.ModelSerializer):
    member = serializers.RelatedField()
    class Meta:
        model = Anniversary
        fields = ('id', 'member', 'date', 'description')
        depth = 0
