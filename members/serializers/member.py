
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from helper.functions import is_email, is_mobile_no
from members.models import Member
from profile import ProfileSerializer
from rest_framework import serializers


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        if username and password:
            member = authenticate(username=username, password=password)
            if member:
                if not member.is_active:
                    raise serializers.ValidationError(_('User account is disabled.'))
                attrs['member'] = member
                return attrs
            else:
                raise serializers.ValidationError(_('Unable to login with provided credentials.'))
        else:
            raise serializers.ValidationError(_('Must include "username" and "password"'))


class MemberSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(many=False, read_only=True)
    class Meta:
        model = Member
        fields = ('id', 'username', 'password', 'member_type', 'last_login', 'date_joined', 'profile')
        read_only_fields = ('id', 'member_type', 'last_login', 'date_joined')
        depth = 0
    
    def validate_username(self, attrs, source):
        value = attrs[source]
        if not is_email(value) and not is_mobile_no(value):
            raise serializers.ValidationError(_('Please enter a valid email or phone number as username.'))
        return attrs
