
from members.models import Review
from rest_framework import serializers


class ReviewSerializer(serializers.ModelSerializer):
    member = serializers.RelatedField()
    restaurant = serializers.RelatedField()
    class Meta:
        model = Review
        fields = ('id', 'member', 'restaurant', 'comments', 'time_created',
                  'score', 'service_score', 'ambience_score', 'food_score', 'price')
        depth = 0
