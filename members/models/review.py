# coding:utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from member import Member
from restaurants.models import Restaurant


class Review(models.Model):
    CHECK = ((0, _('Unapproved')), (1, _('Approved')))
    id = models.AutoField(primary_key=True, db_column='reviewId')
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True, verbose_name=u'会员名')
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True, verbose_name=u'餐厅')

    comments = models.CharField(db_column='comments', max_length=1000, default='', null=True, blank=True,
                                verbose_name=u'评论内容')
    approved = models.IntegerField(db_column='isChecked', default=1, null=True, blank=True, choices=CHECK,
                                   verbose_name=u'是否已审核')
    time_created = models.DateTimeField(db_column='reviewTime', auto_now_add=True, null=True, blank=True,
                                        verbose_name=u'创建时间')

    score = models.IntegerField(db_column='score', default=3, null=True, blank=True, verbose_name=u'分数')
    service_score = models.IntegerField(db_column='serviceScore', default=3, null=True, blank=True, verbose_name=u'服务分')
    ambience_score = models.IntegerField(db_column='environmentScore', default=3, null=True, blank=True,
                                         verbose_name=u'环境分')
    food_score = models.IntegerField(db_column='tasteScore', default=3, null=True, blank=True, verbose_name=u'食物分')
    price = models.IntegerField(db_column='priceHigh', default=0, null=True, blank=True, verbose_name=u'最高价格')
    PRICE_NOT_USEFUL = models.IntegerField(db_column='priceLow', default=0, null=True, blank=True, verbose_name=u'最低价格')

    class Meta:
        app_label = 'members'
        db_table = u'member_review_infor'
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')
