from django.db import models
from django.utils.translation import ugettext_lazy as _
from hashlib import sha1
from helper.functions import make_password, make_random_password
import hmac
import uuid


class MemberManager(models.Manager):
    def create_with_email(self, email):
        pass

    def create_with_mobile(self, email):
        pass


class Member(models.Model):
    MEMBER_TYPE = ((0, _('0. Basic')),
                   (1, _('1. Restaurant Staff')),
                   (2, _('2. Super Admin')),
                   (3, _('3. VVIP Staff')),
                   (4, _('4. Coupon Validator')),
    )
    id = models.AutoField(primary_key=True, db_column='memberId')
    username = models.CharField(db_column='loginName', max_length=255, unique=True, verbose_name=_('Username'))
    password = models.CharField(db_column='loginPassword', max_length=128, verbose_name=_('Password'))

    last_login = models.DateTimeField(db_column='lastTime', auto_now=True, null=True, blank=True)
    date_joined = models.DateTimeField(db_column='registerTime', auto_now_add=True, null=True, blank=True)

    member_type = models.IntegerField(db_column='memberType', default=0, null=True, blank=True, choices=MEMBER_TYPE)
    inactive = models.IntegerField(db_column='isDeleted', default=0, null=True, blank=True)
    hashcode = models.CharField(db_column='hashCode', max_length=255, null=True, blank=True)
    validate_code = models.CharField(db_column='validate_code', max_length=12, null=True, blank=True)
    status = models.IntegerField(db_column='status', null=True, default=0)
    objects = MemberManager()

    @property
    def is_active(self): return self.inactive == 0

    @property
    def is_basic(self): return self.member_type == 0

    @property
    def is_staff(self): return self.member_type == 1

    @property
    def is_admin(self): return self.member_type == 2

    @property
    def is_vvip_staff(self): return self.member_type == 3

    @property
    def is_coupon_validator(self): return self.member_type == 4

    def get_username(self): return self.username

    def natural_key(self): return (self.get_username(),)

    def is_anonymous(self): return False

    def is_authenticated(self): return True

    def set_password(self, raw_password):
        if raw_password is None:
            raw_password = make_random_password()
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        return make_password(raw_password) == self.password

    def set_unusable_password(self):
        self.password = ''

    def has_usable_password(self):
        return self.password != ''

    def __unicode__(self):
        return unicode(self.username)

    def __str__(self):
        return str(self.username)

    class Meta:
        app_label = 'members'
        db_table = u'member_infor'
        verbose_name = _('Member')
        verbose_name_plural = _('Members')


class AuthToken(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    key = models.CharField(db_column='key', max_length=40, unique=True)
    member = models.OneToOneField(Member, db_column='member', related_name='auth_token')
    time_created = models.DateTimeField(db_column='time_created', auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(AuthToken, self).save(*args, **kwargs)

    def generate_key(self):
        unique = uuid.uuid4()
        return hmac.new(unique.bytes, digestmod=sha1).hexdigest()

    def __unicode__(self):
        return self.key

    class Meta:
        app_label = 'members'
        db_table = u'member_auth_token'
        verbose_name = _('Auth Token')
        verbose_name_plural = _('Auth Tokens')


class AnonymousMember(object):
    id = None
    pk = None
    username = ''
    is_staff = False
    is_active = False
    is_superuser = False

    def __init__(self): pass

    def __str__(self): return 'AnonymousUser'

    def __eq__(self, other): return isinstance(other, self.__class__)

    def __ne__(self, other): return not self.__eq__(other)

    def __hash__(self): return 1  # instances always return the same hash value

    def save(self): raise NotImplementedError

    def delete(self): raise NotImplementedError

    def set_password(self, raw_password): raise NotImplementedError

    def check_password(self, raw_password): raise NotImplementedError

    def is_anonymous(self): return True

    def is_authenticated(self): return False
