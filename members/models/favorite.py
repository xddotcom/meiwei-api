# coding:utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from member import Member
from restaurants.models import Restaurant


class Favorite(models.Model):
    id = models.AutoField(primary_key=True, db_column='favoriteId')
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True, verbose_name=u'会员名')
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True, blank=True, verbose_name=u'餐厅')
    favoritetime = models.DateTimeField(db_column='favoriteTime', auto_now_add=True, null=True, blank=True,
                                        verbose_name=u'收藏时间')

    class Meta:
        app_label = 'members'
        db_table = u'member_favorite'
        verbose_name = _('Favorite Restaurant')
        verbose_name_plural = _('Favorite Restaurants')
