# coding:utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from member import Member


class Credit(models.Model):
    CREDIT_TYPE = ((1, _('1. New User')),
                   (2, _('2. Order Fulfilled')),
                   (3, _('3. Order On Registration Day')),
                   (4, _('4. Two Orders In A Week')),
                   (5, _('5. Five Orders In A Month')),
                   (6, _('6. Product Redeem')),
                   (7, _('7. Add Avatar')),
                   (8, u'8. 在线支付'))
    id = models.IntegerField(primary_key=True, db_column='creditId')
    amount = models.IntegerField(db_column='amount', default=0, null=True, blank=True, verbose_name=u'积分数')
    credit_type = models.IntegerField(db_column='creditType', null=True, blank=True, choices=CREDIT_TYPE,
                                      verbose_name=u'积分类型')
    time_created = models.DateTimeField(db_column='happendTime', auto_now_add=True, null=True, blank=True,
                                        verbose_name=u'创建时间')
    reason = models.CharField(db_column='reason', max_length=255, null=True, blank=True, verbose_name=u'原因')
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True, verbose_name=u'会员名')

    class Meta:
        app_label = 'members'
        db_table = 'member_credit_infor'
        ordering = ['-time_created']
        verbose_name = _('Credit')
        verbose_name_plural = _('Credits')


def credit_registration(sender, instance, created, **kwargs):
    if created:
        Credit.objects.create(member=instance,
                              amount=100,
                              credit_type=1,
                              reason=_('New User Registration'))
