# coding:utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from member import Member


class Anniversary(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True, verbose_name=u'会员名')
    date = models.DateField(db_column='date', null=True, blank=True, verbose_name=u'日期')
    description = models.CharField(db_column='description', max_length=1000, default='', null=True, blank=True,
                                   verbose_name=u'纪念日描述')

    class Meta:
        app_label = 'members'
        db_table = u'member_anniversary'
        verbose_name = _('Anniversary')
        verbose_name_plural = _('Anniversaries')
