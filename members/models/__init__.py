from django.db.models.signals import post_save
import anniversary, contact, credit, favorite, member, profile, reference, review

post_save.connect(profile.create_profile, sender=member.Member)
post_save.connect(credit.credit_registration, sender=member.Member)

post_save.connect(profile.update_usename, sender=profile.Profile)


# Import models
from anniversary import Anniversary
from contact import Contact
from credit import Credit
from favorite import Favorite
from member import Member, AuthToken, AnonymousMember
from profile import Profile
from reference import Reference
from review import Review
