# coding:utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from address.models import Province, City, Area
from member import Member


class Contact(models.Model):
    GENDER = ((0, _('Male')), (1, _('Female')))
    RECEIVE_TIME = ((0, u'任何时间'), (1, u'只上班时间9:00--17:00'), (2, u'只晚上19:00--21:00'))
    id = models.AutoField(primary_key=True, db_column='contactId')
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True, verbose_name=u'会员名')
    time_created = models.DateTimeField(db_column='createTime', auto_now=True, null=True, blank=True,
                                        verbose_name=u'创建时间')
    email = models.CharField(db_column='email', max_length=50, default='', null=True, blank=True, verbose_name=u'电子邮件')
    zipcode = models.CharField(db_column='zipcode', max_length=10, default='', null=True, blank=True,
                               verbose_name=u'邮编')
    name = models.CharField(db_column='name', max_length=80, default='', null=True, blank=True, verbose_name=u'姓名')
    sexe = models.IntegerField(db_column='sexe', default=0, null=True, blank=True, choices=GENDER, verbose_name=u'性别')
    address = models.CharField(db_column='address', max_length=140, default='', null=True, blank=True,
                               verbose_name=u'地址')
    mobile = models.CharField(db_column='telephone', max_length=20, default='', null=True, blank=True,
                              verbose_name=u'手机号码')
    province = models.ForeignKey(Province, db_column='province', null=True, blank=True, verbose_name=u'省份')
    city = models.ForeignKey(City, db_column='city', null=True, blank=True, verbose_name=u'城市')
    area = models.ForeignKey(Area, db_column='area', null=True, blank=True, verbose_name=u'区县')
    type = models.IntegerField(db_column='type', null=True, default=0, verbose_name=u'类型')

    blood_type = models.CharField(db_column='blood_type', max_length=10, null=True, verbose_name=u'血型')
    relationship = models.CharField(db_column='relationship', max_length=45, null=True, verbose_name=u'相互关系')
    career = models.CharField(db_column='career', max_length=45, null=True, verbose_name=u'职业')
    revenue = models.IntegerField(db_column='revenue', null=True, verbose_name=u'收入')
    habit = models.CharField(db_column='habit', max_length=45, null=True, verbose_name=u'爱好')
    character = models.CharField(db_column='character', max_length=45, null=True, verbose_name=u'性格')

    color = models.CharField(db_column='color', max_length=45, null=True, verbose_name=u'喜欢颜色')

    receive_time = models.IntegerField(db_column='receive_time', null=True, choices=RECEIVE_TIME, verbose_name=u'收货时间')

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        app_label = 'members'
        db_table = u'member_contact_infor'
        ordering = ['-time_created']
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')
