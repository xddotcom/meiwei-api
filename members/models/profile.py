
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import models
from django.utils.translation import ugettext_lazy as _
from helper.functions import is_email, is_mobile_no
from member import Member
from reference import Reference

def validate_mobile(value):
    try:
        int(value)
    except (ValueError, TypeError):
        raise ValidationError(_('Enter a valid phone number.'))


class NullableCharField(models.CharField):
    description = _("CharField that obeys null=True")
    def to_python(self, value):
        if isinstance(value, models.CharField):
            return value
        return value or ""
    def get_db_prep_value(self, value, connection, prepared=False):
        return value or None


class Profile(models.Model):
    GENDER = ((0, _('Male')), (1, _('Female')))
    id = models.AutoField(primary_key=True, db_column='Id')
    member = models.OneToOneField(Member, db_column='member', null=True, blank=True)
    memberno = NullableCharField(db_column='memberNo', max_length=100, null=True, blank=True, unique=True, verbose_name=_('VIP No.'))
    email = NullableCharField(db_column='email', max_length=255, null=True, blank=True, unique=True, validators=[validate_email], verbose_name=_('Email'))
    mobile = NullableCharField(db_column='mobile', max_length=20, null=True, blank=True, unique=True, validators=[validate_mobile], verbose_name=_('Mobile'))
    avatar = models.ImageField(db_column='avatar', upload_to='avatar/', max_length=100, null=True, blank=True, verbose_name='Avatar')
    nickname = models.CharField(db_column='nickName', max_length=80, default='', null=True, blank=True, verbose_name=_('Name'))
    sexe = models.IntegerField(db_column='sexe', default=0, null=True, blank=True, choices=GENDER, verbose_name=_('Gender'))
    anniversary = models.CharField(db_column='anniversary', max_length=100, default='', null=True, blank=True, verbose_name=_('Anniversary'))
    birthday = models.DateField(db_column='birthday', null=True, blank=True, verbose_name=_('Birthday'))
    credit = models.IntegerField(db_column='credit', default=0, null=True, blank=True, verbose_name=_('Credit'))
    
    class Meta:
        app_label = 'members'
        db_table = u'member_personal_infor'
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

def update_usename(sender, instance, created, **kwargs):
    if not created:
        member = instance.member
        if is_email(member.username):
            member.username = instance.email
            member.save(update_fields=['username'])
        elif is_mobile_no(member.username):
            member.username = instance.mobile
            member.save(update_fields=['username'])


def create_profile(sender, instance, created, **kwargs):
    if created:
        member, username = instance, instance.username
        memberno = Reference.objects.get(id = member.id)    
        Profile.objects.create(member=member,
                               email=username if is_email(username) else None,
                               mobile=username if is_mobile_no(username) else None,
                               memberno=memberno.number)
