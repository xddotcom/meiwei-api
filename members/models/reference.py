
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Reference(models.Model):
    id = models.AutoField(primary_key=True, db_column='referenceId')
    number = models.CharField(max_length=100, unique=True)
    refnumber = models.CharField(max_length=100, default='', null=True, blank=True)
    
    class Meta:
        app_label = 'members'
        db_table = u'member_reference_infor'
        verbose_name = _('Reference')
        verbose_name_plural = _('References')
