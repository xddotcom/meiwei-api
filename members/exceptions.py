
from rest_framework.exceptions import APIException
from rest_framework import status
from django.utils.translation import ugettext_lazy as _


class MemberException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Incorrect member operation.')
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail


class MemberConflict(MemberException):
    status_code = status.HTTP_409_CONFLICT
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail


class MemberOperationForbidden(MemberException):
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail


class MemberPurchaseFail(MemberException):
    default_detail = _('You credit is not enough to redeem this item.')
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail
