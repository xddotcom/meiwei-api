
from django.conf.urls import patterns, include, url
from members.apiviews import MemberViewSet, MemberRegisterView, MemberLoginView, \
    ProfileViewSet, CreditViewSet, ContactViewSet, FavoriteViewSet, ReviewViewSet, \
    AnnniversaryViewSet, MemberValidateCodeView
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'member', MemberViewSet)
router.register(r'profile', ProfileViewSet)
router.register(r'credit', CreditViewSet)
router.register(r'contact', ContactViewSet)
router.register(r'favorite', FavoriteViewSet)
router.register(r'review', ReviewViewSet)
router.register(r'anniversary', AnnniversaryViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^register/$', MemberRegisterView.as_view()),
    url(r'^validatecode/$', MemberValidateCodeView.as_view()),
    url(r'^login/$', MemberLoginView.as_view()),
)
