from rest_framework.permissions import BasePermission

SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']

# Simple Permissions #

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            return False
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        else:
            return False

class AllowAll(BasePermission):
    def has_permission(self, request, view): return True
    def has_object_permission(self, request, view, obj): return True

class DenyAll(BasePermission):
    def has_permission(self, request, view): return False
    def has_object_permission(self, request, view, obj): return False


# Permissions Regarding Users #

class IsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated()

class IsSelf(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated()
    def has_object_permission(self, request, view, obj):
        try:
            return obj.id == request.user.id
        except:
            return False

class IsOwner(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_basic
    def has_object_permission(self, request, view, obj):
        try:
            return obj.member.id == request.user.id
        except:
            return False

class IsStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and (request.user.is_staff or request.user.is_admin)
    def has_object_permission(self, request, view, obj):
        try:
            return request.user.is_admin or obj.staff.id == request.user.id
        except:
            try:
                return obj.restaurant.staff.id == request.user.id
            except:
                return False

class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_admin
    def has_object_permission(self, request, view, obj):
        return True
