from members.models import Member, Profile
from helper.functions import is_email, is_mobile_no

class MeiweiBackend(object):

    def get_member(self, member_id):
        try:
            return Member.objects.get(pk = member_id)
        except:
            return None

    def authenticate(self, username, password):
        try:
            if is_email(username):
                member = Profile.objects.get(email=username).member
            elif is_mobile_no(username):
                member = Profile.objects.get(mobile=username).member
            else:
                member = Member.objects.get(username=username)
            if member.check_password(password):
                return member
        except:
            return None

class OAuthBackend(object):

    def get_member(self, member_id):
        try:
            return Member.objects.get(pk = member_id)
        except:
            return None

    def authenticate(self, provider, identity):
        try:
            member = provider.objects.get(pk = identity).member
            return member
        except:
            return None
