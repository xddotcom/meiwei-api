from __future__ import unicode_literals

from members.models import AuthToken
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header


class TokenAuthentication(BaseAuthentication):
    """
    Simple token based authentication.
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:
        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """
    
    model = AuthToken
    
    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        
        if not auth or auth[0].lower() != b'token':
            return None
        
        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)
        
        return self.authenticate_credentials(auth[1])
    
    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')
        
        if not token.member.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')
        
        return (token.member, token)
    
    def authenticate_header(self, request):
        return 'Token'

class URLTokenAuthentication(BaseAuthentication):
    
    model = AuthToken
    
    def authenticate(self, request):
        auth = request.QUERY_PARAMS.get('auth_token') or request.DATA.get('auth_token')
        
        if auth:
            return self.authenticate_credentials(auth)
        else:
            return None
    
    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')
        
        if not token.member.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')
        
        return (token.member, token)
    
    def authenticate_header(self, request):
        return 'Token'
