import random
import string
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from helper.functions import make_password, is_mobile_no, is_email
from members.auth import permissions
from members.exceptions import MemberException
from members.models import Member, AuthToken, Profile, Credit, Contact, Review, \
    Favorite, Anniversary
from members.serializers import MemberSerializer, AuthTokenSerializer, \
    ProfileSerializer, CreditSerializer, ContactSerializer, ReviewSerializer, \
    FavoriteSerializer, AnniversarySerializer
from orders.notify import send_validate_code_sms, send_validate_code_email
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
import logging


logger = logging.getLogger('meiweiapi')


class MemberRegisterView(APIView):
    permission_classes = []

    def post(self, request):
        params = request.DATA
        if 'validate_code' in params:
            validate_code = params['validate_code']
            username = params['username']
            member = Member.objects.get_or_create(username=username)[0]
            if member.status == 1:
                return Response({'errcode': 12000, 'errmsg': 'User Had Been Register'},
                                status=status.HTTP_400_BAD_REQUEST)
            if member.validate_code != validate_code:
                return Response({'errcode': 12001, 'errmsg': 'Validate Code Error'}, status=status.HTTP_400_BAD_REQUEST)
            password = params['password']
            member.set_password(password)
            member.status = 1
            member.save()
            return Response({}, status=status.HTTP_200_OK)
        serializer = MemberSerializer(data=params, files=request.FILES)
        if serializer.is_valid():
            try:
                with transaction.commit_on_success():
                    member = serializer.object
                    member.set_password(member.password)
                    member.status = 1
                    member.save(force_insert=True)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
            except:
                raise MemberException(detail=_("Couldn't create profile with given email/mobile"))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MemberValidateCodeView(APIView):
    permission_classes = []

    def post(self, request):
        params = request.DATA
        if 'username' in params:
            username = params['username']
            if is_mobile_no(username):
                member = Member.objects.get_or_create(username=username)[0]
                if member.status == 1:
                    return Response({'errcode': 12000, 'errmsg': 'User Had Been Register'},
                                    status=status.HTTP_400_BAD_REQUEST)
                validate_code = string.join(random.sample('123456789', 4), '')
                member.validate_code = validate_code
                send_validate_code_sms(username, {'validate_code': validate_code})
                member.save()
                return Response({}, status=status.HTTP_200_OK)
            elif is_email(username):
                member = Member.objects.get_or_create(username=username)[0]
                if member.status == 1:
                    return Response({'errcode': 12000, 'errmsg': 'User Had Been Register'},
                                    status=status.HTTP_400_BAD_REQUEST)
                validate_code = string.join(random.sample('123456789', 4), '')
                member.validate_code = validate_code
                send_validate_code_email(username, {'validate_code': validate_code})
                member.save()
                return Response({}, status=status.HTTP_200_OK)
            else:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)


class MemberLoginView(APIView):
    permission_classes = []

    def post(self, request):
        serializer = AuthTokenSerializer(data=request.DATA)
        if serializer.is_valid():
            token, _created = AuthToken.objects.get_or_create(member=serializer.object['member'])
            return Response({'token': token.key})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MemberViewSet(ReadOnlyModelViewSet):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
    permission_classes = [permissions.IsSelf]
    paginate_by = 1

    @action(methods=['put'])
    def change_password(self, request, pk=None):
        try:
            member = self.request.user
            member.password = make_password(request.DATA['password'])
            member.save(update_fields=['password'])
            return Response(None, status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response(e.message, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        return Member.objects.filter(id=self.request.user.id)


class OwnerModelMixin(object):
    def get_queryset(self):
        return self.queryset.filter(member=self.request.user)

    def pre_save(self, obj):
        obj.member = self.request.user


class ProfileViewSet(OwnerModelMixin, ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 1

    @action(methods=['post'])
    def avatar(self, request, pk=None):
        member = request.user
        # profile = self.queryset.filter(member=member)
        profile = self.get_object()
        avatar_credit = Credit.objects.all().filter(member=member, credit_type=7)
        if not avatar_credit:
            Credit.objects.create(member=request.user, amount=500, credit_type=7, reason='Add Avatar')
            credit = profile.credit + 500
            profile.credit = credit
            profile.save()
            return Response(self.serializer_class(profile, context={'request': request}).data)
        else:
            return Response(self.serializer_class(profile, context={'request': request}).data)


class CreditViewSet(OwnerModelMixin, ReadOnlyModelViewSet):
    queryset = Credit.objects.all()
    serializer_class = CreditSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 100


class ContactViewSet(OwnerModelMixin, ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 100


class ReviewViewSet(OwnerModelMixin, ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 10


class FavoriteViewSet(OwnerModelMixin, ModelViewSet):
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 10


class AnnniversaryViewSet(OwnerModelMixin, ModelViewSet):
    queryset = Anniversary.objects.extra(select={'month': 'MONTH(date)', 'day': 'DAY(date)'},
                                         order_by=['month', 'day'])
    serializer_class = AnniversarySerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 100
