# from django.utils.translation import ugettext_lazy as _
from orders.models import OrderAttribute, OrderProduct, Payment
from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail
from rest_framework import serializers
from restaurants import ProductItem


class OrderProductSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)

    product_id = serializers.IntegerField(required=True)

    name = serializers.CharField(required=True)
    gender = serializers.IntegerField(required=True)
    mobile = serializers.CharField(required=True)

    datetime = serializers.DateTimeField(required=True)
    address = serializers.CharField(required=False)

    comment = serializers.CharField(required=False)

    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None

        product_id = obj.get('product_id')
        product_item = ProductItem.objects.get(id=product_id)

        obj['product'] = product_item

        comment = '%s,%s(%s)' % (unicode(obj['product'].name), unicode(obj.get('name')), obj.get('mobile'))

        order_type = 20

        if product_item.price > 0:
            price = product_item.price
            payment = Payment.objects.create(amount=price,
                                             member=member,
                                             comment=comment)
        else:
            payment = Payment.objects.create(amount=0,
                                             member=member,
                                             comment=comment)
        obj['payment'] = payment
        order = OrderProduct.objects.create(member=member,
                                            payment=payment,
                                            order_type=order_type,
                                            name=obj.get('name'),
                                            gender=obj.get('gender'),
                                            mobile=obj.get('mobile'),
                                            comment=obj.get('comment'),)
        attributes = [OrderAttribute(order=order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'product_id', 'datetime', 'address'} and val]
        OrderAttribute.objects.bulk_create(attributes)
        obj['id'] = order.id
        obj['saved_order'] = order
