
from datetime import datetime, timedelta
from django.utils.translation import ugettext_lazy as _
from helper.functions import is_mobile_no
from orders.models import OrderAttribute, OrderDriver
from rest_framework import serializers


class OrderDriverSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=False)
    gender = serializers.IntegerField(required=False)
    mobile = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    latitude = serializers.FloatField(required=False)
    longitude = serializers.FloatField(required=False)
    comment = serializers.CharField(required=False)
    request_time = serializers.CharField(required=False)
    
    def validate_request_time(self, attrs, source):
        if attrs[source]:
            try:
                time_str = attrs[source][:5]
                fulltime = datetime.strptime(time_str, "%H:%M").time()
                now = datetime.now()
                if datetime.combine(now, fulltime) < now + timedelta(hours=1):
                    attrs[source] = ''
                else:
                    attrs[source] = time_str
            except:
                raise serializers.ValidationError(_('Time invalid, please choose again.'))
        return attrs
    
    def validate_mobile(self, attrs, source):
        if not attrs[source] or not is_mobile_no(attrs[source]):
            raise serializers.ValidationError(_("Please input a valid mobile number."))
        return attrs
    
    def validate(self, attrs):
        if (not attrs.get('name') or 
            str(attrs.get('gender')) not in ('0', '1') or 
            not attrs.get('address') or not attrs.get('latitude') or not attrs.get('longitude')):
            raise serializers.ValidationError(_("Please complete your information."))
        return attrs
    
    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None
        order = OrderDriver.objects.create(member=member, order_type=30,
                                           name=obj.get('name'),
                                           gender=obj.get('gender'),
                                           mobile=obj.get('mobile'),
                                           comment=obj.get('comment'),)
        attributes = [OrderAttribute(order=order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'address', 'latitude', 'longitude'} and val]
        OrderAttribute.objects.bulk_create(attributes)
        obj['id'] = order.id
        obj['saved_order'] = order
