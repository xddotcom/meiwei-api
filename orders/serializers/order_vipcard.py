# from django.utils.translation import ugettext_lazy as _
from orders.models import OrderAttribute, OrderVIPCard, Payment
# from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail
from rest_framework import serializers
from restaurants import ProductItem


class OrderVIPCardSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    
    product_id = serializers.IntegerField(required=True)
    
    name = serializers.CharField(required=False)
    gender = serializers.IntegerField(required=False)
    mobile = serializers.CharField(required=False)
    
    comment = serializers.CharField(required=False)
    
    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None
        
        product_id = obj.get('product_id')
        
        if product_id != 183:
            return
        
        product_item = ProductItem.objects.get(id=product_id)
        
        obj['product'] = product_item
        
        order_type = 60
        
        payment = Payment.objects.create(member=member,
                                         amount=product_item.price if product_item.price > 0 else 0,
                                         comment=unicode(product_item.name))
        
        obj['payment'] = payment
        
        order = OrderVIPCard.objects.create(member=member,
                                            payment=payment,
                                            order_type=order_type,
                                            name=obj.get('name'),
                                            gender=obj.get('gender'),
                                            mobile=obj.get('mobile'),
                                            comment=obj.get('comment'),)
        
        attributes = [OrderAttribute(order=order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'product_id'} and val]
        OrderAttribute.objects.bulk_create(attributes)
        
        obj['id'] = order.id
        obj['saved_order'] = order
