from datetime import datetime, timedelta
from django.utils.translation import ugettext_lazy as _
from orders.models import Order, Coupon, GenericOrder, OrderAttribute, Payment
from orders.notify import sendNewOrderMail, sendOrderConfirmSMS, sendOrderInvitationSMS, sendOrderWithProductsConfirmSMS, sendClubOrderSMS, sendNewNightClubOrderSMS, sendNewRestaurantOrderSMS, sendNewNightClubOrderSMS2Meiwei, sendNewRestaurantOrderSMS2Meiwei
from orders.serializers import GenericOrderSerializer, OrderProductSerializer
from rest_framework import serializers
from restaurants.serializers import RestaurantSerializer, ProductItemSerializer


class OrderSerializer(serializers.ModelSerializer):
    # member = serializers.RelatedField()
    restaurantinfor = RestaurantSerializer(many=False, source='restaurant', read_only=True)
    product_items = ProductItemSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'order_no', 'time_created', 'status', 'premium', 'consumeamount', 'creditgain',
                  'member', 'restaurant', 'contactname', 'contactphone', 'contactgender',
                  'orderdate', 'ordertime', 'personnum', 'other', 'clubseattype',
                  'tables', 'products', 'product_items',
                  'restaurantinfor')

        read_only_fields = ('id', 'order_no', 'time_created', 'status', 'consumeamount', 'creditgain')
        depth = 0

    """ attrs is from native data (eg POST data).
        Before attrs is given, field validation has been done.
        Only if validate_<field> has no errors, validate will be run.
    """
    
    def validate_personnum(self, attrs, source):
        value = attrs[source]
        if not 0 < value < 100:
            raise serializers.ValidationError(_('Too many people.'))
        return attrs

    def validate(self, attrs):
        if (not attrs['orderdate'] or not attrs['ordertime'] or
                    datetime.combine(attrs['orderdate'], attrs['ordertime']) < datetime.now() + timedelta(minutes=10)):
            raise serializers.ValidationError(_('Time invalid, please choose again.'))
        if not attrs.get('contactname') or not attrs.get('contactphone'):
            raise serializers.ValidationError(_("Contact's information is required."))
        if attrs['premium'] and not attrs['restaurant'].premium:
            raise serializers.ValidationError(_('Cannot make premium order for this restaurant.'))
        if attrs['premium'] and not Coupon.objects.filter(member=self.context['request'].user,
                                                          detail__coupon_type=30).exists():
            raise serializers.ValidationError(_('Not enough Premium Coupons.'))
        return attrs

    def product_save(self, obj):
        for i in obj.product_items:
            order_data = {
                'product_id': i.id,
                'name': obj.contactname,
                'gender': obj.contactgender,
                'mobile': obj.contactphone,
                'datetime': datetime.combine(obj.orderdate, obj.ordertime)
            }
            product_order = OrderProductSerializer(data=order_data, context=self.context)
            if product_order.is_valid():
                product_order.save()

    def post_save(self, obj, created):
        if created:
            sendNewOrderMail(obj)
            if obj.restaurant.id > 1:
                if obj.restaurant.restaurant_type == 20:
                    sendNewNightClubOrderSMS(obj)
                    sendNewNightClubOrderSMS2Meiwei(obj)
                    sendClubOrderSMS(obj)
                else:
                    sendNewRestaurantOrderSMS(obj)
                    sendNewRestaurantOrderSMS2Meiwei(obj)
                    # sendOrderInvitationSMS(obj)
            if obj.product_items and len(obj.product_items) > 0:
                sendOrderWithProductsConfirmSMS(obj)
            self.product_save(obj)

    def save_object(self, obj, **kwargs):
        super(OrderSerializer, self).save_object(obj, **kwargs)
        self.post_save(obj, 1)
