from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from helper.functions import json_dumps
from orders.models import OrderAttribute, OrderAirport, Payment
from rest_framework import serializers


class OrderAirportSerializer(serializers.Serializer):
    class GuestSerializer(serializers.Serializer):
        name = serializers.CharField(required=True)
        gender = serializers.IntegerField(required=True)
        id_no = serializers.CharField(required=True)

    id = serializers.IntegerField(read_only=True)
    guests = GuestSerializer(many=True)

    name = serializers.CharField(required=True)
    gender = serializers.IntegerField(required=True)
    mobile = serializers.CharField(required=True)

    flight_no = serializers.CharField(required=True)
    car_no = serializers.CharField(required=False)
    accompany = serializers.IntegerField(max_value=2)

    service_type = serializers.CharField(required=True)
    datetime = serializers.DateTimeField(required=True)
    airport = serializers.CharField(required=True)

    address = serializers.CharField(required=False)
    comment = serializers.CharField(required=False)

    def validate(self, attrs):
        if len(attrs['guests']) == 0:
            raise serializers.ValidationError(_("Please add at least one guest"))
        if attrs['accompany'] and (attrs['datetime'] - datetime.now()).days < 1:
            raise serializers.ValidationError(_("Please let us know 24 hours before"))
        return attrs

    def calculate_amount(self, obj):
        amount = 1500 if obj['airport'] in {'PVG', 'SHA', 'PEK', 'CAN'} else 800
        delta = obj['datetime'] - datetime.now()
        amount *= 2 if delta.seconds < 3 * 60 * 60 else 1
        guests = len(obj['guests'])
        accompany = int(obj.get('accompany', '0'))
        amount = amount * guests + accompany * 500
        return amount

    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None

        comment = '%s,%s(%s)' % ('vvip order', unicode(obj.get('name')), obj.get('mobile'))

        payment = Payment.objects.create(amount=self.calculate_amount(obj),
                                         member=member,
                                         comment=comment)
        order = OrderAirport.objects.create(member=member,
                                            payment=payment,
                                            order_type=40,
                                            name=obj.get('name'),
                                            gender=obj.get('gender'),
                                            mobile=obj.get('mobile'),
                                            comment=obj.get('comment'))
        attributes = [OrderAttribute(order=order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'flight_no', 'car_no', 'accompany', 'service_type',
                                 'datetime', 'airport', 'address'} and val]
        attributes.append(OrderAttribute(order=order, name='guests', value=json_dumps(obj['guests'])))
        OrderAttribute.objects.bulk_create(attributes)
        obj['id'] = order.id
        obj['saved_order'] = order
