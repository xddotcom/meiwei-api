
from django.utils.translation import ugettext_lazy as _
from orders.models import OrderAttribute, OrderPingan, Coupon
from rest_framework import serializers

class OrderPinganSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    coupon = serializers.CharField(required=True)
    
    name = serializers.CharField(required=True)
    gender = serializers.IntegerField(required=True)
    mobile = serializers.CharField(required=True)
    
    people = serializers.IntegerField(required=True)
    datetime = serializers.DateTimeField(required=True)
    address = serializers.CharField(required=True)
    comment = serializers.CharField(required=False)
    
    def validate_coupon(self, attrs, source):
        try:
            self.coupon = Coupon.objects.get(coupon_no=attrs[source])
        except:
            raise serializers.ValidationError(_("Coupon Invalid."))
        return attrs
    
    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None
        order = OrderPingan.objects.create(member=member, order_type=50,
                                           name=obj.get('name'),
                                           gender=obj.get('gender'),
                                           mobile=obj.get('mobile'),
                                           comment=obj.get('comment'),)
        attributes = [OrderAttribute(order=order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'people', 'datetime', 'address', 'coupon'} and val]
        OrderAttribute.objects.bulk_create(attributes)
        obj['id'] = order.id
        obj['saved_order'] = order
        self.coupon.apply_to_order(order)
