from rest_framework.reverse import reverse
from orders.models import Payment
from rest_framework import serializers

class PaymentSerializer(serializers.ModelSerializer):
    is_payable = serializers.Field(source='is_payable')
    is_paid = serializers.Field(source='is_paid')
    payment_url = serializers.SerializerMethodField('get_payment_url')

    class Meta:
        model = Payment
        fields = ('id', 'time_created', 'payment_no', 'amount', 'status', 'comment', 'is_payable', 'is_paid', 'payment_url')
        read_only_fields = ('id', 'time_created', 'payment_no', 'amount', 'status')
        depth = 0

    def get_payment_url(self, obj):
        return reverse('payments.views.payable',
                       request=self.context['request'],
                       args=(obj.payment_no,))
