
from orders.models import Coupon, CouponDetail
from rest_framework import serializers

class CouponDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = CouponDetail
        fields = ('name', 'coupon_type', 'description')
        depth = 0

class CouponSerializer(serializers.ModelSerializer):
    detail = CouponDetailSerializer(read_only=True)
    class Meta:
        model = Coupon
        fields = ('id', 'coupon_no', 'status', 'detail', 'member')
        read_only_fields = ('id', 'coupon_no', 'status', 'member')
        depth = 0
