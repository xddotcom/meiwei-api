# from django.utils.translation import ugettext_lazy as _
from orders.models import GenericOrder
from payment import PaymentSerializer
from restaurants.serializers import ProductItemSerializer
from restaurants.models import ProductItem, Restaurant
from rest_framework import serializers


class GenericOrderSerializer(serializers.ModelSerializer):
    attributes = serializers.Field(source='attributes')
    detail = serializers.SerializerMethodField('get_detail')
    payment = PaymentSerializer(read_only=True)

    class Meta:
        model = GenericOrder
        fields = ('id', 'order_no', 'time_created', 'order_type', 'status',
                  'name', 'gender', 'mobile', 'comment', 'attributes', 'detail', 'payment')
        read_only_fields = ('id', 'order_no', 'time_created', 'order_type', 'status')
        depth = 0

    def get_detail(self, obj):
        product_id = obj.attributes.get('product_id')
        restaurant_id = obj.attributes.get('restaurant_id')
        if product_id is not None:
            product = ProductItem.objects.get(id=product_id)
            return ProductItemSerializer(product, context=self.context).data
        if restaurant_id is not None:
            restaurant = Restaurant.objects.get(id=restaurant_id)
            return {
                'name': restaurant.fullname,
                'picture': restaurant.frontpic_fullpath(),
                'restaurant_id': restaurant_id
            }
        return None


class PackageGenericOrderSerializer(GenericOrderSerializer):
    orders_packed = GenericOrderSerializer(source='genericorder_set')
    
    class Meta:
        model = GenericOrder
        fields = ('id', 'order_no', 'time_created', 'order_type', 'status',
                  'name', 'gender', 'mobile', 'comment', 'attributes', 'detail', 'payment', 'orders_packed')
        read_only_fields = ('id', 'order_no', 'time_created', 'order_type', 'status')
        depth = 0
