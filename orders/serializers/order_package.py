
# from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from orders.models import OrderAttribute, OrderPackage, Payment

from datetime import date, timedelta


PACKAGE = {
    'CHRISTMAS_FLOWERS': [189, 185, 190],
    'VALENTINE_FLOWERS': [187, 194, 184],
    #'ANNIVERSARY': [193, 192, 191, 188, 196],
    'ANNIVERSARY': 198,
    'BIRTHDAY_CHAMPAGNE': 195,
    'CINEMA': 186,
    'CAR': 197
}


class OrderPackageSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    
    option_christmas = serializers.IntegerField(required=True)
    option_valentine = serializers.IntegerField(required=True)
    
    birthday = serializers.DateField(required=True)
    anniversary = serializers.DateField(required=True)
    anniversaryName = serializers.CharField(max_length=50, required=False)
    
    name = serializers.CharField(required=True)
    gender = serializers.IntegerField(required=True)
    mobile = serializers.CharField(required=True)
    address = serializers.CharField(required=True)
    
    sender_name = serializers.CharField(required=True)
    sender_gender = serializers.IntegerField(required=True)
    sender_mobile = serializers.CharField(required=True)
    sender_address = serializers.CharField(required=True)
    
    comment = serializers.CharField(required=False)
    
    def save_product_order(self, package_order, product_id, address, exp_date):
        order_type = 20
        order = OrderPackage.objects.create(package=package_order,
                                            member=package_order.member,
                                            order_type=order_type,
                                            name=package_order.name,
                                            gender=package_order.gender,
                                            mobile=package_order.mobile,
                                            comment=package_order.comment,)
        if exp_date < date.today():
            exp_date = exp_date.replace(exp_date.year + 1)
        attributes = [OrderAttribute(order=order, name='product_id', value=product_id),
                      OrderAttribute(order=order, name='address', value=address),
                      OrderAttribute(order=order, name='datetime', value=str(exp_date))]
        OrderAttribute.objects.bulk_create(attributes)
    
    def save_object(self, obj, **kwargs):
        request = self.context['request']
        member = request.user if request.user.is_authenticated() else None
        
        order_type = 90
        
        payment = Payment.objects.create(member=member, amount=999, comment=_('999 Lover''s Package'))
        
        obj['payment'] = payment
        
        package_order = OrderPackage.objects.create(member=member,
                                                    payment=payment,
                                                    order_type=order_type,
                                                    name=obj.get('name'),
                                                    gender=obj.get('gender'),
                                                    mobile=obj.get('mobile'),
                                                    comment=obj.get('comment'),)
        
        attributes = [OrderAttribute(order=package_order, name=key, value=val)
                      for key, val in obj.items()
                      if key in {'option_christmas', 'option_valentine', 'birthday', 'anniversary', 'anniversaryName',
                                 'address', 'sender_name', 'sender_gender', 'sender_mobile',
                                 'sender_address'} and val]
        OrderAttribute.objects.bulk_create(attributes)
        
        obj['id'] = package_order.id
        obj['saved_order'] = package_order
        
        product_christmas   = PACKAGE['CHRISTMAS_FLOWERS'][obj.get('option_christmas')]
        product_valentine   = PACKAGE['VALENTINE_FLOWERS'][obj.get('option_valentine')]
        product_birthday    = PACKAGE['BIRTHDAY_CHAMPAGNE']
        product_anniversary = PACKAGE['ANNIVERSARY']
        product_cinema      = PACKAGE['CINEMA']
        product_car         = PACKAGE['CAR']
        
        today = date.today()
        birthday = obj.get('birthday')
        anniversary = obj.get('anniversary')
        
        date_christmas =   date(today.year, 12, 24)
        date_valentine =   date(today.year, 2, 14)
        date_birthday =    anniversary.replace(year = today.year)
        date_anniversary = birthday.replace(year = today.year)
        date_tomorrow =    today + timedelta(days=1)
        
        address = obj.get('address')
        
        obj['orders_packed'] = [self.save_product_order(package_order, product_christmas,   address, date_christmas),
                                self.save_product_order(package_order, product_valentine,   address, date_valentine),
                                self.save_product_order(package_order, product_birthday,    address, date_birthday),
                                self.save_product_order(package_order, product_anniversary, address, date_anniversary),
                                self.save_product_order(package_order, product_cinema,      address, date_tomorrow),
                                self.save_product_order(package_order, product_car,         address, date_tomorrow)]
