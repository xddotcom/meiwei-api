
from django.db import models
from django.utils.translation import ugettext_lazy as _

from generic_order import GenericOrder
from orders.exceptions import OrderException


class OrderProductManager(models.Manager):
    def get_queryset(self):
        return super(OrderProductManager, self).get_queryset().filter(order_type=20)


class OrderProduct(GenericOrder):
    objects = OrderProductManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _('Product Order')
        verbose_name_plural = _("Product Orders")


def confirm_order_payment(sender, instance, **kwargs):
    genericorders = instance.genericorder_set.filter(order_type=20)
    if genericorders.exists():
        for genericorder in genericorders:
            if genericorder.status < 30:
                genericorder.status = 30
                genericorder.save(update_fields=['status'])
            else:
                raise OrderException()
