
from django.db import models
from django.utils.translation import ugettext_lazy as _
from generic_order import GenericOrder
import random


class CouponDetail(models.Model):
    COUPON_TYPE = ((0, _('0. Naive')),
                   (10, _('10. PingAn')),
                   (30, _('30. Restaurant VIP Card')),)
    id = models.AutoField(primary_key=True, db_column='id')
    
    coupon_type = models.IntegerField(db_column='coupon_type', default=0, null=True, blank=True, choices=COUPON_TYPE)
    name = models.CharField(db_column='name', max_length=100, null=True, blank=True)
    description = models.TextField(db_column='description', null=True, blank=True)
    
    #of_members = models.ManyToManyField("members.Member", through='Coupon')
    
    class Meta:
        app_label = 'orders'
        db_table = 'order_coupon_detail'
        verbose_name = _('Coupon Detail')
        verbose_name_plural = _('Coupon Details')

    def __unicode__(self):
        return unicode(self.name)


class Coupon(models.Model):
    COUPON_STATUS = ((0, _('0. Available')),
                     (50, _('50. Used')),
                     (90, _('90. Expired')))
    id = models.AutoField(primary_key=True, db_column='id')
    
    coupon_no = models.CharField(db_column='coupon_no', max_length=100, unique=True, null=True, blank=True)
    time_created = models.DateTimeField(db_column='time_created', auto_now_add=True)
    
    detail = models.ForeignKey(CouponDetail, db_column='detail', null=True, blank=True)
    member = models.ForeignKey('members.Member', db_column='member', null=True, blank=True)
    order = models.ForeignKey(GenericOrder, db_column='order', null=True, blank=True)
    
    status = models.IntegerField(db_column='status', default=0, null=True, blank=True, choices=COUPON_STATUS)
    
    def apply_to_order(self, order=None):
        self.order = order
        self.status = 50
        self.save(update_fields=['status', 'order'])
        
    class Meta:
        app_label = 'orders'
        db_table = 'order_coupon'
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')
    
    def __unicode__(self):
        return unicode(self.coupon_no)


def calculate_coupon_no(sender, instance, created, **kwargs):
    if created:
        instance.coupon_no = '100%03d%06d' % (random.randint(100, 999), instance.id)
        instance.save(update_fields=['coupon_no'])
