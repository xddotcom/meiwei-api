
from datetime import date

from django.db import models
from django.dispatch import Signal
from django.utils.translation import ugettext_lazy as _

from coupon import Coupon
from members.models import Member, Contact, Credit
from orders.exceptions import OrderException
from payment import Payment
from restaurants.models import Restaurant, ProductItem


order_confirmed = Signal(providing_args=['order'])
order_fulfilled = Signal(providing_args=['order'])
order_cancelled = Signal(providing_args=['order'])


class Order(models.Model):
    ORDER_STATUS = ((0, _('0. New')),
                    (10, _('10. Resto Confirmed')),
                    (20, _('20. Member Present')),
                    (30, _('30. Fulfilled, Resto Payable')),
                    (40, _('40. Resto Paid')),
                    (41, _('41. Resto Unpaid')),
                    (50, _('50. Complete')),
                    (99, _('99. Member Cancelled')),
                    (98, _('98. Resto Cancelled')),
                    (97, _('97. Admin Cancelled')),
                    (96, _('96. Member Absent')))
    GENDER = ((0, _('Male')), (1, _('Female')))
    SEATTYPE = ((0, _('Hall')), (1, _('Private Room')))
    
    id = models.AutoField(primary_key=True, db_column='orderId')
    time_created = models.DateTimeField(db_column='orderSubmitTime', auto_now_add=True, null=True, blank=True)
    
    order_no = models.CharField(max_length=100, unique=True, db_column='orderNo', null=True, blank=True,
                                verbose_name=_('Order Number'))
    
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant, db_column='restaurant', null=True)

    orderdate = models.DateField(db_column='orderDate', default=date.min, null=True, blank=True,
                                 verbose_name=_('Order Date'))
    
    ordertime = models.TimeField(db_column='orderTime', default='00:00:00', null=True, blank=True,
                                 verbose_name=_('Order Time'))

    status = models.IntegerField(db_column='status', default=0, null=True, blank=True, choices=ORDER_STATUS)
    
    premium = models.BooleanField(db_column='premium', default=False, verbose_name=_('Premium'))
    
    consumeamount = models.FloatField(db_column='consumeAmount', default=0.0, null=True, blank=True,
                                      verbose_name=_('Consume Amount'))
    
    creditgain = models.IntegerField(db_column='creditgain', default=0, null=True, blank=True,
                                     verbose_name=_('Credit Gain'))
    
    commission = models.FloatField(db_column='commission', default=0.0, null=True, blank=True)
    
    personnum = models.IntegerField(db_column='personNum', default=1, null=True, blank=True,
                                    verbose_name=_('Number of People'))
    
    tables = models.CharField(db_column='tables', max_length=64, default='', null=True, blank=True)
    
    products = models.CharField(db_column='products', max_length=64, default='', null=True, blank=True)
    
    contactname = models.CharField(db_column='contactName', max_length=20, null=True, blank=True,
                                   verbose_name=_('Contact Name'))
    
    contactgender = models.IntegerField(db_column='contactGender', default=0, null=True, blank=True, choices=GENDER,
                                        verbose_name=_('Contact Gender'))
    
    contactphone = models.CharField(db_column='contactTelphone', max_length=20, null=True, blank=True,
                                    verbose_name=_('Contact Mobile'))

    other = models.CharField(db_column='other', max_length=1000, default='', null=True, blank=True,
                             verbose_name=_('Comment'))
    
    adminother = models.CharField(db_column='adminother', max_length=1000, default='', null=True, blank=True)
    
    clubseattype = models.IntegerField(db_column='clubseattype', default=0, null=True, choices=SEATTYPE)
    
    payment = models.ForeignKey(Payment, db_column='payment_id', null=True, blank=True)
    
    cancelreason = models.CharField(db_column='cancelreason', max_length=140, default='', null=True, blank=True,
                                    verbose_name=_('Cancel Reason'))
    
    @property
    def is_new(self):
        return self.status < 10

    @property
    def is_pending(self):
        return 10 <= self.status < 20

    @property
    def is_fulfilled(self):
        return 30 <= self.status <= 50

    @property
    def is_cancelled(self):
        return self.status >= 90

    @property
    def product_items(self):
        pl = []
        if self.products:
            for i in self.products.split(','):
                pl.append(ProductItem.objects.get(id=int(i)))
        return pl

    def cancel(self, staff=None, ):
        if self.is_new or self.is_pending:
            self.status = 98 if staff else 99
            if self.orderdate < date.today(): self.status = 96
            self.save(update_fields=['status', 'cancelreason'])
            order_cancelled.send(sender=self.__class__, order=self)
        else:
            raise OrderException(_("This order couldn't be cancelled."))

    def confirm(self):
        if self.is_new:
            self.status = 10
            self.save(update_fields=['status'])
            order_confirmed.send(sender=self.__class__, order=self)
        else:
            raise OrderException(_("This order couldn't be confirmed."))

    def save_amount(self, amount):
        if self.is_pending:
            self.status = 30
            self.consumeamount = amount
            self.creditgain = int(amount + 0.5)
            self.save(force_update=True)
            Credit.objects.create(member=self.member,
                                  amount=self.creditgain, credit_type=2,
                                  reason='%s %s' % (str(self.orderdate), str(self.restaurant)))
            # profile = Profile.objects.get(member=self.member)
            # credit = profile.credit + amount
            # profile.credit = credit
            # profile.save()
            order_fulfilled.send(sender=self.__class__, order=self)
        else:
            raise OrderException

    class Meta:
        app_label = 'orders'
        db_table = 'order_infor'
        verbose_name = _('Restaurant Order')
        verbose_name_plural = _('Restaurant Orders')

    def __unicode__(self):
        return unicode(self.order_no)


def calculate_order_no(sender, instance, created, **kwargs):
    if created:
        today = date.today()
        orderstoday = Order.objects.filter(time_created__gte=today).order_by('id')
        todayfirst = orderstoday[0] if orderstoday else instance
        instance.order_no = '100%04d%02d%02d%06d' % (
            today.year, today.month, today.day, (instance.id - todayfirst.id + 1))
        instance.save(force_update=True)


def update_contact(sender, instance, created, **kwargs):
    if created:
        try:
            Contact.objects.get_or_create(member=instance.member,
                                          name=instance.contactname,
                                          mobile=instance.contactphone)
        except:
            pass


def apply_premium_coupon(sender, instance, created, **kwargs):
    if created and instance.premium:
        coupon = Coupon.objects.filter(member=instance.member, status=0, detail__coupon_type=30).first()
        if coupon is None:
            raise OrderException(_('Not enough Premium Coupons.'))
        else:
            coupon.apply_to_order()
