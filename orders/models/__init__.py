from django.db.models.signals import post_save
import restaurant_order, generic_order, payment, coupon
import order_airport, order_driver, order_pingan, order_product, order_vipcard, order_package

""" Restaurant Order """
post_save.connect(restaurant_order.apply_premium_coupon, sender=restaurant_order.Order)
post_save.connect(restaurant_order.calculate_order_no, sender=restaurant_order.Order)
post_save.connect(restaurant_order.update_contact, sender=restaurant_order.Order)

""" Generic Order No """
post_save.connect(generic_order.calculate_order_no, sender=generic_order.GenericOrder)
post_save.connect(generic_order.calculate_order_no, sender=order_airport.OrderAirport)
post_save.connect(generic_order.calculate_order_no, sender=order_driver.OrderDriver)
post_save.connect(generic_order.calculate_order_no, sender=order_pingan.OrderPingan)
post_save.connect(generic_order.calculate_order_no, sender=order_product.OrderProduct)
post_save.connect(generic_order.calculate_order_no, sender=order_vipcard.OrderVIPCard)
post_save.connect(generic_order.calculate_order_no, sender=order_package.OrderPackage)

""" Generic Order Contact """
post_save.connect(generic_order.update_contact, sender=generic_order.GenericOrder)
post_save.connect(generic_order.update_contact, sender=order_airport.OrderAirport)
post_save.connect(generic_order.update_contact, sender=order_driver.OrderDriver)
post_save.connect(generic_order.update_contact, sender=order_pingan.OrderPingan)
post_save.connect(generic_order.update_contact, sender=order_product.OrderProduct)
post_save.connect(generic_order.update_contact, sender=order_vipcard.OrderVIPCard)
post_save.connect(generic_order.update_contact, sender=order_package.OrderPackage)

""" Generic Order Payment Confirm """
payment.post_payment_complete.connect(order_airport.confirm_order_payment, sender=payment.Payment)
payment.post_payment_complete.connect(order_product.confirm_order_payment, sender=payment.Payment)
payment.post_payment_complete.connect(order_vipcard.confirm_order_payment, sender=payment.Payment)
payment.post_payment_complete.connect(order_package.confirm_order_payment, sender=payment.Payment)

""" Payment """
post_save.connect(payment.calculate_payment_no, sender=payment.Payment)

""" Coupon """
post_save.connect(coupon.calculate_coupon_no, sender=coupon.Coupon)

# Import models
from coupon import Coupon, CouponDetail
from generic_order import GenericOrder, OrderAttribute
from order_airport import OrderAirport
from order_driver import OrderDriver
from order_pingan import OrderPingan
from order_product import OrderProduct
from order_vipcard import OrderVIPCard
from order_package import OrderPackage
from payment import Payment
from restaurant_order import Order
