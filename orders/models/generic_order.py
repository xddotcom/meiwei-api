
from datetime import date
from django.db import models
from django.utils.translation import ugettext_lazy as _
from members.models import Member, Profile, Contact
from payment import Payment
from orders.notify import sendMemberCreatedOnOrderSMS
from orders.exceptions import OrderException


class GenericOrder(models.Model):
    ORDER_STATUS = ((0, _('0. New')),
                    (10, _('10. Confirmed')),
                    (30, _('30. Paid')),
                    (50, _('50. Complete')),
                    (99, _('99. Cancelled')),)
    
    ORDER_TYPE = ((10, _('10. Restaurant')),
                  (20, _('20. Product')),
                  (30, _('30. Anshifu')),
                  (40, _('40. Airport')),
                  (50, _('50. PingAn')),
                  (60, _('60. VIP Card')),
                  (90, _('90. Package')),)
    
    GENDER = ((0, _('Male')), (1, _('Female')))
    
    id = models.AutoField(primary_key=True, db_column='id')
    time_created = models.DateTimeField(db_column='time_created', auto_now_add=True)
    
    package = models.ForeignKey('self', db_column='package_order_id', null=True, blank=True)
    
    order_type = models.IntegerField(db_column='order_type', default=0, null=True, blank=True, choices=ORDER_TYPE)
    order_no = models.CharField(db_column='order_no', max_length=100, unique=True, null=True, blank=True)
    
    status = models.IntegerField(db_column='status', default=0, null=True, blank=True, choices=ORDER_STATUS)
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True)
    
    name = models.CharField(db_column='name', max_length=20, null=True, blank=True)
    gender = models.IntegerField(db_column='gender', default=0, null=True, blank=True, choices=GENDER)
    mobile = models.CharField(db_column='mobile', max_length=20, null=True, blank=True)
    payment = models.ForeignKey(Payment, db_column='payment_id', null=True, blank=True)
    
    comment = models.CharField(db_column='comment', max_length=1000, default='', null=True, blank=True)
    
    @property
    def attributes(self):
        if not hasattr(self, '_attrs'):
            self._attrs = {attr.name: attr.value for attr in self.orderattribute_set.all()}
        return self._attrs
    
    def confirm(self):
        if self.status < 10:
            self.status = 10
            self.save(update_fields=['status'])
        else:
            raise OrderException()
    
    def confirm_payment(self):
        """the "Paid" status is not necessary for all kind of generic orders"""
        pass
    
    def complete(self):
        if self.status < 50:
            self.status = 50
            self.save(update_fields=['status'])
        else:
            raise OrderException()
    
    def cancel(self):
        if self.status < 50:
            self.status = 99
            self.save(update_fields=['status'])
    
    class Meta:
        app_label = 'orders'
        db_table = 'order_generic'
        verbose_name = _('Generic Order')
        verbose_name_plural = _('Generic Orders')
    
    def __unicode__(self):
        return unicode(self.order_no)


def calculate_order_no(sender, instance, created, **kwargs):
    if created:
        today = date.today()
        orderstoday = GenericOrder.objects.filter(time_created__gte=today).order_by('id')
        todayfirst = orderstoday[0] if orderstoday else instance
        instance.order_no = '300%04d%02d%02d%06d' % (
            today.year, today.month, today.day, (instance.id - todayfirst.id + 1))
        instance.save(force_update=True)


def update_contact(sender, instance, created, **kwargs):
    if created:
        if instance.member:
            try:
                Contact.objects.get_or_create(member=instance.member, name=instance.name, mobile=instance.mobile)
            except:
                pass
        elif instance.mobile:
            try:
                Profile.objects.get(mobile=instance.mobile)
            except:
                username = instance.mobile
                password = instance.mobile[-6:]
                Member.objects.create(username=username, password=password)
                sendMemberCreatedOnOrderSMS(username, password)


###################################################################################################


class OrderAttribute(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    order = models.ForeignKey(GenericOrder, db_column='order', null=True, blank=True)
    name = models.CharField(db_column='name', max_length=64, default='', null=True, blank=True)
    value = models.CharField(db_column='value', max_length=255, default='', null=True, blank=True)
    
    class Meta:
        app_label = 'orders'
        db_table = 'order_attribute'
        verbose_name = _('Order Attribute')
        verbose_name_plural = _('Order Attributes')
        index_together = (('order', 'name'),)
