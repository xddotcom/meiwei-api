
from django.db import models
from django.utils.translation import ugettext_lazy as _
from generic_order import GenericOrder

class OrderPinganManager(models.Manager):
    def get_queryset(self):
        return super(OrderPinganManager, self).get_queryset().filter(order_type=50)

class OrderPingan(GenericOrder):
    objects = OrderPinganManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _("Ping'An Order")
        verbose_name_plural = _("Ping'An Orders")
