
from django.db import models
from django.utils.translation import ugettext_lazy as _

from generic_order import GenericOrder
from orders.exceptions import OrderException


class OrderAirportManager(models.Manager):
    def get_queryset(self):
        return super(OrderAirportManager, self).get_queryset().filter(order_type=40)


class OrderAirport(GenericOrder):
    objects = OrderAirportManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _('VVIP Order')
        verbose_name_plural = _('VVIP Orders')


def confirm_order_payment(sender, instance, **kwargs):
    genericorders = instance.genericorder_set.filter(order_type=40)
    if genericorders.exists():
        for genericorder in genericorders:
            if genericorder.status < 30:
                genericorder.status = 30
                genericorder.save(update_fields=['status'])
            else:
                raise OrderException()
