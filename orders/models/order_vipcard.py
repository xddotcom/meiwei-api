
from django.db import models
from django.utils.translation import ugettext_lazy as _

from generic_order import GenericOrder
from orders.exceptions import OrderException

from .coupon import Coupon, CouponDetail


class OrderVIPCardManager(models.Manager):
    def get_queryset(self):
        return super(OrderVIPCardManager, self).get_queryset().filter(order_type=60)


class OrderVIPCard(GenericOrder):
    objects = OrderVIPCardManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _('VIP Card Order')
        verbose_name_plural = _('VIP Card Orders')


def confirm_order_payment(sender, instance, **kwargs):
    genericorders = instance.genericorder_set.filter(order_type=60)
    if genericorders.exists():
        for genericorder in genericorders:
            if genericorder.status < 30:
                genericorder.status = 30
                genericorder.save(update_fields=['status'])
                create_coupons(genericorder)
            else:
                raise OrderException()


def create_coupons(ordervip):
    if ordervip.attributes.get('product_id') == '183':
        coupondetail = CouponDetail.objects.get(coupon_type=30)
        coupons = [Coupon(detail=coupondetail, member=ordervip.member)] * 12
        Coupon.objects.bulk_create(coupons)
