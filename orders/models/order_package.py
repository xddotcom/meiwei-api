
from django.db import models
from django.utils.translation import ugettext_lazy as _

from generic_order import GenericOrder
from orders.exceptions import OrderException


class OrderPackageManager(models.Manager):
    def get_queryset(self):
        return super(OrderPackageManager, self).get_queryset().filter(order_type=90)


class OrderPackage(GenericOrder):
    objects = OrderPackageManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _('Package Order')
        verbose_name_plural = _('Package Orders')


def confirm_order_payment(sender, instance, **kwargs):
    genericorders = instance.genericorder_set.filter(order_type=90)
    if genericorders.exists():
        for genericorder in genericorders:
            if genericorder.status < 30:
                genericorder.status = 30
                genericorder.save(update_fields=['status'])
            else:
                raise OrderException()
