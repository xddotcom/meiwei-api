
from django.db import models
from django.utils.translation import ugettext_lazy as _
from generic_order import GenericOrder

class OrderDriverManager(models.Manager):
    def get_queryset(self):
        return super(OrderDriverManager, self).get_queryset().filter(order_type=30)

class OrderDriver(GenericOrder):
    objects = OrderDriverManager()
    
    class Meta:
        proxy = True
        app_label = 'orders'
        verbose_name = _('Anshifu Order')
        verbose_name_plural = _('Anshifu Orders')
