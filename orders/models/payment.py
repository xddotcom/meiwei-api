
from datetime import date

from django.db import models
import django.dispatch
from django.utils.translation import ugettext_lazy as _

from members.models import Member
from orders.exceptions import PaymentAmountException


post_payment_complete = django.dispatch.Signal(providing_args=['instance'])

class Payment(models.Model):
    PAYMENT_STATUS = ((0, _('0. Payable')),
                      (50, _('50. Successful')),
                      (99, _('99. Failed')))
    PAYMENT_METHOD = ((0, _('Unknown')),
                      (1, _('Alipay')),
                      (2, _('Weixin')),
                      (3, _('Unipay')),
                      (4, _('Alipay Wap')),)
    id = models.AutoField(db_column='id', primary_key=True)
    
    payment_no = models.CharField(db_column='payment_no', max_length=100, unique=True, null=True, blank=True)
    time_created = models.DateTimeField(db_column='time_created', auto_now_add=True)
    
    amount = models.FloatField(db_column='amount', default=0, null=True, blank=True, verbose_name=_('Amount'))
    
    status = models.IntegerField(db_column='status', default=0, null=True, blank=True, choices=PAYMENT_STATUS, verbose_name=_('Payment Status'))
    
    comment = models.CharField(db_column='comment', max_length=1000, default='', null=True, blank=True)
    
    pay_method = models.IntegerField(db_column='pay_method', default=0, null=True, blank=True, choices=PAYMENT_METHOD, verbose_name=_('Payment Method'))
    
    member = models.ForeignKey(Member, db_column='member', null=True, blank=True)
    
    @property
    def is_payable(self):
        return self.status == 0

    @property
    def is_paid(self):
        return self.status == 50

    def confirm(self, amount, pay_method):
        if self.status == 0:
            if amount != self.amount:
                raise PaymentAmountException()
            self.status = 50
            self.pay_method = pay_method
            self.save(update_fields=['status', 'pay_method'])
            post_payment_complete.send(sender=self.__class__, instance=self)
    
    class Meta:
        app_label = 'orders'
        db_table = 'order_payment'
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')
    
    def __unicode__(self):
        return unicode(self.payment_no)


def calculate_payment_no(sender, instance, created, **kwargs):
    if created:
        today = date.today()
        paymentstoday = sender.objects.filter(time_created__gte=today).order_by('id')
        todayfirst = paymentstoday[0] if paymentstoday else instance
        instance.payment_no = '900%04d%02d%02d%06d' % (
            today.year, today.month, today.day, (instance.id - todayfirst.id + 1))
        instance.save(force_update=True)
