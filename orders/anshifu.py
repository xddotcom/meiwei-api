# encoding=utf-8

from datetime import datetime
from orders.exceptions import OrderException
from django.conf import settings
import hashlib
import logging
import urllib2
import json

logger = logging.getLogger('meiweiapi')


if settings.ANSHIFU_DEBUG:
    API_ID = '1053'
    API_KEY = 'meiwei'
    API_SECRET = '24504e35a953edaabda56d90f186c49e'
    API_SALT = '6ae325b047a2415132d88432c69229e0'
    API_HOST = 'http://test.api.4001002003.com'
else:
    API_ID = '1052'
    API_KEY = 'meiwei'
    API_SECRET = '654321'
    API_SALT = '97f337ea95ee4a1056ca5fd4574b0bf7'
    API_HOST = 'http://api.andaijia.com'

STATUS_CANCEL = {u'已取消'}
STATUS_COMPLETE = {u'已完成', u'完成'}

def unix_time():
    epoch = datetime.utcfromtimestamp(0)
    delta = datetime.now() - epoch
    return str(int(delta.total_seconds()))


def calcu_hash(params):
    items = params.items();
    items.sort()
    sign = ''.join([val for _key, val in items])
    digest = hashlib.md5(sign.encode('utf-8')).hexdigest()
    params['hash'] = digest


def api_request(api, params):
    params['time'] = unix_time()
    params['ver'] = '1.0'
    calcu_hash(params)
    url = api + '?' + '&'.join(['%s=%s' % (key, val) for key, val in params.items()])
    logger.debug(url)
    response = urllib2.urlopen(url.encode('utf-8'))
    result = response.read()
    logger.debug(result)
    response.close()
    try:
        json_obj = json.loads(result)
    except:
        json_obj = {}
    if str(json_obj.get('result')) != '0':
        logger.error('Error from Anshifu Server: %s , request lat: %s, lng: %s' % (json_obj.get('message'),
                                                                                   params.get('address_lat'),
                                                                                   params.get('address_lng')))
        raise OrderException(detail = json_obj.get('message'))
    return json_obj


def get_token():
    password = hashlib.md5(API_SALT + API_SECRET).hexdigest()
    params = {
        'agent_id': API_ID,
        'key': API_KEY,
        'password': password,
    }
    api = API_HOST + '/agent/token'
    json_obj = api_request(api, params)
    return json_obj.get('token')


def submit_order(raw_order):
    params = {
        'name': raw_order['name'],
        'gender': str(raw_order['gender'] + 1),
        'mobile': raw_order['mobile'],
        'address': raw_order['address'],
        'address_lng': '%.6f' % raw_order['latitude'] + '01',
        'address_lat': '%.6f' % raw_order['longitude'] + '01',
        'coupon_amount': '10',
        'coupon_distance': '0',
        'driver_num': '1',
        'request_time': raw_order['request_time'] or '',
        'comment': raw_order['comment'] or '',
        'agent_id': API_ID,
        'token': get_token(),
    }
    api = API_HOST + '/order/add'
    json_obj = api_request(api, params)
    return json_obj.get('order_id')


def get_order(order_id):
    params = {
        'order_id': order_id,
        'agent_id': API_ID,
        'token': get_token(),
    }
    api = API_HOST + '/order/data'
    json_obj = api_request(api, params)
    return json_obj
