from django.conf import settings
from helper.functions import send_sms, send_mail
from orders.messages import messages
import logging

logger = logging.getLogger('meiweiapi')

if settings.EMAIL_DEBUG:
    ORDER_MANAGER = 'xindong.ding@clubmeiwei.com'
else:
    ORDER_MANAGER = 'neworder@clubmeiwei.com'


def sendNewOrderMail(order):
    try:
        product_items = '\n'.join(['- %d - %s %s' % (item.id, unicode(item.name), unicode(item.description))
                                   for item in order.product_items])
        subjectTPL = messages['Admin New Order Subject']
        subject = subjectTPL.format(restaurant=order.restaurant.fullname,
                                    orderdate=order.orderdate,
                                    ordertime=order.ordertime,
                                    personnum=order.personnum,
                                    contactname=order.contactname,
                                    contactgender=messages['GENDER'][order.contactgender],
                                    contactphone=order.contactphone)
        contentTPL = messages['Admin New Order Content']
        content = contentTPL.format(time_created=order.time_created,
                                    order_no=order.order_no,
                                    member=order.member,
                                    restaurant=order.restaurant.fullname,
                                    telephone=order.restaurant.telephone,
                                    other=order.other,
                                    product_items=product_items)
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def send_wechat_right_notify_mail(parameters):
    try:
        subject = 'Wechat Right Notify Subject'
        content = 'Wechat Right Notify Content %s' % parameters
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def send_wechat_warning_notify_mail(parameters):
    try:
        subject = 'Wechat Warning Notify Subject'
        content = 'Wechat Warning Notify Content %s' % parameters
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderCancelMail(order):
    try:
        product_items = '\n'.join(['- %d - %s %s' % (item.id, unicode(item.name), unicode(item.description))
                                   for item in order.product_items])
        subjectTPL = messages['Admin Order Cancel Subject']
        subject = subjectTPL.format(restaurant=order.restaurant.fullname,
                                    orderdate=order.orderdate,
                                    ordertime=order.ordertime,
                                    personnum=order.personnum,
                                    contactname=order.contactname,
                                    contactgender=messages['GENDER'][order.contactgender],
                                    contactphone=order.contactphone)
        contentTPL = messages['Admin Order Cancel Content']
        content = contentTPL.format(time_created=order.time_created,
                                    order_no=order.order_no,
                                    member=order.member,
                                    restaurant=order.restaurant.fullname,
                                    telephone=order.restaurant.telephone,
                                    other=order.other,
                                    product_items=product_items)
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def sendGenericOrderCancelMail(order):
    try:
        subjectTPL = messages['Admin Generic Order Cancel Subject']
        subject = subjectTPL.format(order_type=order.order_type,
                                    order_no=order.order_no,
                                    time_created=order.time_created,
                                    name=order.name,
                                    gender=messages['GENDER'][order.gender],
                                    mobile=order.mobile,
                                    comment=order.comment)
        contentTPL = messages['Admin Generic Order Cancel Content']
        content = contentTPL.format(order_type=order.order_type,
                                    order_no=order.order_no,
                                    time_created=order.time_created,
                                    name=order.name,
                                    gender=messages['GENDER'][order.gender],
                                    mobile=order.mobile,
                                    comment=order.comment, )
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def sendNewProductOrderMail(obj):
    try:
        product = obj['product']
        order = obj['saved_order']
        if 'address' in obj:
            address = obj['address']
        else:
            address = ''
        product_string = '(id %d) %s %s' % (product.id, unicode(product.name), unicode(product.description))
        subjectTPL = messages['Admin New Product Order Subject']
        subject = subjectTPL.format(datetime=obj['datetime'],
                                    name=order.name,
                                    gender=messages['GENDER'][order.gender],
                                    mobile=order.mobile,
                                    product_item=product_string)
        contentTPL = messages['Admin New Product Order Content']
        content = contentTPL.format(time_created=order.time_created,
                                    order_no=order.order_no,
                                    member=order.member,
                                    mobile=order.mobile,
                                    comment=order.comment,
                                    address=address,
                                    datetime=obj['datetime'],
                                    product_item=product_string)
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def sendPaymentFinishMail(obj):
    try:
        subjectTPL = messages['Admin Payment Finish Subject']
        subject = subjectTPL.format(payment_from=obj['payment_from'], amount=obj['amount'],
                                    payment_no=obj['payment_no'])
        contentTPL = messages['Admin Payment Finish Content']
        content = contentTPL.format(payment_from=obj['payment_from'], amount=obj['amount'],
                                    payment_no=obj['payment_no'])
        send_mail(subject, content, ORDER_MANAGER)
    except Exception as e:
        logger.error(unicode(e))


def send_validate_code_email(email, obj):
    try:
        subjectTPL = messages['Send Email Validate Code Subject']
        subject = subjectTPL.format(validate_code=obj['validate_code'])
        contentTPL = messages['Send Email Validate Code Content']
        content = contentTPL.format(validate_code=obj['validate_code'])
        send_mail(subject, content, email)
    except Exception as e:
        logger.error(unicode(e))


def sendNewClubOrderSMS(order):
    try:
        if False:
            smsTPL = messages['Member New Order SMS With Payment']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                amount=0)
        else:
            smsTPL = messages['Member New Order SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNewRestaurantOrderSMS(order):
    try:
        if False:
            smsTPL = messages['Member New Restaurant Order SMS With Payment']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                amount=0)
        else:
            smsTPL = messages['Member New Restaurant Order SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNewNightClubOrderSMS(order):
    try:
        if False:
            smsTPL = messages['Member New Club Order SMS With Payment']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                amount=0,
                                clubseattype=messages['CLUBSEATTYPE'][order.clubseattype])
        else:
            smsTPL = messages['Member New Club Order SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                clubseattype=messages['CLUBSEATTYPE'][order.clubseattype])
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNewRestaurantOrderSMS2Meiwei(order):
    try:
        smsTPL = messages['New Restaurant Order SMS To Meiwei']
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            month=order.orderdate.month,
                            date=order.orderdate.day,
                            weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                            time=str(order.ordertime)[:5],
                            mobile=order.contactphone,
                            other=order.other,
                            restaurant=order.restaurant.fullname,
                            address=order.restaurant.address,
                            personnum=order.personnum,
                            amount=0)
        send_sms(settings.MEIWEI_SERVICE_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNewNightClubOrderSMS2Meiwei(order):
    try:
        smsTPL = messages['New Night Club Order SMS To Meiwei']
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            month=order.orderdate.month,
                            date=order.orderdate.day,
                            weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                            time=str(order.ordertime)[:5],
                            restaurant=order.restaurant.fullname,
                            address=order.restaurant.address,
                            mobile=order.contactphone,
                            personnum=order.personnum,
                            other=order.other,
                            amount=0,
                            clubseattype=messages['CLUBSEATTYPE'][order.clubseattype])
        send_sms(settings.MEIWEI_SERVICE_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderConfirmSMS(order):
    try:
        if False:
            smsTPL = messages['Member New Order Confirm SMS With Payment']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                amount=0)
        else:
            smsTPL = messages['Member New Order Confirm SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNightClubOrderConfirmSMS(order):
    try:
        if False:
            smsTPL = messages['Member Club New Order Confirm SMS With Payment']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                amount=0,
                                clubseattype=messages['CLUBSEATTYPE'][order.clubseattype])
        else:
            smsTPL = messages['Member Club New Order Confirm SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                clubseattype=messages['CLUBSEATTYPE'][order.clubseattype])
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendClubOrderSMS(order):
    try:
        smsTPL = messages['Night Club New Order SMS To Admin']
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            month=order.orderdate.month,
                            date=order.orderdate.day,
                            weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                            time=str(order.ordertime)[:5],
                            restaurant=order.restaurant.fullname,
                            clubseattype=messages['CLUBSEATTYPE'][order.clubseattype],
                            mobile=order.contactphone[7:],
                            personnum=order.personnum,
                            other=order.other)
        send_sms(settings.NIGHT_CLUB_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderWithProductsConfirmSMS(order):
    try:
        smsTPL = messages['Member New Order With Products Confirm SMS']
        product_items = ','.join(unicode(item.name) for item in order.product_items)
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            products=product_items)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendCustomSMS(phone, content):
    try:
        smsTPL = messages['Custom Send SMS']
        msg = smsTPL.format(content=content)
        send_sms(phone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendNewProductOrderConfirmSMS(obj):
    try:
        if 'payment' in obj:
            smsTPL = messages['Member New Product Order Confirm SMS With Payment']
            msg = smsTPL.format(name=obj['saved_order'].name,
                                gender=messages['GENDER'][obj['saved_order'].gender],
                                products=unicode(obj['product'].name),
                                amount=obj['payment'].amount)
        else:
            smsTPL = messages['Member New Product Order Confirm SMS']
            msg = smsTPL.format(name=obj['saved_order'].name,
                                gender=messages['GENDER'][obj['saved_order'].gender],
                                products=unicode(obj['product'].name))
        send_sms(obj['saved_order'].mobile, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendToMeiweiNewProductOrderSMS(obj):
    try:
        if 'payment' in obj:
            smsTPL = messages['New Product Order Confirm SMS With Payment To Meiwei']
            msg = smsTPL.format(name=obj['saved_order'].name,
                                gender=messages['GENDER'][obj['saved_order'].gender],
                                products=unicode(obj['product'].name),
                                amount=obj['payment'].amount)
        else:
            smsTPL = messages['Member New Product Order SMS To Meiwei']
            msg = smsTPL.format(name=obj['saved_order'].name,
                                gender=messages['GENDER'][obj['saved_order'].gender],
                                products=unicode(obj['product'].name))
        send_sms(settings.MEIWEI_SERVICE_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderInvitationSMS(order):
    try:
        smsTPL = messages['Member New Order Invitation SMS']
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            month=order.orderdate.month,
                            date=order.orderdate.day,
                            weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                            time=str(order.ordertime)[:5],
                            restaurant=order.restaurant.fullname,
                            address=order.restaurant.address)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderFulfilledSMS(order):
    try:
        smsTPL = messages['Member Order Fulfilled SMS']
        msg = smsTPL.format(name=order.contactname,
                            gender=messages['GENDER'][order.contactgender],
                            month=order.orderdate.month,
                            date=order.orderdate.day,
                            weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                            time=str(order.ordertime)[:5],
                            restaurant=order.restaurant.fullname,
                            amount=order.consumeamount,
                            credits=order.creditgain)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderCancelSMS(order):
    try:
        if not order.cancelreason:
            smsTPL = messages['Staff Order Cancel SMS No Reason']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum)
        else:
            smsTPL = messages['Staff Order Cancel SMS']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                reason=order.cancelreason)
        send_sms(order.contactphone, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOrderCancelSMS2Meiwei(order):
    try:
        if not order.cancelreason:
            smsTPL = messages['Staff Order Cancel SMS No Reason To Meiwei']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                mobile=order.contactphone,
                                personnum=order.personnum)
        else:
            smsTPL = messages['Staff Order Cancel SMS To Meiwei']
            msg = smsTPL.format(name=order.contactname,
                                gender=messages['GENDER'][order.contactgender],
                                month=order.orderdate.month,
                                date=order.orderdate.day,
                                weekday=messages['WEEKDAY'][order.orderdate.weekday()],
                                time=str(order.ordertime)[:5],
                                restaurant=order.restaurant.fullname,
                                address=order.restaurant.address,
                                personnum=order.personnum,
                                mobile=order.contactphone,
                                reason=order.cancelreason)
        send_sms(settings.MEIWEI_SERVICE_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendMemberCreatedOnOrderSMS(mobile, password):
    try:
        smsTPL = messages['Member Created on Order SMS']
        msg = smsTPL.format(mobile=mobile, password=password, )
        send_sms(mobile, msg)
    except Exception as e:
        logger.error(unicode(e))


def sendOnPaymentFinishSMS(obj):
    try:
        smsTPL = messages['Order on Payment Finish SMS']
        msg = smsTPL.format(payment_from=obj['payment_from'], amount=obj['amount'], payment_no=obj['payment_no'])
        send_sms(settings.ADMIN_MOBILE, msg)
    except Exception as e:
        logger.error(unicode(e))


def send_validate_code_sms(phone, obj):
    try:
        smsTPL = messages['Send Validate Code SMS']
        msg = smsTPL.format(validate_code=obj['validate_code'])
        send_sms(phone, msg)
    except Exception as e:
        logger.error(unicode(e))