
from django.conf.urls import patterns, include, url
from rest_framework import routers

from orders.views import OrderDriverCreation, OrderAirportCreation, \
    OrderPinganCreation, OrderProductCreation, OrderVIPCardCreation, OrderPackageCreation, PaymentView, CouponViewSet, \
    OrderViewSet, GenericOrderViewSet, PackageGenericOrderViewSet


router = routers.DefaultRouter()

router.register(r'order', OrderViewSet)
router.register(r'genericorder', GenericOrderViewSet, base_name="generic_orders")
router.register(r'packagegenericorder', PackageGenericOrderViewSet, base_name="package_generic_orders")
router.register(r'coupon', CouponViewSet, base_name="coupons")

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    
    url(r'^payment/notify/', PaymentView.as_view()),
    
    url(r'^orderdriver/', OrderDriverCreation.as_view()),
    url(r'^orderairport/', OrderAirportCreation.as_view()),
    url(r'^orderpingan/', OrderPinganCreation.as_view()),
    url(r'^orderproduct/', OrderProductCreation.as_view()),
    url(r'^ordervipcard/', OrderVIPCardCreation.as_view()),
    url(r'^orderpackage/', OrderPackageCreation.as_view())
)
