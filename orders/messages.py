
# encoding=utf-8

messages = {

'WEEKDAY': [u'一', u'二', u'三', u'四', u'五', u'六', u'天'],
'GENDER': [u'先生', u'小姐'],
'CLUBSEATTYPE':[u'卡座',u'包厢'],

'Admin New Order Subject':
u'【新订单】 {restaurant} {orderdate} {ordertime} {personnum}位 {contactname} {contactgender} {contactphone}',

'Admin New Order Content':
u"""
下单时间: {time_created}
订单编号: {order_no}
用户名: {member}
餐厅: {restaurant}
餐厅电话: {telephone}
备注: {other}
增值服务:
{product_items}""",

'Admin Order Cancel Subject':
u'【取消订单】 {restaurant} {orderdate} {ordertime} {personnum}位 {contactname} {contactgender} {contactphone}',

'Admin Order Cancel Content':
u"""
下单时间: {time_created}
订单编号: {order_no}
用户名: {member}
餐厅: {restaurant}
餐厅电话: {telephone}
备注: {other}
增值服务:
{product_items}""",

'Admin Generic Order Cancel Subject':
u'【增值服务取消订单】 客户: {name} {gender} 手机: {mobile}',

'Admin Generic Order Cancel Content':
u"""
下单时间: {time_created}
订单编号: {order_no}
订单类型: {order_type}
客户: {name}{gender}
手机: {mobile}
备注: {comment}
10:Restaurant 20:Product 30:Anshifu 40:Airport 50:PingAn 60:Restaurant Reservation
""",

'Admin New Product Order Subject':
u'【新订单】{product_item} {datetime} {name} {gender} {mobile}',

'Admin New Product Order Content':
u"""
下单时间: {time_created}
订单编号: {order_no}
用户名: {member}
时间：{datetime}
地址: {address}
产品: {product_item}
备注: {comment}
""",

'Admin Payment Finish Subject':
u'Get Payment from {payment_from} {payment_no} {amount}',

'Admin Payment Finish Content':
u"""
payment from:{payment_from}
payment_no:{payment_no}
amount:{amount}
""",

'Send Email Validate Code Subject':
u'欢迎注册使用IKE，您的注册验证码:{validate_code}',

'Send Email Validate Code Content':
u'欢迎注册使用IKE，您的注册验证码:{validate_code}',

'Member New Restaurant Order SMS':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人用餐订单已提交成功，
美位客服正在给您预定，请稍候。
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您用餐愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Restaurant Order SMS With Payment':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人用餐订单已提交成功，
美位客服正在给您预定，请稍候。
(预定{restaurant}，需要支付预订金{amount}元，请登录美位APP完成支付)
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您用餐愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Club Order SMS':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人{clubseattype}夜店预定订单已提交，
美位客服正在给您预定，请稍候。
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Club Order SMS With Payment':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人{clubseattype}夜店预定订单已提交，
美位客服正在给您预定，请稍候。
(预定{restaurant}，需要支付预订金{amount}元，请登录美位APP完成支付)
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Order Confirm SMS':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人用餐订单已确认成功。
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您用餐愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Order Confirm SMS With Payment':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人用餐订单已确认成功。
(预定{restaurant}，需要支付预订金{amount}元，请登录美位APP完成支付，已支付飘过)
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您用餐愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member Club New Order Confirm SMS':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人夜店订单已确认成功。
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您用餐愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member Club New Order Confirm SMS With Payment':
u"""{name}{gender}，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人夜店订单已确认成功。
(预定{restaurant}，需要支付预订金{amount}元，请登录美位APP完成支付，已支付飘过)
现在使用手机客户端下单可获赠更多积分！您还可以用积分兑换惊喜好礼！
详情下载登录手机客户端查询http://clubmeiwei.com/ad/apppromo ，或咨询在线客服。
祝您愉快!（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Night Club New Order SMS To Admin':
u"""庄先生您好，
请协助美位网预订夜店订单，并在预订成功后登陆后台向客户反馈信息。
订单详情:{name}{gender} 该客户手机尾号:{mobile}, 夜店名称:{restaurant},
预定日期:{month}月{date}日(星期{weekday}){time},
人数:{personnum},位置要求:{clubseattype}, 备注:{other}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'New Night Club Order SMS To Meiwei':
u"""新夜店订单提醒，
订单详情:{name}{gender} 该客户手机号:{mobile}, 夜店名称:{restaurant},
预定日期:{month}月{date}日(星期{weekday}){time},
人数:{personnum},位置要求:{clubseattype}, 备注:{other}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'New Restaurant Order SMS To Meiwei':
u"""新餐厅订单提醒，
订单详情:{name}{gender} 该客户手机号:{mobile}, 餐厅名称:{restaurant},
预定日期:{month}月{date}日(星期{weekday}){time},
人数:{personnum},备注:{other}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Staff Order Cancel SMS':
u"""您好，{name}{gender}，非常抱歉，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人的预订订单取消了。
取消原因:{reason},如有需要，请拨打客服电话021-60515617。（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Staff Order Cancel SMS No Reason':
u"""您好，{name}{gender}，非常抱歉，
您预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人的预订订单取消了。
如有需要，请拨打客服电话021-60515617。（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Staff Order Cancel SMS To Meiwei':
u"""订单取消提醒，{name}{gender}（{mobile}）预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人的预订订单取消了。
取消原因:{reason}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Staff Order Cancel SMS No Reason To Meiwei':
u"""订单取消提醒，{name}{gender}（{mobile}）预订的{month}月{date}日(星期{weekday}){time}在{restaurant}({address}){personnum}人的预订订单取消了。
取消原因:{reason}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Order With Products Confirm SMS':
u"""{name}{gender}，
您预订的{products}已确认成功，美位专人客服会尽快与您联系，感谢您使用美位网。（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Product Order Confirm SMS':
u"""{name}{gender}，
您预订的{products}已确认成功，美位专人客服会尽快与您联系，感谢您使用美位网。（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Product Order Confirm SMS With Payment':
u"""{name}{gender}，
您预订的{products}已确认成功，(需要支付{amount}元，请登录美位APP完成支付)，美位专人客服会尽快与您联系，感谢您使用美位网。
（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Product Order SMS To Meiwei':
u"""{name}{gender}，
预订的{products}已确认成功，美位客服请尽快处理。（www.clubmeiwei.com）(021-60515617)【美位网】""",

'New Product Order Confirm SMS With Payment To Meiwei':
u"""{name}{gender}，
预订的{products}已确认成功，(需要支付{amount}元)，美位客服请尽快处理。
（www.clubmeiwei.com）(021-60515617)【美位网】""",

'Member New Order Invitation SMS':
u"""{name}{gender}
诚邀您于{month}月{date}日(星期{weekday}){time}莅临{restaurant}({address})共享佳宴!
(www.clubmeiwei.com)(021-60515617)【美位网】""",

'Member Order Fulfilled SMS':
u"""{name}{gender}，
您于{month}月{date}日（星期{weekday}）{time}在{restaurant}用餐的消费金额是{amount}元(美位积分+{credits})。
欢迎再次使用美位网订餐，谢谢!(www.clubmeiwei.com)(021-60515617)【美位网】""",

'Member Night Club Order Fulfilled SMS':
u"""{name}{gender}，
您于{month}月{date}日（星期{weekday}）{time}在{restaurant}用餐的消费金额是{amount}元(美位积分+{credits})。
欢迎再次使用美位网订餐，谢谢!(www.clubmeiwei.com)(021-60515617)【美位网】""",

'Member Created on Order SMS':
u"""感谢您使用美位网。
你可以使用手机号码{mobile}和密码{password}随时登录美位网查询订单，预订餐厅和体验更多增值服务。
(www.clubmeiwei.com)(021-60515617)【美位网】""",

'Custom Send SMS':
u"""{content}（www.clubmeiwei.com）(021-60515617)【美位网】""",

'VVIP New Order Subject':
[u"""""", u""""""],

'VVIP New Order Content':
[u"""航班：{flight_no}, 起飞时间：{datetime}, 机场：{airport}，
服务类别：国内出港,贵宾人数：{persons}人，贵宾姓名：{name}，车牌信息：{car_no}，联系人：张三(联系电话{mobile})，
客人要求的接送详细地址{address}。
""",
u"""航班：{flight_no}, 降落时间：{datetime}, 机场：{airport}，
服务类别：国内进港,贵宾人数：{persons}人，贵宾姓名：{name}，车牌信息：{car_no}，联系人：张三(联系电话{mobile})，
客人要求的接送详细地址{address}。
"""],

'Wechat Right Notify Subject':
u"""微信投诉""",

'Wechat Right Notify Content':
u"""
投诉内容：
""",

'Wechat Warning Notify Subject':
u"""微信告警通知""",

'Wechat Warning Notify Content':
u"""
告警内容：
""",

'Order on Payment Finish SMS':
u"""
payment from:{payment_from}
payment_no:{payment_no}
amount:{amount}
(www.clubmeiwei.com)(021-60515617)【美位网】""",

'Send Validate Code SMS':
u"""
欢迎注册使用IKE，您的手机验证码为：{validate_code}
(www.clubmeiwei.com)(021-60515617)【美位网】""",

} # END messages dict
