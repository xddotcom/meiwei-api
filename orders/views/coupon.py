from rest_framework.decorators import action, link
from rest_framework.permissions import BasePermission
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.viewsets import ReadOnlyModelViewSet

from members.auth import permissions
from orders.models import Coupon
from orders.serializers import CouponSerializer


class IsCouponValidator(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_coupon_validator
    def has_object_permission(self, request, view, obj):
        return True


class CouponViewSet(ReadOnlyModelViewSet):
    permission_classes = (permissions.IsOwner,)
    lookup_field = 'coupon_no'
    queryset = Coupon.objects.all().order_by('-id')
    serializer_class = CouponSerializer
    paginate_by = None
    
    def get_queryset(self):
        return self.queryset.filter(member=self.request.user, status=0)
    
    @link(renderer_classes=(TemplateHTMLRenderer,),permission_classes=[])
    def highlight(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data, template_name='coupon_detail.html')
    
    @action(methods=['put'], permission_classes=(IsCouponValidator,))
    def validate(self, request, coupon_no=None):
        coupon = self.get_object()
        coupon.apply_to_order(order=None)
        return Response({}, status=HTTP_200_OK)
