
from orders.serializers import OrderPinganSerializer
from rest_framework.generics import CreateAPIView


class OrderPinganCreation(CreateAPIView):
    permission_classes = []
    serializer_class = OrderPinganSerializer
