from datetime import datetime

from rest_framework.generics import CreateAPIView

from members.auth import permissions
from orders.models import Payment
from orders.serializers import OrderAirportSerializer


class OrderAirportCreation(CreateAPIView):
    permission_classes = (permissions.IsOwner,)
    serializer_class = OrderAirportSerializer
    
    # def calculate_amount(self, obj):
    #     amount = 1500 if obj['airport'] in {'PVG', 'SHA', 'PEK', 'CAN'} else 800
    #     delta = obj['datetime'] - datetime.now()
    #     amount *= 2 if delta.seconds < 3 * 60 * 60 else 1
    #     guests = len(obj['guests'])
    #     accompany = int(obj.get('accompany', '0'))
    #     amount = amount * guests + accompany * 500
    #     return amount
    
    # def post_save(self, obj, created):
    #     if created:
    #         Payment.objects.create(order=obj['saved_order'], amount=self.calculate_amount(obj), comment='vvip order')
