
from orders.models import Payment
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
import hashlib


class PaymentView(APIView):
    permission_classes = []
    
    def put(self, request):
        data = request.DATA
        token = '1qaz2wsx3edc4rfv'
        payment_no = data.get('payment_no', '')
        amount = data.get('amount', '')
        comment = data.get('comment', '')
        hash_client = data.get('hash', '')
        
        sign = ''.join([amount, comment, payment_no, token])
        hash_calcu = hashlib.md5(sign.encode('utf-8')).hexdigest()
        
        if hash_client == hash_calcu:
            payment = Payment.objects.get(payment_no=payment_no)
            payment.confirm() 
            return Response({}, status=HTTP_200_OK)
        else:
            return Response({'detail': 'Invalid hash'}, status=HTTP_400_BAD_REQUEST)