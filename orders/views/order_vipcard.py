from datetime import date

from rest_framework.generics import CreateAPIView

from members.auth import permissions
from orders.models import Payment
from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail, sendToMeiweiNewProductOrderSMS
from orders.serializers import OrderVIPCardSerializer
from restaurants import ProductItem


class OrderVIPCardCreation(CreateAPIView):
    permission_classes = (permissions.IsOwner,)
    serializer_class = OrderVIPCardSerializer

    def post_save(self, obj, created):
        if created:
            pass
            #sendNewProductOrderConfirmSMS(obj)
            #sendToMeiweiNewProductOrderSMS(obj)
            #sendNewProductOrderMail(obj)
