from members.auth import permissions
from orders.models import GenericOrder, Payment
from orders.notify import sendGenericOrderCancelMail#, sendOrderCancelMail
from orders.serializers import GenericOrderSerializer, PackageGenericOrderSerializer, PaymentSerializer
from rest_framework.decorators import link, action
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.viewsets import ReadOnlyModelViewSet


class GenericOrderViewSet(ReadOnlyModelViewSet):
    permission_classes = (permissions.IsOwner,)
    queryset = GenericOrder.objects.all().order_by('-id')
    serializer_class = GenericOrderSerializer
    paginate_by = 10
    
    def get_queryset(self):
        queryset = self.queryset.filter(member=self.request.user)
        params = self.request.QUERY_PARAMS
        
        if 'status' in params:
            status = params['status']
            if status == 'fulfilled':
                queryset = queryset.filter(status__gte=30, status__lte=50).order_by('-id')
            elif status == 'pending':
                queryset = queryset.filter(status__lt=30)
        
        if 'order_type' in params:
            queryset = queryset.filter(order_type=params['order_type'], package__isnull=True)
        else:
            queryset = queryset.filter(order_type__in=[20, 40, 60], package__isnull=True)
        
        return queryset
    
    @link(permission_classes=[])
    def payment(self, request, pk=None):
        payment, _created = Payment.objects.get_or_create(order=self.get_object())
        serializer = PaymentSerializer(payment, context={'request': request})
        return Response(serializer.data)
    
    @action(methods=['put'])
    def cancel(self, request, pk=None):
        order = self.get_object()
        order.cancel()
        sendGenericOrderCancelMail(order)
        return Response({}, status=HTTP_204_NO_CONTENT)


class PackageGenericOrderViewSet(ReadOnlyModelViewSet):
    permission_classes = (permissions.IsOwner,)
    queryset = GenericOrder.objects.filter(order_type=90, status__lte=50)
    serializer_class = PackageGenericOrderSerializer
    paginate_by = 10
    
    def get_queryset(self):
        queryset = self.queryset.filter(member=self.request.user)
        return queryset
    
    @action(methods=['put'])
    def cancel(self, request, pk=None):
        order = self.get_object()
        order.cancel()
        sendGenericOrderCancelMail(order)
        return Response({}, status=HTTP_204_NO_CONTENT)
