from datetime import datetime
from members.auth import permissions
from orders.models import Order, GenericOrder, OrderAttribute, Payment
from orders.notify import sendOrderCancelMail, sendNewOrderMail, \
    sendOrderWithProductsConfirmSMS, sendOrderConfirmSMS, sendOrderInvitationSMS
from orders.serializers import OrderSerializer, OrderProductSerializer
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, \
    ListModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.viewsets import GenericViewSet
import logging

logger = logging.getLogger('meiweiapi')


class OrderViewSet(CreateModelMixin,
                   RetrieveModelMixin,
                   ListModelMixin,
                   GenericViewSet):
    queryset = Order.objects.all().order_by('orderdate', 'ordertime', 'id')
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsOwner,)
    paginate_by = 10

    def get_queryset(self):
        queryset = self.queryset.filter(member=self.request.user, restaurant__id__gt=1)
        params = self.request.QUERY_PARAMS

        if 'status' in params:
            status = params['status']
            if status == 'fulfilled':
                queryset = queryset.filter(status__gte=30, status__lte=50).order_by('-orderdate', '-ordertime', '-id')
            elif status == 'pending':
                queryset = queryset.filter(status__lt=30)
        
        return queryset

    @action(methods=['put'])
    def cancel(self, request, pk=None):
        order = self.get_object()
        order.cancel()
        sendOrderCancelMail(order)
        return Response({}, status=HTTP_204_NO_CONTENT)

    def pre_save(self, obj):
        obj.member = self.request.user
