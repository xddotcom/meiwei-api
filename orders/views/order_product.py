from datetime import date

from rest_framework.generics import CreateAPIView

from members.auth import permissions
from orders.models import Payment
from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail, sendToMeiweiNewProductOrderSMS
from orders.serializers import OrderProductSerializer
from restaurants import ProductItem


class OrderProductCreation(CreateAPIView):
    permission_classes = (permissions.IsOwner,)
    serializer_class = OrderProductSerializer

    def post_save(self, obj, created):
        if created:
            sendNewProductOrderConfirmSMS(obj)
            sendToMeiweiNewProductOrderSMS(obj)
            sendNewProductOrderMail(obj)
