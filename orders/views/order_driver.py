from rest_framework.generics import CreateAPIView

from members.auth import permissions
from orders import anshifu
from orders.models import OrderAttribute
from orders.serializers import OrderDriverSerializer


class OrderDriverCreation(CreateAPIView):
    permission_classes = (permissions.IsOwner,)
    serializer_class = OrderDriverSerializer
    
    def post_save(self, obj, created):
        if created:
            order_no = anshifu.submit_order(obj)
            OrderAttribute.objects.create(order=obj['saved_order'], name='remote_order_no', value=order_no)
