#from datetime import date

from rest_framework.generics import CreateAPIView

from members.auth import permissions
#from orders.models import Payment
#from orders.notify import sendNewProductOrderConfirmSMS, sendNewProductOrderMail, sendToMeiweiNewProductOrderSMS
from orders.serializers import OrderPackageSerializer
#from restaurants import ProductItem


class OrderPackageCreation(CreateAPIView):
    permission_classes = (permissions.IsOwner,)
    serializer_class = OrderPackageSerializer

    def post_save(self, obj, created):
        if created:
            pass
            #sendNewProductOrderConfirmSMS(obj)
            #sendToMeiweiNewProductOrderSMS(obj)
            #sendNewProductOrderMail(obj)
