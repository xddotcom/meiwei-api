
from rest_framework.exceptions import APIException
from rest_framework import status
from django.utils.translation import ugettext_lazy as _


class OrderException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Invalid operation.')
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail


class PaymentAmountException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('The transaction amount is wrong.')
    def __init__(self, detail=None):
        self.detail = detail or self.default_detail
