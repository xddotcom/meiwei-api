from django.contrib import admin
from django.core.urlresolvers import reverse
from django.db import models
from django.forms import Select, TextInput
from django.utils.translation import ugettext_lazy as _

from orders import anshifu
from orders.models import Order, GenericOrder, OrderAirport, OrderDriver, \
    OrderProduct, OrderAttribute, Payment, CouponDetail, Coupon, OrderVIPCard


class BaseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs={'style': 'max-width: 150px'})},
        models.CharField: {'widget': TextInput(attrs={'style': 'max-width: 150px'})},
    }


class BaseInline(admin.TabularInline):
    formfield_overrides = {
        models.ForeignKey: {'widget': Select(attrs={'style': 'max-width: 150px'})},
    }


class OrderAdmin(BaseAdmin):
    def payment_link(self, instance):
        if instance.payment:
            return '<a href="/admin/orders/payment/%s">%s</a>' % (instance.payment.id, instance.payment.payment_no)
    payment_link.allow_tags = True
    
    list_display = ['order_no', 'restaurant', 'orderdate', 'ordertime',
                    'contactname', 'contactphone', 'contactgender', 'personnum',
                    'premium', 'status', 'consumeamount', 'creditgain',
                    'other', 'adminother', 'cancelreason',
                    'time_created']
    list_display_links = ['order_no']
    #list_editable = ['status', 'contactname', 'contactgender', 'contactphone']
    list_filter = ['status']
    list_per_page = 10
    # date_hierarchy = 'orderdate'


class OrderAttributeInline(BaseInline):
    model = OrderAttribute
    extra = 1


class PaymentInline(BaseInline):
    model = Payment
    extra = 1


class GenericOrderAdmin(BaseAdmin):
    def payment_link(self, instance):
        if instance.payment:
            return '<a href="/admin/orders/payment/%s">%s</a>' % (instance.payment.id, instance.payment.payment_no)

    payment_link.allow_tags = True
    list_display = ['id', 'time_created', 'order_type', 'order_no', 'status', 'member',
                    'name', 'gender', 'mobile', 'payment_link', 'comment', 'package']
    list_filter = ['order_type', 'status', 'payment__status']
    list_display_links = ['id', 'order_no', 'payment_link']
    inlines = [OrderAttributeInline]


class OrderAirportAdmin(GenericOrderAdmin):
    list_filter = ['status']


class OrderDriverAdmin(GenericOrderAdmin):
    list_filter = ['status']
    actions = ['checkOrder']

    def checkOrder(self, request, queryset):
        for order in queryset.filter(order_type=30):
            if 'remote_order_no' in order.attributes:
                order_no = order.attributes['remote_order_no']
                try:
                    result = anshifu.get_order(order_no)
                except:
                    result = {}
                status = result.get('order_state')
                if status in anshifu.STATUS_COMPLETE:
                    order.status = 50
                    order.save(update_fields=['status'])
                elif status in anshifu.STATUS_CANCEL:
                    order.status = 99
                    order.save(update_fields=['status'])
                for key, val in result.items():
                    if val:
                        attribute, _created = order.orderattribute_set.get_or_create(order=order, name=key)
                        attribute.value = val
                        attribute.save()
            else:
                order.status = 99
                order.save(update_fields=['status'])

    checkOrder.short_description = "Check order status from Anshifu"


# class OrderPinganAdmin(GenericOrderAdmin):
#     list_filter = ['status']


class OrderProductAdmin(GenericOrderAdmin):
    list_filter = ['status']


class OrderVIPCardAdmin(GenericOrderAdmin):
    list_filter = ['status']


class PaymentAdmin(BaseAdmin):
    def payable_link(self, instance):
        path = reverse('payments.views.payable', args=(instance.payment_no,))
        return '<a href="%s">%s</a>' % (path, 'Right click to copy the link')
    payable_link.allow_tags = True
    payable_link.short_description = _("Payable Link")
    
    def confirmPayment(self, request, queryset):
        for payment in queryset:
            payment.confirm(payment.amount, 0)
    confirmPayment.short_description = _("Confirm Payment")
    
    actions = ['confirmPayment']
    list_display = ['id', 'time_created', 'payment_no', 'payable_link', 'status', 'amount', 'comment', 'pay_method', 'member']
    list_display_links = ['id', 'payment_no']
    list_filter = ['status', 'pay_method']


class CouponDetailAdmin(BaseAdmin):
    class CouponInline(BaseInline):
        model = Coupon
        count = 3

    list_display = ['id', 'name', 'coupon_type', 'description']
    list_display_links = ['id', 'name']
    list_filter = ['coupon_type']
    # inlines = [CouponInline]


class CouponAdmin(BaseAdmin):
    list_display = ['id', 'time_created', 'coupon_no', 'status', 'detail', 'member']
    list_display_links = ['id', 'coupon_no']
    list_filter = ['status']


admin.site.register(Order, OrderAdmin)
admin.site.register(GenericOrder, GenericOrderAdmin)
admin.site.register(OrderAirport, OrderAirportAdmin)
admin.site.register(OrderDriver, OrderDriverAdmin)
admin.site.register(OrderProduct, OrderProductAdmin)
admin.site.register(OrderVIPCard, OrderVIPCardAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(CouponDetail, CouponDetailAdmin)
admin.site.register(Coupon, CouponAdmin)
# admin.site.register(OrderPingan, OrderPinganAdmin)
