��    `        �         (     )     6     ?  
   F     Q  
   _     j     y     �     �     �     �     �     �     �     	     	     "	  
   /	     :	     I	     R	     _	     k	     w	     �	     �	     �	  
   �	     �	     �	  
   �	     �	     �	     
     
  .   #
     R
     Z
     j
     y
     �
     �
  "   �
     �
     �
     �
     �
     �
                    $     3     8     K     P  0   l     �     �     �  
   �     �  
   �     �                     $     3     B     K     Y     h  !   �  #   �  "   �     �     �               !     2      D  !   e  !   �  "   �     �     �     �     �     �  
             "  d  )  	   �     �     �     �     �     �     �     �       
             ,     L     Z     n     �     �     �     �  
   �     �     �     �     �          !     2     C  
   T     _  	   p     z     �     �     �     �  $   �     �     �     �               !     1  	   M     W     g     w  	   �     �     �     �     �     �     �     �     �  <   	     F     M     Z     g  	   t     ~     �     �     �     �     �     �     �     �     �                ;  ;   T  	   �     �     �     �     �     �     �     �     
  '   #     K     a     h     o     |     �     �     �            3           _   O   *             9       R   0          J   =       D   $       #   "         !   	   -      Y   P       1                    `   ?   ;   U           M   I          +   '   %   6      W   &           V       Q   Z   L   <                  ^   /   B                     H             
       C      ,   X   A   4           >   .      )       S   G              5   :         7   T   @   8          (       K   F       [   ]             2             E   N      \        0. Available 0. Naive 0. New 0. Payable 10. Confirmed 10. PingAn 10. Restaurant 10. Resto Confirmed 20. Member Present 20. Product 30. Anshifu 30. Fulfilled, Resto Payable 30. Paid 30. Restaurant VIP Card 40. Airport 40. Resto Paid 41. Resto Unpaid 50. Complete 50. PingAn 50. Successful 50. Used 60. VIP Card 90. Expired 90. Package 96. Member Absent 97. Admin Cancelled 98. Resto Cancelled 99. Cancelled 99. Failed 99. Member Cancelled Alipay Alipay Wap Amount Anshifu Order Anshifu Orders Cancel Reason Cannot make premium order for this restaurant. Comment Confirm Payment Consume Amount Contact Gender Contact Mobile Contact Name Contact's information is required. Coupon Coupon Detail Coupon Details Coupon Invalid. Coupons Credit Gain Female Generic Order Generic Orders Hall Invalid operation. Male Not enough Premium Coupons. Not enough coupons for premium restaurant order. Number of People Order Attribute Order Attributes Order Date Order Number Order Time Package Order Package Orders Payable Link Payment Payment Method Payment Status Payments Ping'An Order Ping'An Orders Please add at least one guest Please complete your information. Please input a valid mobile number. Please let us know 24 hours before Premium Private Room Product Order Product Orders Restaurant Order Restaurant Orders The transaction amount is wrong. This order couldn't be cancelled. This order couldn't be confirmed. Time invalid, please choose again. Too many people. Unipay Unknown VIP Card Order VIP Card Orders VVIP Order VVIP Orders Weixin Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-26 19:04+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 0. 有效 0. Naive 0. 新订单 0. 待支付 10. 订单已确认 10. 平安银行 10. 餐厅预订 10. 餐厅确认 20. 客人出席 20. 商品 30. 安师傅 30. 消费完成，佣金待付 30. 已支付 30. 美位不等位 40. VVIP机场服务 40. 佣金已付 41. 佣金未付 50. 订单完成 50. 平安银行 50. 成功 50. 已使用 60. 会员卡 90. 已过期 90. 爱情契约 96. 客人未出席 97. 美位取消 98. 餐厅取消 99. 订单取消 99. 失败 99. 用户取消 支付宝 支付宝Wap 金额 安师傅订单 安师傅订单 取消原因 该餐厅不支持不等位订餐。 备注 确认支付 消费金额 联系人性别 联系人手机 联系人姓名 请填写联系人信息。 优惠券 优惠券详情 优惠券详情 优惠券无效。 优惠券 获得积分 小姐 通用订单 通用订单 大厅/卡座 订单操作无效。 先生 不等位卡数量不足。 用户不等位卡数量不足，无法确认不等位订单 人数 订单属性 订单属性 订单日期 订单号 订单时间 礼盒订单 礼盒订单 支付链接 支付 支付方式 支付状态 支付 平安银行订单 平安银行订单 请至少添加一名贵宾 请完善您的预订信息。 请输入手机号码。 请至少在飞机起飞前24小时提醒我们陪同人数 不等位 神秘的包房 商品订单 商品订单 餐厅订单 餐厅订单 交易金额错误 无法取消该订单。 无法确认该订单。 订单时间无效，请重新选择。 订单人数过多。 银联 未知 VIP卡订单 VIP卡订单 机场服务订单 机场服务订单 微信 