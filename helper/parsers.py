from django.conf import settings
from rest_framework.compat import six
from rest_framework.exceptions import ParseError
from rest_framework import renderers
import json
from rest_framework.parsers import BaseParser

class PlainTextParser(BaseParser):
    """
    Parses JSON-serialized data.
    """

    media_type = 'text/plain'
    renderer_class = renderers.UnicodeJSONRenderer

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Parses the incoming bytestream as JSON and returns the resulting data.
        """
        parser_context = parser_context or {}
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)

        try:
            data = stream.read().decode(encoding)
            return json.loads(data)
        except ValueError as exc:
            raise ParseError('JSON parse error - %s' % six.text_type(exc))
