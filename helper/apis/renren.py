#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

import logging
import time
import hashlib
import urllib2
import json

from django.utils.http import urlencode


def force_utf8(string):
    """ Detect if a string is unicode and encode as utf-8 if necessary """
    if isinstance(string, unicode):
        string = string.encode('utf-8')
        logging.debug('Convert to utf-8:' + string)
    return string


def http_request(url, args = None, post_args = None):
    args = args or {}
    post_data = None if post_args is None else urlencode(post_args)

    try:
        response = urllib2.urlopen(url + "?" + urlencode(args), post_data)
    except urllib2.HTTPError, e:
        error = json.loads(e.read())
        raise APIError(error)

    try:
        file_info = response.info()
        if file_info.maintype == 'text' or file_info.type == 'application/json':
            result = json.loads(response.read())
        elif file_info.maintype == 'image':
            mimetype = file_info['content-type']
            result = {'data': response.read(), 'mime-type': mimetype, 'url': file.url}
        else:
            raise APIError('Maintype was not text or image')
    finally:
        response.close()

    if result and isinstance(result, dict) and ('error' in result or 'error_code' in result):
        raise APIError(result)
    return result


def auth_url(client_id, redirect_uri, scope = None):
    url = 'https://graph.renren.com/oauth/authorize?'
    kvps = {'client_id': client_id, 'redirect_uri': redirect_uri, 'response_type': 'code'}
    if scope:
        kvps['scope'] = ' '.join(scope)
    return url + urlencode(kvps)


def get_access_token_from_code(code, client_id, client_secret, redirect_uri):
    """ {'access_token': '***', 'expires_in': ***, 'refresh_token': '***', 'scope': '***'} """
    args = {'grant_type': 'authorization_code', 'code': code, 'redirect_uri': redirect_uri,
            'client_id': client_id, 'client_secret': client_secret}
    token_url = 'http://graph.renren.com/oauth/token'
    response = http_request(url = token_url, args = args)
    try:
        return response['access_token'], response['expires_in'], response['refresh_token']
    except:
        raise APIError('Get access token failed', response)


class ClientAPI(object):

    def __init__(self, secret_key, access_token):
        self.api_url = 'http://api.renren.com/restserver.do'
        self.secret_key = secret_key
        self.access_token = access_token

    def calculate_sig(self, args):
        params = ['%s=%s' % (force_utf8(x), force_utf8(args[x])) for x in sorted(args.keys())]
        hasher = hashlib.md5(''.join(params))
        hasher.update(self.secret_key)
        return hasher.hexdigest()

    def api_request(self, method, post_args = None):
        post_args = post_args or {}
        post_args['v'] = '1.0'
        post_args['format'] = 'json'
        post_args['method'] = method
        post_args['call_id'] = str(int(time.time() * 1000))
        post_args['access_token'] = self.access_token
        sig = self.calculate_sig(post_args);
        post_args['sig'] = sig
        response = http_request(url = self.api_url, post_args = post_args)
        return response

    def get_user_id(self):
        response = self.api_request(method = 'users.getLoggedInUser')
        try:
            return response['uid']
        except:
            raise APIError('Get lggedin user failed', response)


class APIError(Exception):
    def __init__(self, message, result = None):
        self.message = str(message)
        self.result = result or []
        Exception.__init__(self, self.message)
    def __str__(self):
        return self.message + ' : ' + json.dumps(self.result, ensure_ascii = False)
