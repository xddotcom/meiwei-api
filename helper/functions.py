
from django.conf import settings
from django.core.mail import send_mail as django_send_mail
from django.http import HttpResponse
from django.utils.crypto import get_random_string
from django.utils.http import urlencode
from helper import jieba
from structured import dict2xml
import hashlib
import json
import logging
import re
import urllib2

logger = logging.getLogger('meiweiapi')

def json_loads(s):
    try:
        return json.loads(s)
    except:
        return {}


def json_dumps(s):
    return json.dumps(s, ensure_ascii=False)


def json_response(json_msg, status = 200):
    return HttpResponse(json_dumps(json_msg), content_type="application/json", status = status)


def xml_response(xml_msg, roottag="xml", status = 200):
    return HttpResponse(dict2xml(xml_msg, roottag), content_type="application/xml", status = status)


def make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    return get_random_string(length, allowed_chars)


def make_password(raw_password):
    m = hashlib.md5()
    m.update(raw_password)
    return m.hexdigest()


def normalize_email(email):
    email = email or ''
    try:
        email_name, domain_part = email.strip().rsplit('@', 1)
    except ValueError:
        pass
    else:
        email = '@'.join([email_name, domain_part.lower()])
    return email


email_re = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
    # quoted-string, see also http://tools.ietf.org/html/rfc2822#section-3.2.5
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"'
    r')@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)'  # domain
    r'|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$', re.IGNORECASE)  # literal form, ipv4 address (SMTP 4.1.3)
def is_email(email):
    return email_re.match(email) is not None


#mobile_no_re = re.compile(r'^[0-9]+$')
mobile_no_re = re.compile(r'^1[0-9]{10}$')
def is_mobile_no(mobile_no):
    return mobile_no_re.match(mobile_no) is not None


def distance_square(lat1, lon1, lat2, lon2):
    ''' @var lat1, lon1, lat2, lon2: integer coordinates multiplied by 100000
        @return: square of distance in meters
        delta_lat 1 == delta_dis 1.1m
        delta_lon 1 == delta_dis 1.1m * (1-lat*lat/8100/1e+10)
    '''
    lat_mid = (lat1 + lat2) >> 1
    lat = abs(lat1 - lat2)
    lon = abs(lon1 - lon2) * (81000000000000 - lat_mid * lat_mid) / 81000000000000
    return 121 * (lat * lat + lon * lon) / 100


def cut_words(text):
    keywords = text + ' ';
    keywords += ' '.join(jieba.cut_for_search(text))
    return keywords


def http_request(url, args = None, post_args = None):
    args = args or {}
    post_data = None if post_args is None else urlencode(post_args)
    
    try:
        url = url + "?" + urlencode(args)
        logger.debug(url)
        response = urllib2.urlopen(url, post_data)
        result = response.read()
        response.close()
        return result
    except urllib2.HTTPError, e:
        logger.error(e.read())


def send_sms(dest, content):
    if not settings.SMS_DEBUG:
        result = http_request(url = 'http://www.jianzhou.sh.cn/JianzhouSMSWSServer/http/sendBatchMessage',
                              post_args = {'account': 'sdk_meiwei',
                                           'password': 'jianzhou1009',
                                           'destmobile': dest,
                                           'msgText': unicode(content),
                                           'sendDateTime': ''})
        logger.info('SMS Sent to %s %s' % (str(dest), str(result)))
    else:
        logger.info('SMS OFF')

def send_mail(subject, message, sent_to):
    if not settings.EMAIL_DEBUG:
        django_send_mail(subject = subject, 
                         message = message, 
                         from_email = 'systemadmin@clubmeiwei.com', 
                         recipient_list = [sent_to],
                         fail_silently=False)
        logger.info('Email Sent to %s' % str(sent_to))
    else:
        logger.info('Email OFF')
