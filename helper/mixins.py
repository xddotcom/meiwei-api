from django.utils.cache import patch_response_headers
from rest_framework.compat import HttpResponseBase
from rest_framework.response import Response

class CacheControlMixin(object):
    cache_timeout = 60 * 60 * 24
    
    def get_cache_timeout(self):
        return self.cache_timeout
    
    """
    `.finalize_response()` is pretty much the same as APIView's regular finalize_response,
    but with extra hooks for cache-control.
    """
    def finalize_response(self, request, response, *args, **kwargs):
        """
        Returns the final response object.
        """
        # Make the error obvious if a proper response is not returned
        assert isinstance(response, HttpResponseBase), (
            'Expected a `Response`, `HttpResponse` or `HttpStreamingResponse` '
            'to be returned from the view, but received a `%s`'
            % type(response)
        )

        if isinstance(response, Response):
            if not getattr(request, 'accepted_renderer', None):
                neg = self.perform_content_negotiation(request, force=True)
                request.accepted_renderer, request.accepted_media_type = neg

            response.accepted_renderer = request.accepted_renderer
            response.accepted_media_type = request.accepted_media_type
            response.renderer_context = self.get_renderer_context()

        for key, value in self.headers.items():
            response[key] = value

        if not response.exception:
            patch_response_headers(response, self.get_cache_timeout())

        return response
