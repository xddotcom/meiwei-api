

success = {'success': True}
fail = {'error': True}

login_succeeded = lambda msg : {'status': 'Login Succeeded', 'message': msg}
login_failed = lambda msg: {'status': 'Login Failed', 'message': msg}

logout_succeeded = lambda msg: {'status': 'Logout Succeeded', 'message': msg}
logout_failed = lambda msg: {'status': 'Logout Failed', 'message': msg}

register_succeeded = lambda msg: {'status': 'Register Succeeded', 'message': msg}
register_failed = lambda msg: {'status': 'Register Failed', 'message': msg}

status = {
    'BAD_REQUEST': 40000,
    
}
