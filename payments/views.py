from datetime import datetime
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.utils.translation import ugettext_lazy as _
from helper.functions import json_dumps, json_loads
from members.models import Credit
from orders.models import Payment
from orders.notify import sendOnPaymentFinishSMS, sendPaymentFinishMail
from payments import bocomm, payment_form_tpl
import base64


if settings.PAYMENT_DEBUG:
    merchID = '301310063009501' # test merchant id
else:
    #merchID = '301310058129500'
    merchID = '301310053119894'

interfaceVersion = '1.0.0.0'
tranType = '0'  # 0 for B2C
curType = 'CNY'  # trade currency
notifyType = '1'  # notify
jumpSeconds = '5'  # trade finish redirect jump time
netType = '0'

phdFlag = '0'
payBatchNo = ''
proxyMerName = ''
proxyMerType = ''
proxyMerCredentials = ''

default_redirect_uri = 'http://www.clubmeiwei.com/'


def payment_error(errMsg):
    return render_to_response('payment_error.html', {'errMsg': errMsg})


def payable(request, payment_no):
    now = datetime.now()
    payment = Payment.objects.get(payment_no=payment_no)
    redirect_uri = request.GET.get('redirect', default_redirect_uri)
    merURL = 'http://%s/payments/payment_notify/' % request.META['HTTP_HOST']
    goodsURL = 'http://%s/payments/payment_result/' % request.META['HTTP_HOST']
    
    if not payment.is_payable:
        return payment_error(_('Already Paid'))
    
    payment_no = payment_no
    orderDate = datetime.strftime(now, '%Y%m%d')
    orderTime = datetime.strftime(now, '%H%M%S')
    amount = str(payment.amount)
    orderContent = payment.comment
    orderMono = base64.b64encode(json_dumps({'redirect': redirect_uri}))
    issBankNo = 'OTHERS' #BOCOM #OTHERS
    
    source = '|'.join([interfaceVersion, merchID, payment_no, orderDate, orderTime,
              tranType, amount, curType, orderContent, orderMono, phdFlag,
              notifyType, merURL, goodsURL, jumpSeconds, payBatchNo, proxyMerName,
              proxyMerType, proxyMerCredentials, netType])
    
    retCode, errMsg, signMsg, orderUrl = bocomm.sign(source.encode('utf-8'))
    
    if (retCode != '0'):
        return payment_error(errMsg)
    
    context = {'orderUrl': orderUrl,
               'interfaceVersion': interfaceVersion,
               'merchID': merchID,
               'payment_no': payment_no,
               'orderDate': orderDate,
               'orderTime': orderTime,
               'tranType': tranType,
               'amount': amount,
               'curType': curType,
               'orderContent': orderContent,    # the order desc, wont be returned  
               'orderMono': orderMono,          # system mono, will be returned
               'phdFlag': phdFlag,
               'notifyType': notifyType,
               'merURL': merURL,
               'goodsURL': goodsURL,
               'jumpSeconds': jumpSeconds,
               'payBatchNo': payBatchNo,
               'proxyMerName': proxyMerName,
               'proxyMerType': proxyMerType,
               'proxyMerCredentials': proxyMerCredentials,
               'netType': netType,
               'signMsg': signMsg,
               'issBankNo': issBankNo}
    
    response_string = payment_form_tpl.PAYMENT_FORM_TEMPLATE.format(**context)
    
    return HttpResponse(response_string.encode('gbk'), content_type='text/html;charset=gbk')


def payment_result(request):
    notifyMsg = request.POST["notifyMsg"]
    retCode, errMsg = bocomm.verify(notifyMsg)
    if (retCode != '0'):
        return payment_error(errMsg)
    
    values = notifyMsg.split('|')
    keys = ['MERCHNO', 'ORDERNO', 'TRANAMOUNT', 'TRANCURRTYPE', 'PAYBATNO', 'MERCHBATCHNO',
            'TRANDATE', 'TRANTIME', 'SERIALNO', 'TRANRST', 'FEESUM', 'CARDTYPE', 'BankMoNo',
            'ErrDis', 'UserRemoteAddr', 'Referer', 'merComment', 'SignMsg']
    
    order_info = dict(zip(keys, values))
    
    order_info['merComment'] = json_loads(base64.b64decode(order_info['merComment'].decode('base64')))
    
    comment = json_loads(order_info['merComment']) # orderMono in payable view
    redirect_uri = comment.get('redirect', default_redirect_uri)
    
    amount = float(order_info['TRANAMOUNT'])
    payment_no = order_info['ORDERNO']
    payment = Payment.objects.get(payment_no=payment_no)
    payment.confirm(amount, 3)

    obj = {
        'payment_no': payment_no,
        'payment_from': 'yinlianzhifu',
        'amount': str(amount)
    }
    sendPaymentFinishMail(obj)
    sendOnPaymentFinishSMS(obj)

    if payment.member:
        Credit.objects.create(member=payment.member,
                              amount=int(amount+1), credit_type=8,
                              reason=payment.comment)

    return redirect(redirect_uri, permanent=True)


def payment_notify(request):
    payment_result(request)
    return HttpResponse(status=200)
