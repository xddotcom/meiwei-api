import socket
import xml.etree.ElementTree as ET

socket_ip = "127.0.0.1"
socket_port = 8088

xmlTpl = "<?xml version='1.0' encoding='utf-8'?><Message><TranCode>%s</TranCode><MsgContent>%s</MsgContent></Message>"

def connect_socket(tranCode, message):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((socket_ip, socket_port))
    
    xml = xmlTpl % (tranCode, message)
    
    client_socket.send(xml)
    
    recvMsg = client_socket.recv(16384)
    client_socket.close()
    
    return recvMsg


def sign(source):
    tranCode = "cb2200_sign"
    recvMsg = connect_socket(tranCode, source)
    rootElem = ET.fromstring(recvMsg)
    retCode = rootElem.find('opRep/retCode').text
    errMsg = rootElem.find('opRep/errMsg').text
    signMsg = rootElem.find('opRep/opResult/signMsg').text
    orderUrl = rootElem.find('opRep/opResult/orderUrl').text
    return retCode, errMsg, signMsg, orderUrl


def verify(notifyMsg):
    tranCode = "cb2200_verify" 
    recvMsg = connect_socket(tranCode, notifyMsg)
    rootElem = ET.fromstring(recvMsg)
    retCode = rootElem.find('opRep/retCode').text
    errMsg = rootElem.find('opRep/errMsg').text
    return retCode, errMsg
