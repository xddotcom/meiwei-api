
from django.conf.urls import patterns, url

urlpatterns = patterns('payments.views',
    url(r'^payable/(?P<payment_no>p?[0-9]+)', 'payable'),
    url(r'^payment_notify', 'payment_notify'),
    url(r'^payment_result', 'payment_result'),
)
