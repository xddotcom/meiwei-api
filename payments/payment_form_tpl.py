# encoding=gbk

"""
    Since Django could only render template in UTF-8
    We use the python string template to construct a GBK html. 
"""

PAYMENT_FORM_TEMPLATE = u"""
<!DOCTYPE html>
<html>
<head>
    <meta charset="gbk">
    <script>function pay() {{payableForm.submit();}}</script>
</head>
<body onload="pay()">
    <form name="payableForm" method="post" action="{orderUrl}">
        <input type="hidden" name="interfaceVersion" value="{interfaceVersion}">
        <input type="hidden" name="merID" value="{merchID}">
        <input type="hidden" name="orderid" value="{payment_no}">
        <input type="hidden" name="orderDate" value="{orderDate}">
        <input type="hidden" name="orderTime" value="{orderTime}">
        <input type="hidden" name="tranType" value="{tranType}">
        <input type="hidden" name="amount" value="{amount}">
        <input type="hidden" name="curType" value="{curType}">
        <input type="hidden" name="orderContent" value="{orderContent}">
        <input type="hidden" name="orderMono" value="{orderMono}">
        <input type="hidden" name="phdFlag" value="{phdFlag}">
        <input type="hidden" name="notifyType" value="{notifyType}">
        <input type="hidden" name="merURL" value="{merURL}">
        <input type="hidden" name="goodsURL" value="{goodsURL}">
        <input type="hidden" name="jumpSeconds" value="{jumpSeconds}">
        <input type="hidden" name="payBatchNo" value="{payBatchNo}">
        <input type="hidden" name="proxyMerName" value="{proxyMerName}">
        <input type="hidden" name="proxyMerType" value="{proxyMerType}">
        <input type="hidden" name="proxyMerCredentials" value="{proxyMerCredentials}">
        <input type="hidden" name="netType" value="{netType}">
        <input type="hidden" name="merSignMsg" value="{signMsg}">
        <input type="hidden" name="issBankNo" value="{issBankNo}">
        <!--<input type="submit">-->
    </form>
</body>
</html>
"""
