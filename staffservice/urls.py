
from django.conf.urls import patterns, url

urlpatterns = patterns('staffservice.views',
    url(r'^sendmsg', 'send_msg'),
    url(r'^msgpage', 'get_send_msg_page'),
)
