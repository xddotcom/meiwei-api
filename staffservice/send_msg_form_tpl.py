# encoding=gbk

"""
    Since Django could only render template in UTF-8
    We use the python string template to construct a GBK html. 
"""

PAYMENT_FORM_TEMPLATE = u"""
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>发送短信</h2>
    <form method="post" action="/staffservice/sendmsg/">
    <table style="width:360px">
    <tbody>
        <tr>
        <td>手机</td>
        <td><input name="phone" style='width:300px;height:2em'></td>
    </tr>
        <tr>
    <td>内容</td>
    <td><textarea rows="6" name="content" style="width:300px;resize:none"></textarea></td>
    </tr>
            <tr>
        <td></td>
        <td><input type="submit" value="发送"></td>
    </tr>
    </tbody>
    </table>
    </form>
</body>
</html>
"""
