import logging
from django.http import HttpResponse
from helper.functions import is_mobile_no
from orders.notify import sendCustomSMS
from staffservice import send_msg_form_tpl

logger = logging.getLogger('meiweiapi')


def send_msg(request):
    if 'phone' in request.POST:
        phone = request.POST["phone"]
        if 'content' in request.POST:
            content = request.POST["content"]
            if not is_mobile_no(phone):
                return HttpResponse('error')
            sendCustomSMS(phone, content)
            return HttpResponse('success')
        else:
            return HttpResponse('error')
    else:
        return HttpResponse('error')


def get_send_msg_page(request):
    response_string = send_msg_form_tpl.PAYMENT_FORM_TEMPLATE.format()
    return HttpResponse(response_string.encode('utf-8'))
