
from django.contrib import admin
from clients.models import ClientError

class ClientErrorAdmin(admin.ModelAdmin):
    list_display = ['id', 'time_created', 'message', 'detail']

admin.site.register(ClientError, ClientErrorAdmin)
