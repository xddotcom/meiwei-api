
from rest_framework import serializers
from clients.models import ClientError

class ClientErrorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ClientError
        fields = ('id', 'message', 'detail')
