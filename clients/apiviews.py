# encoding=utf-8

from clients.models import ClientError
from clients.serializers import ClientErrorSerializer
from django.views.decorators.cache import cache_page
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import api_view


class ClientErrorViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = ClientError.objects.all()
    serializer_class = ClientErrorSerializer
    permission_classes = []


@api_view(['GET'])
@cache_page(60 * 60 * 24)
def app(request):
    return Response({'version': '2.0'})


@api_view(['GET'])
@cache_page(60 * 60 * 24)
def package_order_stat(request):
    return Response({ 'count': 20129 })


@api_view(['GET'])
@cache_page(60 * 60 * 24)
def ad(request):
    tpl = """
        <header>This is an Ad</header>
        <img src="assets/img/hero.png" alt="">
    """
    return Response({#'online': True,
                     'target': 'internal',
                     'uri': 'ProductPurchase',
                     'template': tpl})


@api_view(['GET'])
@cache_page(60 * 60 * 24)
def hero(request):
    anshifu = {'id': 'RequestDriver1',
               'target': 'internal',
               'uri': 'RequestDriver',
               'name': u'美位代驾',
               'description': '',
               'picture': 'http://meiwei.u.qiniudn.com/upload/daijia.jpg'}
    vvip = {'id': 'VVIP1',
            'target': 'internal',
            'uri': 'VVIP',
            'name': u'美位VVIP服务',
            'description': '',
            'picture': 'assets/images/default-order-avatar.png'}
    cake = {'id': 'Cake1',
            'target': 'internal',
            'options': {'productId': 39},
            'uri': 'ProductList',
            'name': u'生日蛋糕定制',
            'description': '',
            'picture': 'http://meiwei.u.qiniudn.com/media/product/31a3c149f44996eec3eebe5b71950f67b31aa069.jpg'}
    flower = {'id': 'Flower1',
              'target': 'internal',
              'options': {'productId': 21},
              'uri': 'ProductList',
              'name': u'鲜花预订',
              'description': '',
              'picture': 'http://meiwei.u.qiniudn.com/media/product/f1c334e85e7505bd962413ae0cb6617717aaa810.jpg'}
    style = {'id': 'Style1',
             'target': 'internal',
             'options': {'productId': 32},
             'uri': 'ProductList',
             'name': u'派对造型',
             'description': '',
             'picture': 'http://meiwei.u.qiniudn.com/media/product/73c3953f1875931386af18f6adfc2fc2deb1219e.jpg'}
    plain = {'id': 'Plain1',
             'target': 'internal',
             'uri': 'ProductOrder',
             'options': {'productItemId': 175},
             'name': u'美位·飞体验',
             'description': '',
             'picture': 'http://meiwei.u.qiniudn.com/media/product/4651c3859c11825715d07f01b6a494eecebf1e13.jpg'}
    nightclub = {'id': 'nightclub1',
                 'target': 'internal',
                 'uri': 'RestaurantSearch',
                 'options': {'recommendId': 31},
                 'name': u'夜店预订',
                 'description': '',
                 'picture': 'http://meiwei.u.qiniudn.com/custom/upload3.jpg'}
    packageorder = {'id': 'packageorder',
                    'target': 'internal',
                    'uri': 'PackageOrder',
                    'name': u'敢爱礼盒',
                    'description': '',
                    'picture': 'assets/images/package-order-avatar.jpg'}
    return Response([packageorder, nightclub, plain, cake, flower, style, vvip, anshifu])


@api_view(['GET'])
@cache_page(60 * 60 * 24)
def newhero(request):
    anshifu = {'id': 'RequestDriver1',
               'target': 'internal',
               'uri': 'RequestDriver',
               'name': u'美位代驾',
               'description': '',
               'picture': 'http://meiwei.u.qiniudn.com/upload/daijia.jpg'}
    vvip = {'id': 'VVIP1',
            'target': 'internal',
            'uri': 'VVIP',
            'name': u'美位VVIP服务',
            'description': '',
            'picture': 'assets/img/default-order-avatar.png'}
    cake = {'id': 'Cake1',
            'target': 'internal',
            'options': {'productId': 39},
            'uri': 'ProductList',
            'name': u'生日蛋糕定制',
            'description': '',
            'picture': 'http://meiwei.u.qiniudn.com/media/product/31a3c149f44996eec3eebe5b71950f67b31aa069.jpg'}
    flower = {'id': 'Flower1',
              'target': 'internal',
              'options': {'productId': 21},
              'uri': 'ProductList',
              'name': u'鲜花预订',
              'description': '',
              'picture': 'http://meiwei.u.qiniudn.com/media/product/f1c334e85e7505bd962413ae0cb6617717aaa810.jpg'}
    style = {'id': 'Style1',
             'target': 'internal',
             'options': {'productId': 32},
             'uri': 'ProductList',
             'name': u'派对造型',
             'description': '',
             'picture': 'http://meiwei.u.qiniudn.com/media/product/73c3953f1875931386af18f6adfc2fc2deb1219e.jpg'}
    plain = {'id': 'Plain1',
             'target': 'internal',
             'uri': 'ProductOrder',
             'options': {'productItemId': 175},
             'name': u'美位·飞体验',
             'description': '',
             'picture': 'http://meiwei.u.qiniudn.com/media/product/4651c3859c11825715d07f01b6a494eecebf1e13.jpg'}
    nightclub = {'id': 'nightclub1',
                 'target': 'internal',
                 'uri': 'RestaurantSearch',
                 'options': {'recommendId': 31},
                 'name': u'夜店预订',
                 'description': '',
                 'picture': 'http://meiwei.u.qiniudn.com/custom/upload3.jpg'}
    return Response([nightclub, plain, cake, flower, style, vvip, anshifu])
