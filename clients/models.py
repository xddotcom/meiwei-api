from django.db import models

class ClientError(models.Model):
    id = models.AutoField(primary_key=True, db_column='id')
    message = models.TextField(db_column='message', null=True, blank=True)
    detail = models.TextField(db_column='detail', null=True, blank=True)
    time_created = models.DateTimeField(db_column='time_created', auto_now_add=True, null=True, blank=True)
    class Meta:
        db_table = u'client_error'
    def __unicode__(self):
        return unicode(self.message)
