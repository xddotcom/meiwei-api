
from django.conf.urls import patterns, include, url
from clients.apiviews import ClientErrorViewSet, app, ad, hero, newhero,\
    package_order_stat
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'error', ClientErrorViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^package_order_stat/$', package_order_stat),
    url(r'^app/$', app),
    url(r'^ad/$', ad),
    url(r'^hero/$', hero),
    url(r'^newhero/$', newhero),
)
